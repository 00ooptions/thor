/*
 * Copyright © 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.server;

import static net.luminis.tls.util.ByteUtils.bytesToHex;

import androidx.annotation.Nullable;

import net.luminis.LogUtils;
import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;
import net.luminis.quic.RawStreamData;
import net.luminis.quic.Receiver;
import net.luminis.quic.Version;
import net.luminis.quic.packet.InitialPacket;
import net.luminis.quic.packet.VersionNegotiationPacket;
import net.luminis.tls.handshake.TlsServerEngineFactory;
import net.luminis.tls.util.ByteUtils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.net.ssl.X509TrustManager;


/**
 * Listens for QUIC connections on a given port. Requires server certificate and corresponding private key.
 */
public class ServerConnector implements ServerConnectionRegistry {
    private static final String TAG = ServerConnector.class.getSimpleName();
    private static final int MINIMUM_LONG_HEADER_LENGTH = 1 + 4 + 1 + 1;
    private static final int CONNECTION_ID_LENGTH = 4;
    private final TlsServerEngineFactory tlsEngineFactory;
    private final Receiver receiver;
    private final List<Version> supportedVersions;
    private final List<Integer> supportedVersionIds;
    private final DatagramSocket serverSocket;
    private final boolean requireRetry;
    private final boolean supportVersionNegotiation;
    private final X509TrustManager trustManager;
    private final Map<ConnectionSource, ServerConnectionProxy> connections = new ConcurrentHashMap<>();
    private final ApplicationProtocolRegistry applicationProtocolRegistry =
            new ApplicationProtocolRegistry();
    private final Function<QuicStream, Consumer<RawStreamData>> streamDataConsumer;
    @Nullable
    private Consumer<QuicConnection> closedConsumer;


    public ServerConnector(DatagramSocket socket, X509TrustManager trustManager,
                           byte[] certificateFile, byte[] certificateKeyFile,
                           List<Version> supportedVersions,
                           Function<QuicStream, Consumer<RawStreamData>> streamDataConsumer,
                           boolean requireRetry, boolean supportVersionNegotiation) {
        this.serverSocket = socket;
        this.trustManager = trustManager;
        this.supportedVersions = supportedVersions;
        this.supportVersionNegotiation = supportVersionNegotiation;
        this.tlsEngineFactory = new TlsServerEngineFactory(certificateFile, certificateKeyFile);
        this.supportedVersionIds = supportedVersions.stream().map(
                Version::getId).collect(Collectors.toList());
        this.requireRetry = requireRetry;
        this.streamDataConsumer = streamDataConsumer;
        this.receiver = new Receiver(socket, datagramPacket -> {
            try {
                process(Instant.now(), datagramPacket);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, "Server packet " + throwable.getMessage());
            }
        }, throwable -> LogUtils.error(TAG, throwable));
    }


    public void setClosedConsumer(@Nullable Consumer<QuicConnection> closedConsumer) {
        this.closedConsumer = closedConsumer;
    }


    public int numConnections() {
        Set<ServerConnectionImpl> treeSet = new LinkedHashSet<>();
        for (ServerConnectionProxy conn : connections.values()) {
            if (conn instanceof ServerConnectionImpl) {
                treeSet.add((ServerConnectionImpl) conn);
            }

        }
        return treeSet.size();
    }

    public void shutdown() {
        try {
            connections.values().forEach(ServerConnectionProxy::terminate);
            receiver.shutdown();
            serverSocket.close();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            connections.clear();
        }
    }

    public void registerApplicationProtocol(String protocol, ApplicationProtocolConnectionFactory protocolConnectionFactory) {
        applicationProtocolRegistry.registerApplicationProtocol(protocol, protocolConnectionFactory);
    }

    public Set<String> getRegisteredApplicationProtocols() {
        return applicationProtocolRegistry.getRegisteredApplicationProtocols();
    }

    public void start() {
        receiver.start();
    }

    protected void process(Instant timeReceived, DatagramPacket datagramPacket) {
        ByteBuffer data = ByteBuffer.wrap(datagramPacket.getData(), 0, datagramPacket.getLength());
        int flags = data.get();
        data.rewind();
        if ((flags & 0b1100_0000) == 0b1100_0000) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2
            // "Header Form:  The most significant bit (0x80) of byte 0 (the first byte) is set to 1 for long headers."
            processLongHeaderPacket((InetSocketAddress) datagramPacket.getSocketAddress(), data, timeReceived);
        } else if ((flags & 0b1100_0000) == 0b0100_0000) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.3
            // "Header Form:  The most significant bit (0x80) of byte 0 is set to 0 for the short header.
            processShortHeaderPacket((InetSocketAddress) datagramPacket.getSocketAddress(), data, timeReceived);
            //todo LogUtils.error(TAG, "processShortHeaderPacket");
        } else {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.3
            // "The next bit (0x40) of byte 0 is set to 1. Packets containing a zero value for this bit are not valid
            //  packets in this version and MUST be discarded."
            LogUtils.debug(TAG, "Local port " + serverSocket.getLocalPort() +
                    String.format(" Invalid Quic packet (flags: %02x) is discarded", flags));
        }
    }

    private void processLongHeaderPacket(InetSocketAddress clientAddress, ByteBuffer data, Instant received) {
        if (data.remaining() >= MINIMUM_LONG_HEADER_LENGTH) {
            data.position(1);
            int version = data.getInt();


            data.position(5);
            int dcidLength = data.get() & 0xff;
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2
            // "In QUIC version 1, this value MUST NOT exceed 20. Endpoints that receive a version 1 long header with a
            //  value larger than 20 MUST drop the packet. In order to properly form a Version Negotiation packet,
            //  servers SHOULD be able to read longer connection IDs from other QUIC versions."
            if (dcidLength > 20) {
                if (initialWithUnspportedVersion(data, version)) {
                    // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-6
                    // "A server sends a Version Negotiation packet in response to each packet that might initiate a new connection;"
                    if (supportVersionNegotiation) {
                        sendVersionNegotiationPacket(clientAddress, data, dcidLength);
                    }
                }
                LogUtils.error(TAG, "Local port " + serverSocket.getLocalPort() +
                        " Ignore connection from  " + clientAddress);

                return;
            }
            if (data.remaining() >= dcidLength + 1) {  // after dcid at least one byte scid length
                byte[] dcid = new byte[dcidLength];
                data.get(dcid);

                int scidLength = data.get() & 0xff;
                if (data.remaining() >= scidLength) {
                    byte[] scid = new byte[scidLength];
                    data.get(scid);
                    data.rewind();


                    ServerConnectionProxy connection = isExistingConnection(clientAddress, dcid);
                    if (connection == null) {
                        LogUtils.debug(TAG,
                                String.format("Original destination connection id: %s (scid: %s)",
                                        bytesToHex(dcid),
                                        bytesToHex(scid)));
                        if (mightStartNewConnection(data, version, dcid) &&
                                isExistingConnection(clientAddress, dcid) == null) {
                            connection = createNewConnection(version, clientAddress, dcid);
                        } else if (initialWithUnspportedVersion(data, version)) {
                            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-6
                            // "A server sends a Version Negotiation packet in response to each packet that might initiate a new connection;"
                            if (supportVersionNegotiation) {
                                sendVersionNegotiationPacket(clientAddress, data, dcidLength);
                            }
                        }
                    }
                    if (connection != null) {
                        connection.parsePackets(received, data);
                    }
                }
            }
        }
    }

    private void processShortHeaderPacket(InetSocketAddress clientAddress, ByteBuffer data, Instant received) {
        byte[] dcid = new byte[CONNECTION_ID_LENGTH];
        data.position(1);
        data.get(dcid);
        data.rewind();

        ServerConnectionProxy connection = isExistingConnection(clientAddress, dcid);
        if (connection != null) {
            connection.parsePackets(received, data);
        } else {
            LogUtils.error(TAG, "Local port " + serverSocket.getLocalPort() +
                    " Discarding short header " + bytesToHex(dcid) +
                    " packet addressing non existent connection " + clientAddress.getPort());
        }
    }

    private boolean mightStartNewConnection(ByteBuffer packetBytes, int version, byte[] dcid) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-7.2
        // "This Destination Connection ID MUST be at least 8 bytes in length."
        if (dcid.length >= 8) {
            return supportedVersionIds.contains(version);
        } else {
            return false;
        }
    }

    private boolean initialWithUnspportedVersion(ByteBuffer packetBytes, int version) {
        packetBytes.rewind();
        int type = (packetBytes.get() & 0x30) >> 4;
        if (InitialPacket.isInitial(type, Version.parse(version))) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-14.1
            // "A server MUST discard an Initial packet that is carried in a UDP
            //   datagram with a payload that is smaller than the smallest allowed
            //   maximum datagram size of 1200 bytes. "
            if (packetBytes.limit() >= 1200) {
                return !supportedVersionIds.contains(version);
            }
        }
        return false;
    }

    private ServerConnectionProxy createNewConnection(int versionValue, InetSocketAddress clientAddress, byte[] dcid) {
        Version version = Version.parse(versionValue);
        ServerConnectionProxy connectionCandidate = new ServerConnectionCandidate(trustManager,
                CONNECTION_ID_LENGTH, serverSocket, tlsEngineFactory,
                requireRetry, applicationProtocolRegistry, 100, this,
                this::removeConnection, streamDataConsumer,
                version, clientAddress, dcid);
        // Register new connection now with the original connection id, as retransmitted initial packets with the
        // same original dcid might be received, which should _not_ lead to another connection candidate)

        connections.put(new ConnectionSource(dcid), connectionCandidate);
        return connectionCandidate;
    }

    private void removeConnection(ServerConnectionImpl connection) {
        ServerConnectionProxy removed = null;
        for (byte[] connectionId : connection.getActiveConnectionIds()) {
            if (removed == null) {
                removed = removeConnection(connectionId);
                if (removed == null) {
                    LogUtils.error(TAG, "Cannot remove connection with cid " + ByteUtils.bytesToHex(connectionId));
                }
            } else {
                if (removed != removeConnection(connectionId)) {
                    LogUtils.error(TAG, "Removed connections for set of active cids are not identical");
                }
            }
        }
        connections.remove(new ConnectionSource(connection.getOriginalDestinationConnectionId()));

        if (removed != null) {
            if (!removed.isClosed()) {
                LogUtils.error(TAG, "Removed connection with dcid " +
                        ByteUtils.bytesToHex(connection.getOriginalDestinationConnectionId()) +
                        " that is not closed...");
            }
            removed.terminate();

            if (closedConsumer != null) {
                closedConsumer.accept(connection);
            }
        }
    }

    @Nullable
    private ServerConnectionProxy isExistingConnection(InetSocketAddress clientAddress, byte[] dcid) {
        return getConnection(dcid);
    }

    private void sendVersionNegotiationPacket(InetSocketAddress clientAddress, ByteBuffer data, int dcidLength) {
        data.rewind();
        if (data.remaining() >= 1 + 4 + 1 + dcidLength + 1) {
            byte[] dcid = new byte[dcidLength];
            data.position(1 + 4 + 1);
            data.get(dcid);
            int scidLength = data.get() & 0xff;
            byte[] scid = new byte[scidLength];
            if (scidLength > 0) {
                data.get(scid);
            }
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2.1
            // "The server MUST include the value from the Source Connection ID field of the packet it receives in the
            //  Destination Connection ID field. The value for Source Connection ID MUST be copied from the Destination
            //  Connection ID of the received packet, ..."
            VersionNegotiationPacket versionNegotiationPacket =
                    new VersionNegotiationPacket(supportedVersions, dcid, scid);
            byte[] packetBytes = versionNegotiationPacket.generatePacketBytes(null, null);
            DatagramPacket datagram = new DatagramPacket(packetBytes, packetBytes.length,
                    clientAddress.getAddress(), clientAddress.getPort());
            try {
                serverSocket.send(datagram);
            } catch (IOException e) {
                LogUtils.error(TAG, "Sending version negotiation packet failed", e);
            }
        }
    }

    @Override
    public void registerConnection(ServerConnectionProxy connection, byte[] connectionId) {
        connections.put(new ConnectionSource(connectionId), connection);
    }

    @Override
    public void deregisterConnection(ServerConnectionProxy connection, byte[] connectionId) {
        boolean removed = removeConnection(connection, connectionId);
        if (!removed && containsKey(connectionId)) {
            LogUtils.error(TAG, "Connection " + connection + " not removed, because "
                    + getConnection(connectionId) + " is registered for "
                    + ByteUtils.bytesToHex(connectionId));
        }
    }

    private boolean containsKey(byte[] connectionId) {
        return connections.containsKey(new ConnectionSource(connectionId));
    }

    private boolean removeConnection(ServerConnectionProxy connection, byte[] connectionId) {
        return connections.remove(new ConnectionSource(connectionId), connection);
    }

    private ServerConnectionProxy removeConnection(byte[] connectionId) {
        return connections.remove(new ConnectionSource(connectionId));
    }

    @Nullable
    private ServerConnectionProxy getConnection(byte[] connectionId) {

        return connections.get(new ConnectionSource(connectionId));
    }

    @Override
    public void registerAdditionalConnectionId(byte[] currentConnectionId, byte[] newConnectionId) {
        ServerConnectionProxy connection = getConnection(currentConnectionId);
        if (connection != null) {
            registerConnection(connection, newConnectionId);
        } else {
            LogUtils.error(TAG, "Cannot add additional cid to non-existing connection " +
                    ByteUtils.bytesToHex(currentConnectionId));
        }
    }

    @Override
    public void deregisterConnectionId(byte[] connectionId) {
        removeConnection(connectionId);
    }

    public DatagramSocket getSocket() {
        return serverSocket;
    }

    public Receiver getReceiver() {
        return receiver;
    }

}
