/*
 * Copyright © 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.cid;

import androidx.annotation.Nullable;

import net.luminis.quic.Settings;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;


public abstract class ConnectionIdRegistry {


    /**
     * Maps sequence number to connection ID (info)
     */
    protected final Map<Integer, ConnectionIdInfo> connectionIds = new ConcurrentHashMap<>();
    protected final AtomicReference<byte[]> currentConnectionId = new AtomicReference<>();
    private final int connectionIdLength;

    public ConnectionIdRegistry() {
        this(Settings.DEFAULT_CID_LENGTH);
    }

    public ConnectionIdRegistry(Integer cidLength) {
        this.connectionIdLength = cidLength != null ? cidLength : Settings.DEFAULT_CID_LENGTH;
        currentConnectionId.set(generateConnectionId());
        connectionIds.put(0, new ConnectionIdInfo(0,
                currentConnectionId.get(), ConnectionIdStatus.IN_USE));
    }

    @Nullable
    public byte[] retireConnectionId(int sequenceNr) {
        ConnectionIdInfo cidInfo = connectionIds.get(sequenceNr);
        if (cidInfo != null) {
            if (cidInfo.getConnectionIdStatus().active()) {
                cidInfo.setStatus(ConnectionIdStatus.RETIRED);
                return cidInfo.getConnectionId();
            }
        }
        return null;
    }

    /**
     * use getActive to get <em>an</em> active connection ID
     */
    public byte[] getCurrent() {
        return currentConnectionId.get();
    }

    /**
     * Get an active connection ID. There can be multiple active connection IDs, this method returns an arbitrary one.
     *
     * @return an active connection ID or null if non is active (which should never happen).
     */
    @Nullable
    public byte[] getActive() {
        return connectionIds.entrySet().stream()
                .filter(e -> e.getValue().getConnectionIdStatus().active())
                .map(e -> e.getValue().getConnectionId())
                .findFirst().orElse(null);
    }

    public Map<Integer, ConnectionIdInfo> getAll() {
        return connectionIds;
    }

    protected int currentIndex() {
        //noinspection OptionalGetWithoutIsPresent
        return connectionIds.entrySet().stream()
                .filter(entry -> Arrays.equals(entry.getValue().getConnectionId(),
                        currentConnectionId.get()))
                .mapToInt(Map.Entry::getKey)
                .findFirst().getAsInt();
    }

    protected byte[] generateConnectionId() {
        byte[] connectionId = new byte[connectionIdLength];
        new SecureRandom().nextBytes(connectionId);
        return connectionId;
    }

    public int getConnectionIdlength() {
        return connectionIdLength;
    }

    public List<byte[]> getActiveConnectionIds() {
        return connectionIds.values().stream()
                .filter(cid -> cid.getConnectionIdStatus().active())
                .map(ConnectionIdInfo::getConnectionId)
                .collect(Collectors.toList());
    }
}

