package net.luminis.quic;

public class Settings {

    public static final int DEFAULT_CID_LENGTH = 8;
    public final static int K_MAX_DATAGRAM_SIZE = 1200; // TODO: 1200 is the minimum, actual value can be larger

    // https://tools.ietf.org/html/draft-ietf-quic-recovery-23#appendix-B.1
    // "The RECOMMENDED value is the minimum of 10 * kMaxDatagramSize and max(2* kMaxDatagramSize, 14600))."
    // "kMaxDatagramSize: The RECOMMENDED value is 1200 bytes."
    public static final int INITIAL_WINDOW_SIZE = 10 * K_MAX_DATAGRAM_SIZE;
    // https://tools.ietf.org/html/draft-ietf-quic-recovery-23#appendix-B.1
    // "Minimum congestion window in bytes.  The RECOMMENDED value is 2 * kMaxDatagramSize."
    public final static int K_MINIMUM_WINDOW = 2 * K_MAX_DATAGRAM_SIZE;


    // https://tools.ietf.org/html/draft-ietf-quic-recovery-23#appendix-B.1
    // "Reduction in congestion window when a new loss event is detected.  The RECOMMENDED value is 0.5."
    public static final int K_LOSS_REDUCTION_FACTOR = 2; // note how it is used

    // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-14.1:
    // "An endpoint SHOULD use Datagram Packetization Layer PMTU Discovery
    //   ([DPLPMTUD]) or implement Path MTU Discovery (PMTUD) [RFC1191]
    //   [RFC8201] ..."
    // "In the absence of these mechanisms, QUIC endpoints SHOULD NOT send IP
    //   packets larger than 1280 bytes.  Assuming the minimum IP header size,
    //   this results in a QUIC maximum packet size of 1232 bytes for IPv6 and
    //   1252 bytes for IPv4."
    // As it is not know (yet) whether running over IP4 or IP6, take the smallest of the two:
    public static final int MAX_PACKAGE_SIZE = 1232;

    public static final int MAX_STREAMS = 10000;
    public static final int INITIAL_MAX_STREAM_DATA_SERVER = 1 << 22; // 4 MB
}
