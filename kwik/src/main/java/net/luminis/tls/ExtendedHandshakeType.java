package net.luminis.tls;

import android.util.SparseArray;

import net.luminis.quic.ImplementationError;


public enum ExtendedHandshakeType {
    client_hello(1),
    server_hello(2),
    new_session_ticket(4),
    end_of_early_data(5),
    encrypted_extensions(8),
    certificate(11),
    certificate_request(13),
    certificate_verify(15),
    finished(20),
    key_update(24),
    server_certificate(249),
    server_certificate_verify(250),
    server_finished(251),
    client_certificate(252),
    client_certificate_verify(253),
    client_finished(254);

    private static final SparseArray<ExtendedHandshakeType> byOrdinal = new SparseArray<>();

    static {
        for (ExtendedHandshakeType t : ExtendedHandshakeType.values()) {
            byOrdinal.put(t.ordinal(), t);
        }
    }

    public final byte value;


    ExtendedHandshakeType(int value) {
        this.value = (byte) value;
    }

    public static ExtendedHandshakeType get(int ordinal) {
        ExtendedHandshakeType type = byOrdinal.get(ordinal);
        if (type == null) {
            throw new ImplementationError();
        }
        return type;
    }

}
