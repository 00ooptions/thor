package threads.thor.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import threads.thor.core.events.EVENTS;
import threads.thor.core.events.Event;
import threads.thor.core.events.EventsDatabase;

public class EventViewModel extends AndroidViewModel {

    private final EventsDatabase eventsDatabase;

    public EventViewModel(@NonNull Application application) {
        super(application);
        eventsDatabase = EVENTS.getInstance(
                application.getApplicationContext()).getEventsDatabase();
    }

    public LiveData<Event> getUri() {
        return eventsDatabase.eventDao().getEvent(EVENTS.URI);
    }


    public LiveData<Event> getFatal() {
        return eventsDatabase.eventDao().getEvent(EVENTS.FATAL);
    }

    public LiveData<Event> getError() {
        return eventsDatabase.eventDao().getEvent(EVENTS.ERROR);
    }

    public LiveData<Event> getWarning() {
        return eventsDatabase.eventDao().getEvent(EVENTS.WARNING);
    }

    public LiveData<Event> getInfo() {
        return eventsDatabase.eventDao().getEvent(EVENTS.INFO);
    }


    public void removeEvent(@NonNull final Event event) {
        eventsDatabase.eventDao().deleteEvent(event);
    }

    public LiveData<Event> getPermission() {
        return eventsDatabase.eventDao().getEvent(EVENTS.PERMISSION);
    }
}