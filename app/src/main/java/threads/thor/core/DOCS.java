package threads.thor.core;

import android.content.Context;
import android.net.Uri;
import android.webkit.WebResourceResponse;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Link;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;
import threads.thor.InitApplication;
import threads.thor.LogUtils;
import threads.thor.core.books.BOOKS;
import threads.thor.core.locals.LOCALS;
import threads.thor.magic.ContentInfo;
import threads.thor.magic.ContentInfoUtil;
import threads.thor.services.MimeTypeService;

public class DOCS {

    private static final String TAG = DOCS.class.getSimpleName();

    private static volatile DOCS INSTANCE = null;
    public final AtomicBoolean darkMode = new AtomicBoolean(false);
    private final IPFS ipfs;
    private final BOOKS books;
    private final LOCALS locals;
    @NonNull
    private final Set<Uri> uris = ConcurrentHashMap.newKeySet();
    @NonNull
    private final ConcurrentHashMap<Integer, Session> sessions = new ConcurrentHashMap<>();
    @NonNull
    private final ConcurrentHashMap<PeerId, Cid> resolves = new ConcurrentHashMap<>();


    private DOCS(@NonNull Context context) throws Exception {
        long start = System.currentTimeMillis();
        locals = LOCALS.getInstance();
        ipfs = IPFS.getInstance(context);
        books = BOOKS.getInstance(context);

        LogUtils.info(InitApplication.TIME_TAG, "DOCS finish [" +
                (System.currentTimeMillis() - start) + "]...");
    }

    public static DOCS getInstance(@NonNull Context context) throws Exception {

        if (INSTANCE == null) {
            synchronized (DOCS.class) {
                if (INSTANCE == null) {
                    INSTANCE = new DOCS(context);
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    public static String getFileName(@NonNull Uri uri) {

        List<String> paths = uri.getPathSegments();
        if (!paths.isEmpty()) {
            return paths.get(paths.size() - 1);
        } else {
            return "" + uri.getHost();
        }

    }

    @NonNull
    public static Uri redirectHttp(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), Content.HTTP)) {
                String host = uri.getHost();
                Objects.requireNonNull(host);
                if (Objects.equals(host, "localhost") || Objects.equals(host, "127.0.0.1")) {
                    List<String> paths = uri.getPathSegments();
                    if (paths.size() >= 2) {
                        String protocol = paths.get(0);
                        String authority = paths.get(1);
                        List<String> subPaths = new ArrayList<>(paths);
                        subPaths.remove(protocol);
                        subPaths.remove(authority);
                        Cid.decode(authority); // make sure authority is valid

                        if (Objects.equals(protocol, Content.IPFS)) {
                            Uri.Builder builder = new Uri.Builder();
                            builder.scheme(Content.IPFS)
                                    .authority(authority);

                            for (String path : subPaths) {
                                builder.appendPath(path);
                            }
                            return builder.build();
                        } else if (Objects.equals(protocol, Content.IPNS)) {
                            Uri.Builder builder = new Uri.Builder();
                            builder.scheme(Content.IPNS)
                                    .authority(authority);

                            for (String path : subPaths) {
                                builder.appendPath(path);
                            }
                            return builder.build();
                        }
                    }

                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return uri;
    }

    @NonNull
    public static Uri redirectHttps(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), Content.HTTPS)) {
                List<String> paths = uri.getPathSegments();
                if (paths.size() >= 2) {
                    String protocol = paths.get(0);
                    if (Objects.equals(protocol, Content.IPFS) ||
                            Objects.equals(protocol, Content.IPNS)) {
                        String authority = paths.get(1);
                        List<String> subPaths = new ArrayList<>(paths);
                        subPaths.remove(protocol);
                        subPaths.remove(authority);
                        Cid.decode(authority); // make sure authority is valid
                        if (Objects.equals(protocol, Content.IPFS)) {
                            Uri.Builder builder = new Uri.Builder();
                            builder.scheme(Content.IPFS)
                                    .authority(authority);

                            for (String path : subPaths) {
                                builder.appendPath(path);
                            }
                            return builder.build();
                        } else if (Objects.equals(protocol, Content.IPNS)) {
                            Uri.Builder builder = new Uri.Builder();
                            builder.scheme(Content.IPNS)
                                    .authority(authority);

                            for (String path : subPaths) {
                                builder.appendPath(path);
                            }
                            return builder.build();
                        }
                    }

                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return uri;
    }


    public int numUris() {
        return uris.size();
    }

    public void detachUri(@NonNull Uri uri) {
        uris.remove(uri);
    }

    public void attachUri(@NonNull Uri uri) {
        uris.add(uri);
    }

    @NonNull
    public Session getSession(int authority) {
        Session session = sessions.get(authority);
        if (session != null) {
            return session;
        }

        LogUtils.error(TAG, "create session for authority " + authority);

        Session newSession = ipfs.createSession(true);

        locals.getLocals().parallelStream().forEach(multiaddr -> {
            try {
                if (!Objects.equals(multiaddr.getPeerId(), ipfs.self())) {
                    LogUtils.error(TAG, "try " + multiaddr);
                    ipfs.dial(newSession, multiaddr, ipfs.getConnectionParameters());
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });
        sessions.put(authority, newSession);

        return newSession;
    }

    public boolean hasSession(int authority) {
        return sessions.containsKey(authority);
    }

    // release all sessions except the session with the given authority
    public void releaseSessions(int authority) {
        LogUtils.error(TAG, "Release sessions except " + authority);
        for (Map.Entry<Integer, Session> entry : sessions.entrySet()) {
            if (!Objects.equals(entry.getKey(), authority)) {
                Session session = sessions.remove(entry.getKey());
                if (session != null) {
                    session.close();
                }
            }
        }
    }

    @NonNull
    private String getMimeType(@NonNull Session session, @NonNull Context context, @NonNull Cid cid,
                               @NonNull Cancellable closeable) throws Exception {

        if (ipfs.isDir(session, cid, closeable)) {
            return MimeTypeService.DIR_MIME_TYPE;
        }

        return getContentMimeType(session, context, cid, closeable);
    }

    @NonNull
    private String getContentMimeType(@NonNull Session session, @NonNull Context context,
                                      @NonNull Cid cid,
                                      @NonNull Cancellable closeable) throws IOException {

        String mimeType = MimeTypeService.OCTET_MIME_TYPE;

        try (InputStream in = ipfs.getInputStream(session, cid, closeable)) {

            ContentInfo info = ContentInfoUtil.getInstance(context).findMatch(in);

            if (info != null) {
                mimeType = info.getMimeType();
            }

        } catch (IOException closedException) {
            throw closedException;
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        return mimeType;
    }

    @Nullable
    public String getHost(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), Content.IPNS)) {
                return uri.getHost();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return null;
    }


    @Nullable
    private Cid resolveLocal(Session session, PeerId peerId) {

        try {
            Multiaddr multiaddr = locals.getLocalAddress(peerId);
            if (multiaddr != null) {
                Connection connection = session.connect(multiaddr, ipfs.getConnectionParameters());
                IpnsEntity ipnsEntity =
                        ipfs.pull(connection).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                if (Objects.equals(ipnsEntity.getPeerId(), peerId)) {
                    ipfs.getPageStore().storePage(ipnsEntity);
                    Cid resolvedCid = ipfs.decodeIpnsData(ipnsEntity);
                    addResolves(ipnsEntity.getPeerId(), resolvedCid);
                    return resolvedCid;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        return null;
    }

    @Nullable
    private Cid resolveSwarm(Session session, PeerId peerId) {

        try {
            Connection connection = session.getConnection(peerId);
            if (connection != null) {
                IpnsEntity ipnsEntity =
                        ipfs.pull(connection).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                if (Objects.equals(ipnsEntity.getPeerId(), peerId)) {
                    ipfs.getPageStore().storePage(ipnsEntity);
                    Cid resolvedCid = ipfs.decodeIpnsData(ipnsEntity);
                    addResolves(ipnsEntity.getPeerId(), resolvedCid);
                    return resolvedCid;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        return null;
    }

    @NonNull
    public Cid resolveName(@NonNull Session session, @NonNull Uri uri, @NonNull PeerId peerId,
                           @NonNull Cancellable closeable) throws Exception {

        Cid resolved = resolves.get(peerId);
        if (resolved != null) {
            return resolved;
        }

        resolved = resolveSwarm(session, peerId);
        if (resolved != null) {
            return resolved;
        }

        resolved = resolveLocal(session, peerId);
        if (resolved != null) {
            return resolved;
        }

        long sequence = 0L;
        Cid cid = null;
        long timeout = IPFS.MAX_TIMEOUT;

        IpnsEntity page = ipfs.getPageStore().getPage(peerId);
        if (page != null) {
            sequence = page.getSequence();
            cid = IpnsEntity.decodeIpnsData(page.getValue());
            if (page.getEolDate().after(new Date(System.currentTimeMillis()))) {
                // page is still valid, therefore timeout just 30 sec
                timeout = 30;
            }
        }


        IpnsEntity ipnsEntity = ipfs.resolve(session, peerId, sequence,
                new TimeoutCancellable(closeable, timeout));
        if (ipnsEntity == null) {
            if (cid != null) {
                resolves.put(peerId, cid);
                return cid;
            }
            throw new ResolveNameException(uri.toString());
        }

        if (Objects.equals(ipnsEntity.getPeerId(), peerId)) {
            ipfs.getPageStore().storePage(ipnsEntity);
            Cid resolvedCid = ipfs.decodeIpnsData(ipnsEntity);
            addResolves(ipnsEntity.getPeerId(), resolvedCid);
            return resolvedCid;
        }
        throw new ResolveNameException(uri.toString());
    }


    public void addResolves(PeerId peerId, Cid cid) {
        resolves.put(peerId, cid);
    }

    public WebResourceResponse createRedirectMessage(@NonNull Uri uri) {
        return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE, Content.UTF8,
                new ByteArrayInputStream(("<!DOCTYPE HTML>\n" +
                        "<html lang=\"en-US\">\n" +
                        "    <head>\n" + MimeTypeService.META +
                        "        <meta http-equiv=\"refresh\" content=\"0; url=" + uri + "\">\n" +
                        "        <title>Page Redirection</title>\n" +
                        "    </head>\n" + MimeTypeService.STYLE +
                        "    <body>\n" +
                        "        Automatically redirected to the <a style=\"word-wrap: break-word;\" href='" + uri + "'>" + uri + "</a> location\n" +
                        "</html>").getBytes()));
    }

    public String generateDirectoryHtml(@NonNull Uri uri,
                                        @NonNull List<String> paths,
                                        @Nullable List<Link> links) {
        String title = uri.getHost();

        StringBuilder answer = new StringBuilder("<html>" + "<head>" + MimeTypeService.META +
                "<title>" + title + "</title>");

        answer.append("</head>");
        answer.append(MimeTypeService.STYLE);
        answer.append("<body>");

        if (links != null) {
            if (!links.isEmpty()) {
                answer.append("<form><table  width=\"100%\" style=\"border-spacing: 8px;\">");
                for (Link link : links) {

                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme(uri.getScheme()).authority(uri.getAuthority());
                    for (String path : paths) {
                        builder.appendPath(path);
                    }
                    builder.appendPath(link.getName());
                    builder.appendQueryParameter("download", "0");
                    Uri linkUri = builder.build();
                    answer.append("<tr>");

                    answer.append("<td>");
                    answer.append(MimeTypeService.getSvgResource(link.getName(), darkMode.get()));
                    answer.append("</td>");

                    answer.append("<td width=\"100%\" style=\"word-break:break-word\">");
                    answer.append("<a href=\"");
                    answer.append(linkUri.toString());
                    answer.append("\">");
                    answer.append(link.getName());
                    answer.append("</a>");
                    answer.append("</td>");

                    answer.append("<td>");
                    answer.append(getFileSize(link.getSize()));
                    answer.append("</td>");

                    answer.append("<td align=\"center\">");
                    String text = "<button style=\"float:none!important;display:inline;\" " +
                            "name=\"download\" value=\"1\" formenctype=\"text/plain\" " +
                            "formmethod=\"get\" type=\"submit\" formaction=\"" +
                            linkUri + "\">" + MimeTypeService.getSvgDownload() + "</button>";
                    answer.append(text);
                    answer.append("</td>");
                    answer.append("</tr>");
                }
                answer.append("</table></form>");
            }

        }
        answer.append("</body></html>");


        return answer.toString();
    }

    private String getFileSize(long size) {

        String fileSize;

        if (size < 1000) {
            fileSize = String.valueOf(size);
            return fileSize.concat(" B");
        } else if (size < 1000 * 1000) {
            fileSize = String.valueOf((double) (size / 1000));
            return fileSize.concat(" KB");
        } else {
            fileSize = String.valueOf((double) (size / (1000 * 1000)));
            return fileSize.concat(" MB");
        }
    }

    @NonNull
    public WebResourceResponse getResponse(@NonNull Session session,
                                           @NonNull Context context, @NonNull Uri uri,
                                           @NonNull Cid root, @NonNull List<String> paths,
                                           @NonNull Cancellable closeable) throws Exception {
        if (paths.isEmpty()) {
            if (ipfs.isDir(session, root, closeable)) {
                List<Link> links = ipfs.links(session, root, false, closeable);
                String answer = generateDirectoryHtml(uri, paths, links);
                return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE, Content.UTF8,
                        new ByteArrayInputStream(answer.getBytes()));
            } else {
                String mimeType = getContentMimeType(session, context, root, closeable);
                return getContentResponse(session, root, mimeType, closeable);
            }


        } else {
            Cid cid = ipfs.resolveCid(session, root, paths, closeable);
            if (ipfs.isDir(session, cid, closeable)) {
                List<Link> links = ipfs.links(session, cid, false, closeable);
                String answer = generateDirectoryHtml(uri, paths, links);
                return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE, Content.UTF8,
                        new ByteArrayInputStream(answer.getBytes()));

            } else {
                String mimeType = getMimeType(session, context, uri, cid, closeable);
                return getContentResponse(session, cid, mimeType, closeable);
            }
        }
    }

    @NonNull
    private WebResourceResponse getContentResponse(@NonNull Session session,
                                                   @NonNull Cid cid, @NonNull String mimeType,
                                                   @NonNull Cancellable closeable)
            throws Exception {

        try (InputStream in = ipfs.getInputStream(session, cid, closeable)) {


            Map<String, String> responseHeaders = new HashMap<>();

            return new WebResourceResponse(mimeType, Content.UTF8, 200,
                    "OK", responseHeaders, new BufferedInputStream(in));
        }


    }

    @NonNull
    public String getMimeType(@NonNull Session session, @NonNull Context context, @NonNull Uri uri,
                              @NonNull Cid cid, @NonNull Cancellable closeable)
            throws Exception {

        List<String> paths = uri.getPathSegments();
        if (!paths.isEmpty()) {
            String name = paths.get(paths.size() - 1);
            String mimeType = MimeTypeService.getMimeType(name);
            if (!mimeType.equals(MimeTypeService.OCTET_MIME_TYPE)) {
                return mimeType;
            } else {
                return getMimeType(session, context, cid, closeable);
            }
        } else {
            return getMimeType(session, context, cid, closeable);
        }

    }

    @Nullable
    public Cid getContent(@NonNull Session session, @NonNull Uri uri, @NonNull Cancellable closeable)
            throws Exception {

        String host = uri.getHost();
        Objects.requireNonNull(host);

        Cid root = getRoot(session, uri, closeable);

        List<String> paths = uri.getPathSegments();

        return ipfs.resolveCid(session, root, paths, closeable);
    }

    @NonNull
    private Cid resolveHost(@NonNull Session session, @NonNull Uri uri,
                            @NonNull String host, @NonNull Cancellable closeable)
            throws Exception {
        String link = ipfs.resolveDnsLink(host);
        if (link.isEmpty()) {
            // could not resolved, maybe no NETWORK
            String dnsLink = books.getDnsLink(uri.toString());
            if (dnsLink == null) {
                throw new DOCS.ResolveNameException(uri.toString());
            } else {
                return resolveDnsLink(session, uri, dnsLink, closeable);
            }
        } else {
            if (link.startsWith(IPFS.IPFS_PATH)) {
                // try to store value
                books.storeDnsLink(uri.toString(), link);
                return resolveDnsLink(session, uri, link, closeable);
            } else if (link.startsWith(IPFS.IPNS_PATH)) {
                return resolveHost(session, uri,
                        link.replaceFirst(IPFS.IPNS_PATH, ""),
                        closeable);

            } else {
                throw new DOCS.ResolveNameException(uri.toString());
            }
        }
    }

    @NonNull
    private Cid resolveUri(@NonNull Session session, @NonNull Uri uri, @NonNull Cancellable closeable)
            throws Exception {
        String host = uri.getHost();
        Objects.requireNonNull(host);

        if (!Objects.equals(uri.getScheme(), Content.IPNS)) {
            throw new RuntimeException();
        }
        try {
            PeerId peerId = ipfs.decodePeerId(host);
            return resolveName(session, uri, peerId, closeable);
        } catch (Throwable ignore) {
            return resolveHost(session, uri, host, closeable);
        }
    }

    @NonNull
    private Cid getRoot(@NonNull Session session, @NonNull Uri uri, @NonNull Cancellable cancellable)
            throws Exception {
        String host = uri.getHost();
        Objects.requireNonNull(host);

        Cid root;
        if (Objects.equals(uri.getScheme(), Content.IPNS)) {
            root = resolveUri(session, uri, cancellable);
        } else {
            try {
                root = Cid.decode(host);
            } catch (Throwable throwable) {
                throw new InvalidNameException(uri.toString());
            }
        }

        if (!root.isSupported()) {
            throw new ResolveNameException("Encoding type '" + root.getPrefix().getType().name() +
                    "' is not supported." +
                    "Currently only 'sha2_256' encoded CID's are supported.");
        }

        return root;
    }

    @NonNull
    public WebResourceResponse getResponse(@NonNull Session session, @NonNull Context context,
                                           @NonNull Uri uri, @NonNull Cancellable cancellable)
            throws Exception {

        List<String> paths = uri.getPathSegments();
        Cid root = getRoot(session, uri, cancellable);
        return getResponse(session, context, uri, root, paths, cancellable);
    }

    @NonNull
    private Cid resolveDnsLink(@NonNull Session session, @NonNull Uri uri, @NonNull String link,
                               @NonNull Cancellable closeable)
            throws Exception {

        List<String> paths = uri.getPathSegments();
        if (link.startsWith(IPFS.IPFS_PATH)) {
            return Cid.decode(link.replaceFirst(IPFS.IPFS_PATH, ""));
        } else if (link.startsWith(IPFS.IPNS_PATH)) {
            String pid = link.replaceFirst(IPFS.IPNS_PATH, "");
            try {
                PeerId peerId = ipfs.decodePeerId(pid);
                // is is assume like /ipns/<dns_link> = > therefore <dns_link> is url
                return resolveName(session, uri, peerId, closeable);
            } catch (Throwable ignore) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme(Content.IPNS).authority(pid);
                for (String path : paths) {
                    builder.appendPath(path);
                }
                return resolveUri(session, builder.build(), closeable);
            }
        } else {
            // is is assume that links is  <dns_link> is url

            Uri dnsUri = Uri.parse(link);
            if (dnsUri != null) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme(Content.IPNS)
                        .authority(dnsUri.getAuthority());
                for (String path : paths) {
                    builder.appendPath(path);
                }
                return resolveUri(session, builder.build(), closeable);
            }
        }
        throw new ResolveNameException(uri.toString());
    }

    @NonNull
    public Uri redirectUri(@NonNull Session session, @NonNull Uri uri, @NonNull Cancellable closeable)
            throws Exception {


        if (Objects.equals(uri.getScheme(), Content.IPNS) ||
                Objects.equals(uri.getScheme(), Content.IPFS)) {
            List<String> paths = uri.getPathSegments();
            Cid root = getRoot(session, uri, closeable);
            return redirect(session, uri, root, paths, closeable);
        }
        return uri;
    }

    @NonNull
    private Uri redirect(@NonNull Session session, @NonNull Uri uri, @NonNull Cid root,
                         @NonNull List<String> paths, @NonNull Cancellable closeable)
            throws Exception {


        Cid cid = ipfs.resolveCid(session, root, paths, closeable);

        if (ipfs.isDir(session, cid, closeable)) {
            boolean exists = ipfs.hasLink(session, cid, IPFS.INDEX_HTML, closeable);

            if (exists) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme(uri.getScheme())
                        .authority(uri.getAuthority());
                for (String path : paths) {
                    builder.appendPath(path);
                }
                builder.appendPath(IPFS.INDEX_HTML);
                return builder.build();
            }
        }


        return uri;
    }


    public void cleanupResolver(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), Content.IPNS)) {
                String host = getHost(uri);
                if (host != null) {
                    PeerId peerId = ipfs.decodePeerId(host);
                    resolves.remove(peerId);
                }
            }
        } catch (Throwable ignore) {
            // ignore common failure
        }
    }


    public static class ResolveNameException extends Exception {

        public ResolveNameException(@NonNull String name) {
            super("Resolve name failed for " + name);
        }
    }

    public static class InvalidNameException extends Exception {

        public InvalidNameException(@NonNull String name) {
            super("Invalid name detected for " + name);
        }


    }
}
