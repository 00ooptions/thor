package threads.thor.provider;

import android.content.Context;
import android.os.Environment;

import androidx.annotation.NonNull;

import java.io.File;

import threads.thor.LogUtils;

public class FileProvider {
    private static final String TAG = FileProvider.class.getSimpleName();
    private static volatile FileProvider INSTANCE = null;

    private final File mDataDir;

    private FileProvider(@NonNull Context context) {

        mDataDir = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
    }


    public static FileProvider getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (FileProvider.class) {
                if (INSTANCE == null) {
                    INSTANCE = new FileProvider(context);
                }
            }
        }
        return INSTANCE;
    }


    public File getDataDir() {
        if (!mDataDir.exists()) {
            boolean result = mDataDir.mkdir();
            if (!result) {
                throw new RuntimeException("image directory does not exists");
            }
        }
        return mDataDir;
    }

    public void cleanDataDir() {
        deleteFile(getDataDir(), false);
    }

    private void deleteFile(@NonNull File root, boolean deleteWhenDir) {
        try {
            if (root.isDirectory()) {
                File[] files = root.listFiles();
                if (files != null) {
                    for (File file : files) {
                        if (file.isDirectory()) {
                            deleteFile(file, true);
                            boolean result = file.delete();
                            if (!result) {
                                LogUtils.error(TAG, "File " + file.getName() + " not deleted");
                            }
                        } else {
                            boolean result = file.delete();
                            if (!result) {
                                LogUtils.error(TAG, "File " + file.getName() + " not deleted");
                            }
                        }
                    }
                }
                if (deleteWhenDir) {
                    boolean result = root.delete();
                    if (!result) {
                        LogUtils.error(TAG, "File " + root.getName() + " not deleted");
                    }
                }
            } else {
                boolean result = root.delete();
                if (!result) {
                    LogUtils.error(TAG, "File " + root.getName() + " not deleted");
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}
