package threads.thor.provider;

import static android.provider.DocumentsContract.QUERY_ARG_DISPLAY_NAME;
import static android.provider.DocumentsContract.QUERY_ARG_MIME_TYPES;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.DocumentsProvider;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.services.MimeTypeService;

public class DataProvider extends DocumentsProvider {

    private static final String TAG = DataProvider.class.getSimpleName();
    private static final String ROOT_ID = "downloads";
    private static final String DOCUMENTS_AUTHORITY = "threads.thor.documents";
    private static final String[] DEFAULT_DOCUMENT_PROJECTION = new String[]{
            DocumentsContract.Document.COLUMN_DOCUMENT_ID,
            DocumentsContract.Document.COLUMN_MIME_TYPE,
            DocumentsContract.Document.COLUMN_DISPLAY_NAME,
            DocumentsContract.Document.COLUMN_LAST_MODIFIED,
            DocumentsContract.Document.COLUMN_FLAGS,
            DocumentsContract.Document.COLUMN_SIZE
    };

    private String downloads;
    private FileProvider fileProvider;

    private static String[] resolveRootProjection(String[] projection) {
        String[] DEFAULT_ROOT_PROJECTION = new String[]{
                DocumentsContract.Root.COLUMN_ROOT_ID,
                DocumentsContract.Root.COLUMN_ICON,
                DocumentsContract.Root.COLUMN_TITLE,
                DocumentsContract.Root.COLUMN_DOCUMENT_ID,
                DocumentsContract.Root.COLUMN_FLAGS,

        };
        return projection != null ? projection : DEFAULT_ROOT_PROJECTION;
    }

    private static String[] resolveDocumentProjection(String[] projection) {
        return projection != null ? projection : DEFAULT_DOCUMENT_PROJECTION;
    }


    public static Uri getDownloadsUri() {
        return DocumentsContract.buildRootUri(DOCUMENTS_AUTHORITY, ROOT_ID);
    }


    @Override
    public boolean isChildDocument(String parentDocumentId, String documentId) {
        LogUtils.info(TAG, "isChildDocument : " + documentId + " " + parentDocumentId);
        try {
            File directory = getFile(parentDocumentId);
            File child = getFile(documentId);
            return Objects.equals(child.getParentFile(), directory);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    @Override
    public Cursor queryRecentDocuments(String rootId, String[] projection) {

        final MatrixCursor result = new MatrixCursor(resolveDocumentProjection(projection));
        try {
            if (Objects.equals(ROOT_ID, rootId)) {
                List<File> files = getAllDataChildren();

                files.stream().sorted(Comparator.comparingLong(File::lastModified))
                        .limit(15).forEach(child -> includeFile(result, child));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        return result;
    }

    @Override
    public Cursor querySearchDocuments(@NonNull String rootId,
                                       @Nullable String[] projection, @NonNull Bundle queryArgs)
            throws FileNotFoundException {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            String query = queryArgs.getString(QUERY_ARG_DISPLAY_NAME, "");

            LogUtils.error(TAG, queryArgs.toString());
            List<String> mimeTypes = new ArrayList<>();

            String[] mimes = queryArgs.getStringArray(QUERY_ARG_MIME_TYPES);
            if (mimes != null) {
                for (String mimeType : mimes) {
                    if (mimeType.endsWith("/*")) {
                        mimeTypes.add(mimeType.replace("/*", ""));
                    } else {
                        mimeTypes.add(mimeType);
                    }
                }
            }

            return queryDocuments(rootId, query, projection, mimeTypes);
        } else {
            return super.querySearchDocuments(rootId, projection, queryArgs);
        }
    }

    @NonNull
    private String getDocumentId(@NonNull File file) {
        return file.getAbsolutePath().replace(
                Objects.requireNonNull(fileProvider.getDataDir().getParent()), "");
    }

    @NonNull
    private File getFile(@NonNull String documentId) {
        String[] paths = documentId.split(File.pathSeparator);
        File root = fileProvider.getDataDir().getParentFile();
        Objects.requireNonNull(root);
        for (String path : paths) {
            root = new File(root, path);
        }
        return root;
    }

    private boolean isMimeType(File file, List<String> mimeTypes) {

        if (mimeTypes.isEmpty()) {
            return true;
        }
        String mimeType;
        if (file.isDirectory()) {
            mimeType = DocumentsContract.Document.MIME_TYPE_DIR;
        } else {
            mimeType = MimeTypeService.getMimeType(file.getName());
        }
        if (mimeTypes.contains(mimeType)) {
            return true;
        }
        for (String mime : mimeTypes) {
            if (mimeType.startsWith(mime)) {
                return true;
            }
        }

        return false;

    }

    @NonNull
    public Cursor queryDocuments(String rootId, String query,
                                 String[] projection, List<String> mimeTypes) {
        final MatrixCursor result = new MatrixCursor(resolveDocumentProjection(projection));

        if (Objects.equals(ROOT_ID, rootId)) {
            List<File> children = getAllDataChildren();

            for (File file : children) {
                if (file.getName().contains(query)) {
                    if (isMimeType(file, mimeTypes)) {
                        includeFile(result, file);
                    }
                }
            }
        }
        return result;
    }

    @Override
    public Cursor queryRoots(String[] projection) {
        MatrixCursor result = new MatrixCursor(resolveRootProjection(projection));

        MatrixCursor.RowBuilder row = result.newRow();
        row.add(DocumentsContract.Root.COLUMN_ROOT_ID, ROOT_ID);
        row.add(DocumentsContract.Root.COLUMN_ICON, R.mipmap.ic_launcher_round);
        row.add(DocumentsContract.Root.COLUMN_TITLE, downloads);
        row.add(DocumentsContract.Root.COLUMN_FLAGS,
                DocumentsContract.Root.FLAG_LOCAL_ONLY |
                        DocumentsContract.Root.FLAG_SUPPORTS_IS_CHILD |
                        DocumentsContract.Root.FLAG_SUPPORTS_RECENTS |
                        DocumentsContract.Root.FLAG_SUPPORTS_SEARCH);
        row.add(DocumentsContract.Root.COLUMN_DOCUMENT_ID,
                getDocumentId(fileProvider.getDataDir()));
        row.add(DocumentsContract.Root.COLUMN_MIME_TYPES, "*/*");
        return result;
    }


    private void includeFile(MatrixCursor result, File file) {
        if (!file.exists()) {
            return;
        }

        String mimeType;
        if (file.isDirectory()) {
            mimeType = DocumentsContract.Document.MIME_TYPE_DIR;
        } else {
            mimeType = MimeTypeService.getMimeType(file.getName());
        }

        MatrixCursor.RowBuilder row = result.newRow();
        row.add(DocumentsContract.Document.COLUMN_DOCUMENT_ID, getDocumentId(file));
        row.add(DocumentsContract.Document.COLUMN_DISPLAY_NAME, file.getName());
        row.add(DocumentsContract.Document.COLUMN_SIZE, file.length());
        row.add(DocumentsContract.Document.COLUMN_MIME_TYPE, mimeType);
        row.add(DocumentsContract.Document.COLUMN_LAST_MODIFIED, file.lastModified());
        row.add(DocumentsContract.Document.COLUMN_FLAGS,
                DocumentsContract.Document.FLAG_SUPPORTS_DELETE |
                        DocumentsContract.Document.FLAG_SUPPORTS_REMOVE);
    }

    @Override
    public Cursor queryDocument(String documentId, String[] projection) {
        LogUtils.error(TAG, "queryDocument : " + documentId);
        MatrixCursor result = new MatrixCursor(resolveDocumentProjection(projection));
        try {
            File file = getFile(documentId);
            includeFile(result, file);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return result;
    }

    @Override
    public void removeDocument(String documentId, String parentDocumentId) throws FileNotFoundException {
        deleteDocument(documentId);
    }

    @Override
    public void deleteDocument(String documentId) throws FileNotFoundException {
        LogUtils.info(TAG, "deleteDocument : " + documentId);
        try {
            File file = getFile(documentId);
            boolean success = file.delete();
            if (!success) {
                LogUtils.error(TAG, "File could not be deleted");
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            throw new FileNotFoundException(throwable.getLocalizedMessage());
        }
    }

    @Override
    public Cursor queryChildDocuments(String parentDocumentId, String[] projection, String sortOrder) {
        MatrixCursor result = new MatrixCursor(resolveDocumentProjection(projection));
        try {
            File directory = getFile(parentDocumentId);
            if (directory.exists()) {
                File[] files = directory.listFiles();
                if (files != null) {
                    for (File file : files) {
                        includeFile(result, file);
                    }
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        return result;
    }


    @NonNull
    private List<String> getPaths(@Nullable String parentDocumentId, @NonNull String childDocumentId) {
        File cmpParent = null;
        if (parentDocumentId != null) {
            cmpParent = getFile(parentDocumentId);
            if (!cmpParent.exists()) {
                cmpParent = null;
            }
        }
        List<String> paths = new ArrayList<>();
        File file = getFile(childDocumentId);
        if (!file.exists()) {
            return paths;
        }
        if (Objects.equals(file, fileProvider.getDataDir())) {
            return paths;
        }
        File parent = file.getParentFile();
        while (parent != null) {

            if (!Objects.equals(parent, cmpParent)) {
                paths.add(getDocumentId(parent));
                if (Objects.equals(parent, fileProvider.getDataDir())) {
                    break;
                }
            } else {
                break;
            }
            parent = file.getParentFile();
        }
        return paths;
    }


    @NonNull
    public DocumentsContract.Path findDocumentPath(@Nullable String parentDocumentId, String childDocumentId)
            throws FileNotFoundException {
        LogUtils.info(TAG, "" + parentDocumentId + " " + childDocumentId);

        try {
            List<String> paths = getPaths(parentDocumentId, childDocumentId);
            return new DocumentsContract.Path(DOCUMENTS_AUTHORITY, paths);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            throw new FileNotFoundException(throwable.getLocalizedMessage());
        }
    }

    @Override
    public ParcelFileDescriptor openDocument(String documentId, String mode,
                                             @Nullable CancellationSignal signal) throws FileNotFoundException {

        LogUtils.error(TAG, "openDocument : " + documentId);
        try {
            File file = getFile(documentId);
            return ParcelFileDescriptor.open(file, ParcelFileDescriptor.parseMode(mode));
        } catch (Throwable throwable) {
            throw new FileNotFoundException(throwable.getLocalizedMessage());
        }
    }

    @NonNull
    private List<File> getAllDataChildren() {
        return getAllChildren(fileProvider.getDataDir());
    }

    @NonNull
    private List<File> getAllChildren(File root) {
        List<File> children = new ArrayList<>();
        try {
            if (root.isDirectory()) {
                File[] files = root.listFiles();
                if (files != null) {
                    for (File file : files) {
                        if (file.isDirectory()) {
                            children.addAll(getAllChildren(file));
                        } else {
                            children.add(file);
                        }
                    }
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return children;
    }

    @Override
    public boolean onCreate() {
        Context context = getContext();
        Objects.requireNonNull(context);
        downloads = context.getString(R.string.downloads);
        fileProvider = FileProvider.getInstance(context);
        return true;
    }

}
