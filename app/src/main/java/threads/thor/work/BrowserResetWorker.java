package threads.thor.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.webkit.CookieManager;
import android.webkit.WebViewDatabase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.File;

import threads.lite.blockstore.BLOCKS;
import threads.lite.pagestore.PAGES;
import threads.thor.InitApplication;
import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.provider.FileProvider;

public class BrowserResetWorker extends Worker {

    private static final String TAG = BrowserResetWorker.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    public BrowserResetWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork() {
        return new OneTimeWorkRequest.Builder(BrowserResetWorker.class).build();

    }

    public static void reset(@NonNull Context context) {

        WorkManager.getInstance(context).enqueueUniqueWork(
                TAG, ExistingWorkPolicy.REPLACE, getWork());

    }

    private void deleteCache(@NonNull Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private boolean deleteDir(@Nullable File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            if (children != null) {
                for (String child : children) {
                    boolean success = deleteDir(new File(dir, child));
                    if (!success) {
                        return false;
                    }
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start ...");

        try {

            NotificationManager notificationManager = (NotificationManager)
                    getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                    InitApplication.CLEANUP_CHANNEL_ID);


            PendingIntent cancelPendingIntent = WorkManager.getInstance(getApplicationContext())
                    .createCancelPendingIntent(getId());
            String cancel = getApplicationContext().getString(android.R.string.cancel);

            Intent intent = InitApplication.getDownloadsIntent();

            int requestID = (int) System.currentTimeMillis();
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

            Notification.Action action = new Notification.Action.Builder(
                    Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                    cancelPendingIntent).build();

            builder.setContentTitle(getApplicationContext().getString(R.string.action_cleanup))
                    .setContentIntent(pendingIntent)
                    .setOnlyAlertOnce(true)
                    .setSmallIcon(R.drawable.delete_outline)
                    .addAction(action)
                    .setCategory(Notification.CATEGORY_SERVICE)
                    .setUsesChronometer(true);


            Notification notification = builder.build();

            int notificationId = getId().hashCode();

            notificationManager.notify(notificationId, notification);
            setForegroundAsync(new ForegroundInfo(notificationId, notification));

            // Clear all the cookies
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();


            // clears passwords
            WebViewDatabase.getInstance(getApplicationContext()).clearHttpAuthUsernamePassword();

            // Clear downloads data
            FileProvider fileProvider = FileProvider.getInstance(getApplicationContext());
            fileProvider.cleanDataDir();

            // Clear ipfs and pages data
            BLOCKS.getInstance(getApplicationContext()).clear();
            PAGES.getInstance(getApplicationContext()).clear();

            deleteCache(getApplicationContext());


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();
    }
}

