package threads.thor.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.core.Link;
import threads.lite.core.Progress;
import threads.lite.core.Session;
import threads.thor.InitApplication;
import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.core.Content;
import threads.thor.core.DOCS;
import threads.thor.core.locals.LOCALS;
import threads.thor.provider.FileProvider;
import threads.thor.services.MimeTypeService;

public class DownloadContentWorker extends Worker {

    private static final String TAG = DownloadContentWorker.class.getSimpleName();


    private final AtomicBoolean success = new AtomicBoolean(true);

    @SuppressWarnings("WeakerAccess")
    public DownloadContentWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri content) {

        Data.Builder data = new Data.Builder();
        data.putString(Content.ADDR, content.toString());

        return new OneTimeWorkRequest.Builder(DownloadContentWorker.class)
                .setInputData(data.build())
                .setInitialDelay(1, TimeUnit.MILLISECONDS)
                .build();
    }

    public static void download(@NonNull Context context, @NonNull Uri content) {
        WorkManager.getInstance(context).enqueue(getWork(content));
    }

    @NonNull
    @Override
    public Result doWork() {


        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... ");

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            DOCS docs = DOCS.getInstance(getApplicationContext());
            LOCALS locals = LOCALS.getInstance();
            FileProvider fileProvider = FileProvider.getInstance(getApplicationContext());

            try (Session session = ipfs.createSession(true)) {

                locals.getLocals().parallelStream().forEach(multiaddr -> {
                    try {
                        if (!Objects.equals(multiaddr.getPeerId(), ipfs.self())) {
                            LogUtils.error(TAG, "try " + multiaddr);
                            session.connect(multiaddr, ipfs.getConnectionParameters());
                        }
                    } catch (Throwable ignore) {
                    }
                });


                String url = getInputData().getString(Content.ADDR);
                Objects.requireNonNull(url);
                Uri uri = Uri.parse(url);

                String name = DOCS.getFileName(uri);

                NotificationManager notificationManager = (NotificationManager)
                        getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                        InitApplication.STORAGE_CHANNEL_ID);


                PendingIntent cancelPendingIntent = WorkManager.getInstance(getApplicationContext())
                        .createCancelPendingIntent(getId());
                String cancel = getApplicationContext().getString(android.R.string.cancel);

                Intent intent = InitApplication.getDownloadsIntent();

                int requestID = (int) System.currentTimeMillis();
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                        intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

                Notification.Action action = new Notification.Action.Builder(
                        Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                        cancelPendingIntent).build();

                builder.setContentTitle(name)
                        .setSubText("" + 0 + "%")
                        .setContentIntent(pendingIntent)
                        .setProgress(100, 0, false)
                        .setOnlyAlertOnce(true)
                        .setSmallIcon(R.drawable.download)
                        .addAction(action)
                        .setCategory(Notification.CATEGORY_PROGRESS)
                        .setUsesChronometer(true)
                        .setOngoing(true);


                Notification notification = builder.build();

                int notificationId = getId().hashCode();

                notificationManager.notify(notificationId, notification);
                setForegroundAsync(new ForegroundInfo(notificationId, notification));


                try {
                    Cid content = docs.getContent(session, uri, this::isStopped);
                    Objects.requireNonNull(content);
                    String mimeType = docs.getMimeType(session, getApplicationContext(),
                            uri, content, this::isStopped);

                    File doc;
                    if (Objects.equals(mimeType, MimeTypeService.DIR_MIME_TYPE)) {
                        doc = new File(fileProvider.getDataDir(), name);
                        if (!doc.exists()) {
                            boolean created = doc.mkdir();
                            if (!created) {
                                throw new IllegalArgumentException("Directory couldn't be created");
                            }
                        }
                    } else {
                        doc = new File(fileProvider.getDataDir(), name);
                        if (!doc.exists()) {
                            boolean created = doc.createNewFile();
                            if (!created) {
                                throw new IllegalArgumentException("File couldn't be created");
                            }
                        }
                    }
                    if (doc.isDirectory()) {
                        downloadLinks(session, doc, builder, content, notificationId);
                    } else {
                        download(session, doc, builder, content, notificationId);
                    }


                    if (!isStopped()) {
                        if (!success.get()) {
                            if (!isStopped()) {
                                builder.setContentText(getApplicationContext()
                                                .getString(R.string.download_failed, name))
                                        .setSubText("")
                                        .setProgress(0, 0, false);
                                notificationManager.notify(notificationId, builder.build());
                            }
                        } else {
                            builder.setContentText(getApplicationContext().
                                            getString(R.string.download_complete, name))
                                    .setSubText("")
                                    .setProgress(0, 0, false);
                            notificationManager.notify(notificationId, builder.build());
                        }
                    }

                } catch (Throwable e) {
                    if (!isStopped()) {
                        builder.setContentText(getApplicationContext()
                                        .getString(R.string.download_failed, name))
                                .setSubText("")
                                .setProgress(0, 0, false);
                        notificationManager.notify(notificationId, builder.build());
                    }
                    throw e;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" +
                    (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }

    private void download(@NonNull Session session, @NonNull File file,
                          @NonNull Notification.Builder builder,
                          @NonNull Cid cid, int notificationId) throws Exception {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start [" + (System.currentTimeMillis() - start) + "]...");


        String name = file.getName();
        Objects.requireNonNull(name);
        IPFS ipfs = IPFS.getInstance(getApplicationContext());
        NotificationManager notificationManager = (NotificationManager)
                getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (!ipfs.isDir(session, cid, this::isStopped)) {
            try {
                ipfs.fetchToFile(session, file, cid, new Progress() {
                    @Override
                    public boolean isCancelled() {
                        return isStopped();
                    }

                    @Override
                    public void setProgress(int progress) {
                        builder.setSubText("" + progress + "%")
                                .setContentText(name)
                                .setProgress(100, progress, false);
                        notificationManager.notify(notificationId, builder.build());
                    }
                });

            } catch (Throwable throwable) {
                success.set(false);

                boolean deleted = file.delete();
                if (!deleted) {
                    LogUtils.error(TAG, "File could not be deleted");
                }

            } finally {
                LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
            }
        }
    }


    private void evalLinks(@NonNull Session session, @NonNull File parent,
                           @NonNull Notification.Builder builder,
                           @NonNull List<Link> links, int notificationId) throws Exception {

        IPFS ipfs = IPFS.getInstance(getApplicationContext());

        for (Link link : links) {
            if (!isStopped()) {
                Cid cid = link.getCid();
                if (ipfs.isDir(session, cid, this::isStopped)) {
                    File dir = new File(parent, link.getName());
                    if (!dir.exists()) {
                        boolean created = dir.mkdir();
                        if (!created) {
                            throw new IllegalArgumentException("File not created");
                        }
                    }
                    downloadLinks(session, dir, builder, cid, notificationId);
                } else {
                    File file = new File(parent, link.getName());
                    if (!file.exists()) {
                        boolean created = file.createNewFile();
                        if (!created) {
                            throw new IllegalArgumentException("File not created");
                        }
                    }
                    download(session, file, builder, cid, notificationId);
                }
            }
        }

    }


    private void downloadLinks(@NonNull Session session, @NonNull File parent,
                               @NonNull Notification.Builder builder,
                               @NonNull Cid content, int notificationId) throws Exception {
        IPFS ipfs = IPFS.getInstance(getApplicationContext());

        List<Link> links = ipfs.links(session, content, false, this::isStopped);

        evalLinks(session, parent, builder, links, notificationId);

    }

}
