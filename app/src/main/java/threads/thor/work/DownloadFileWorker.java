package threads.thor.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.File;
import java.net.URL;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.core.Progress;
import threads.thor.InitApplication;
import threads.thor.R;
import threads.thor.core.Content;
import threads.thor.provider.FileProvider;

public class DownloadFileWorker extends Worker {

    private static final String TAG = DownloadFileWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public DownloadFileWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri source, @NonNull String filename,
                                              @NonNull String mimeType, long size) {
        Data.Builder data = new Data.Builder();
        data.putString(Content.NAME, filename);
        data.putString(Content.TYPE, mimeType);
        data.putLong(Content.SIZE, size);
        data.putString(Content.FILE, source.toString());

        return new OneTimeWorkRequest.Builder(DownloadFileWorker.class)
                .setInputData(data.build())
                .setInitialDelay(1, TimeUnit.MILLISECONDS)
                .build();
    }

    public static void download(@NonNull Context context, @NonNull Uri source,
                                @NonNull String filename, @NonNull String mimeType, long size) {
        WorkManager.getInstance(context).enqueue(getWork(source, filename, mimeType, size));
    }


    @Override
    public void onStopped() {
    }


    @NonNull
    @Override
    public Result doWork() {


        long start = System.currentTimeMillis();
        threads.lite.LogUtils.info(TAG, " start ... ");

        try {
            FileProvider fileProvider = FileProvider.getInstance(getApplicationContext());
            long size = getInputData().getLong(Content.SIZE, 0);
            String name = getInputData().getString(Content.NAME);
            Objects.requireNonNull(name);
            String mimeType = getInputData().getString(Content.TYPE);
            Objects.requireNonNull(mimeType);

            String url = getInputData().getString(Content.FILE);
            Objects.requireNonNull(url);
            Uri uri = Uri.parse(url);

            URL urlCon = new URL(uri.toString());


            NotificationManager notificationManager = (NotificationManager)
                    getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                    InitApplication.STORAGE_CHANNEL_ID);


            PendingIntent cancelPendingIntent = WorkManager.getInstance(getApplicationContext())
                    .createCancelPendingIntent(getId());
            String cancel = getApplicationContext().getString(android.R.string.cancel);

            Intent intent = InitApplication.getDownloadsIntent();

            int requestID = (int) System.currentTimeMillis();
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

            Notification.Action action = new Notification.Action.Builder(
                    Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                    cancelPendingIntent).build();

            builder.setContentTitle(name)
                    .setSubText("" + 0 + "%")
                    .setContentIntent(pendingIntent)
                    .setProgress(100, 0, false)
                    .setOnlyAlertOnce(true)
                    .setSmallIcon(R.drawable.download)
                    .addAction(action)
                    .setCategory(Notification.CATEGORY_PROGRESS)
                    .setUsesChronometer(true)
                    .setOngoing(true);


            Notification notification = builder.build();

            int notificationId = getId().hashCode();

            notificationManager.notify(notificationId, notification);
            setForegroundAsync(new ForegroundInfo(notificationId, notification));

            File file = new File(fileProvider.getDataDir(), name);
            if (!file.exists()) {
                boolean success = file.createNewFile();
                if (!success) {
                    throw new IllegalArgumentException("File could not be created");
                }
            }

            try {
                IPFS.downloadUrl(urlCon, file, 30, new Progress() {
                    @Override
                    public void setProgress(int progress) {
                        builder.setSubText("" + progress + "%")
                                .setProgress(100, progress, false);
                        notificationManager.notify(notificationId, builder.build());
                    }

                    @Override
                    public boolean isCancelled() {
                        return isStopped();
                    }

                }, size);

            } catch (Throwable e) {
                boolean success = file.delete();
                if (!success) {
                    LogUtils.error(TAG, "File could not be deleted");
                }
                if (!isStopped()) {
                    builder.setContentText(getApplicationContext()
                                    .getString(R.string.download_failed, name))
                            .setSubText("")
                            .setProgress(0, 0, false);
                    notificationManager.notify(notificationId, builder.build());
                }
                throw e;
            }

            builder.setContentText(getApplicationContext().
                            getString(R.string.download_complete, name))
                    .setSubText("")
                    .setProgress(0, 0, false);
            notificationManager.notify(notificationId, builder.build());

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();

    }

}
