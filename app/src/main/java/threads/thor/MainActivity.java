package threads.thor;


import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewDatabase;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.ListPopupWindow;
import androidx.appcompat.widget.SearchView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textview.MaterialTextView;
import com.journeyapps.barcodescanner.ScanContract;
import com.journeyapps.barcodescanner.ScanOptions;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import threads.lite.cid.Cid;
import threads.lite.core.Cancellable;
import threads.lite.core.Session;
import threads.magnet.magnet.MagnetUri;
import threads.magnet.magnet.MagnetUriParser;
import threads.thor.core.Content;
import threads.thor.core.DOCS;
import threads.thor.core.books.BOOKS;
import threads.thor.core.books.Bookmark;
import threads.thor.core.events.EVENTS;
import threads.thor.fragments.ActionListener;
import threads.thor.fragments.BookmarksDialogFragment;
import threads.thor.fragments.ContentDialogFragment;
import threads.thor.fragments.HistoryDialogFragment;
import threads.thor.fragments.SettingsDialogFragment;
import threads.thor.model.EventViewModel;
import threads.thor.services.MimeTypeService;
import threads.thor.utils.AdBlocker;
import threads.thor.utils.CustomWebChromeClient;
import threads.thor.utils.PermissionAction;
import threads.thor.utils.SearchesAdapter;
import threads.thor.work.BrowserResetWorker;
import threads.thor.work.DownloadContentWorker;
import threads.thor.work.DownloadFileWorker;
import threads.thor.work.DownloadMagnetWorker;


public class MainActivity extends AppCompatActivity implements
        ActionListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final long CLICK_OFFSET = 500;

    private final ActivityResultLauncher<Intent> mFolderRequestForResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();

                    try {
                        Objects.requireNonNull(data);
                        Uri uri = data.getData();
                        Objects.requireNonNull(uri);


                        String mimeType = getContentResolver().getType(uri);


                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(uri, mimeType);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                        startActivity(intent);

                    } catch (Throwable e) {
                        EVENTS.getInstance(getApplicationContext()).warning(
                                getString(R.string.no_activity_found_to_handle_uri));
                    }
                }
            });

    private WebView webView;
    private long lastClickTime = 0;
    private MaterialTextView browserText;
    private ActionMode actionMode;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearProgressIndicator progressIndicator;
    private AppBarLayout appBarLayout;
    private final ActivityResultLauncher<ScanOptions>
            mScanRequestForResult = registerForActivityResult(new ScanContract(),
            result -> {
                if (result.getContents() != null) {
                    try {
                        Uri uri = Uri.parse(result.getContents());
                        if (uri != null) {
                            String scheme = uri.getScheme();
                            if (Objects.equals(scheme, Content.IPNS) ||
                                    Objects.equals(scheme, Content.IPFS) ||
                                    Objects.equals(scheme, Content.HTTP) ||
                                    Objects.equals(scheme, Content.HTTPS)) {
                                openUri(uri);
                            } else {
                                EVENTS.getInstance(getApplicationContext()).error(
                                        getString(R.string.codec_not_supported));
                            }
                        } else {
                            EVENTS.getInstance(getApplicationContext()).error(
                                    getString(R.string.codec_not_supported));
                        }
                    } catch (Throwable throwable) {
                        EVENTS.getInstance(getApplicationContext()).error(
                                getString(R.string.codec_not_supported));
                    }
                }
            });
    private final ActivityResultLauncher<String> requestPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                if (isGranted) {
                    invokeScan();
                } else {
                    EVENTS.getInstance(getApplicationContext()).permission(
                            getString(R.string.permission_camera_denied));
                }
            });
    private boolean hasCamera;


    private void contentDownloader(@NonNull Uri uri) {

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        builder.setTitle(R.string.download_title);
        builder.setMessage(DOCS.getFileName(uri));

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {
            DownloadContentWorker.download(getApplicationContext(), uri);

            progressIndicator.setVisibility(View.GONE);
        });
        builder.setNeutralButton(getString(android.R.string.cancel),
                (dialog, which) -> {
                    progressIndicator.setVisibility(View.GONE);
                    dialog.cancel();
                });
        builder.show();


    }

    private void magnetDownloader(@NonNull Uri uri, @NonNull String name) {


        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        builder.setTitle(R.string.download_title);
        builder.setMessage(name);

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {

            DownloadMagnetWorker.download(getApplicationContext(), uri);

            progressIndicator.setVisibility(View.GONE);
        });
        builder.setNeutralButton(getString(android.R.string.cancel),
                (dialog, which) -> {
                    progressIndicator.setVisibility(View.GONE);
                    dialog.cancel();
                });
        builder.show();


    }

    private void fileDownloader(@NonNull Uri uri, @NonNull String filename,
                                @NonNull String mimeType, long size) {

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        builder.setTitle(R.string.download_title);
        builder.setMessage(filename);

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {

            DownloadFileWorker.download(getApplicationContext(), uri, filename, mimeType, size);

            progressIndicator.setVisibility(View.GONE);

        });
        builder.setNeutralButton(getString(android.R.string.cancel),
                (dialog, which) -> {
                    progressIndicator.setVisibility(View.GONE);
                    dialog.cancel();
                });
        builder.show();

    }

    public void reload() {

        try {
            progressIndicator.setVisibility(View.GONE);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        try {
            DOCS.getInstance(getApplicationContext())
                    .cleanupResolver(Uri.parse(webView.getUrl()));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        try {
            webView.reload();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    private void invokeScan() {
        try {
            PackageManager pm = getPackageManager();

            if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
                ScanOptions options = new ScanOptions();
                options.setDesiredBarcodeFormats(ScanOptions.ALL_CODE_TYPES);
                options.setPrompt(getString(R.string.scan_url));
                options.setCameraId(0);  // Use a specific camera of the device
                options.setBeepEnabled(true);
                options.setOrientationLocked(false);
                mScanRequestForResult.launch(options);
            } else {
                EVENTS.getInstance(getApplicationContext()).permission(
                        getString(R.string.feature_camera_required));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private ActionMode.Callback createFindActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_find_action_mode, menu);


                MenuItem action_mode_find = menu.findItem(R.id.action_mode_find);
                EditText mEditText = (EditText) action_mode_find.getActionView();

                mEditText.setMinHeight(dp48ToPixels());
                mEditText.setWidth(400);
                mEditText.setBackgroundResource(android.R.color.transparent);
                mEditText.setSingleLine();
                mEditText.setTextSize(16);
                mEditText.setHint(R.string.find_page);
                mEditText.setFocusable(true);
                mEditText.requestFocus();

                mEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        webView.findAllAsync(editable.toString());
                    }
                });


                mode.setTitle("0/0");

                webView.setFindListener((activeMatchOrdinal, numberOfMatches, isDoneCounting) -> {
                    try {
                        String result = "" + activeMatchOrdinal + "/" + numberOfMatches;
                        mode.setTitle(result);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                });

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

                if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                    return true;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                int itemId = item.getItemId();

                if (itemId == R.id.action_mode_previous) {
                    try {
                        webView.findNext(false);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                    return true;
                } else if (itemId == R.id.action_mode_next) {
                    try {
                        webView.findNext(true);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                    return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                try {
                    webView.clearMatches();
                    webView.setFindListener(null);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                } finally {
                    actionMode = null;
                }
            }
        };

    }

    public boolean onBackPressedCheck() {

        if (webView.canGoBack()) {
            goBack();
            return true;
        }

        return false;
    }

    private String prettyUri(@NonNull Uri uri, @NonNull String replace) {
        return uri.toString().replaceFirst(replace, "");
    }

    private void updateUri(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), Content.HTTPS)) {
                browserText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        R.drawable.lock, 0, 0, 0
                );
                browserText.setText(prettyUri(uri, "https://"));
            } else if (Objects.equals(uri.getScheme(), Content.HTTP)) {
                browserText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        R.drawable.lock_open, 0, 0, 0
                );
                browserText.setText(prettyUri(uri, "http://"));
            } else {
                BOOKS books = BOOKS.getInstance(getApplicationContext());
                Bookmark bookmark = books.getBookmark(uri.toString());

                String title = uri.toString();
                if (bookmark != null) {
                    String bookmarkTitle = bookmark.getTitle();
                    if (!bookmarkTitle.isEmpty()) {
                        title = bookmarkTitle;
                    }
                }

                browserText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        R.drawable.lock, 0, 0, 0
                );
                browserText.setText(title);
            }


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void goBack() {
        try {
            webView.stopLoading();
            webView.goBack();
            appBarLayout.setExpanded(true, true);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void goForward() {
        try {
            webView.stopLoading();
            webView.goForward();
            appBarLayout.setExpanded(true, true);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private int dp48ToPixels() {
        float density = getResources().getDisplayMetrics().density;
        return Math.round((float) 48 * density);
    }

    private int actionBarSize() {
        TypedValue tv = new TypedValue();
        getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true);
        return getResources().getDimensionPixelSize(tv.resourceId);
    }


    private boolean isDarkTheme() {
        int nightModeFlags = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
        return nightModeFlags == Configuration.UI_MODE_NIGHT_YES;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        PackageManager pm = getPackageManager();
        hasCamera = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);

        CoordinatorLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        appBarLayout = findViewById(R.id.appbar);
        progressIndicator = findViewById(R.id.progress_bar);
        progressIndicator.setVisibility(View.GONE);
        webView = findViewById(R.id.web_view);
        swipeRefreshLayout = findViewById(R.id.swipe_container);

        appBarLayout.addOnOffsetChangedListener(new AppBarStateChangedListener() {
            @Override
            public void onStateChanged(State state) {
                if (state == State.EXPANDED) {
                    swipeRefreshLayout.setEnabled(true);
                } else if (state == State.COLLAPSED) {
                    swipeRefreshLayout.setEnabled(false);
                }

            }
        });


        Settings.setWebSettings(webView, Settings.isJavascriptEnabled(getApplicationContext()));

        CookieManager.getInstance().setAcceptThirdPartyCookies(webView, false);

        MaterialToolbar materialToolbar = findViewById(R.id.toolbar);
        materialToolbar.setNavigationOnClickListener(v -> {
            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                openUri(Uri.parse(Settings.HOMEPAGE));
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        Button mActionOverflow = findViewById(R.id.action_overflow);

        mActionOverflow.setOnClickListener(v -> {

            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                LayoutInflater inflater = (LayoutInflater)
                        getSystemService(LAYOUT_INFLATER_SERVICE);


                View menuOverflow = inflater.inflate(
                        R.layout.menu_overflow, mDrawerLayout, false);


                PopupWindow dialog = new PopupWindow(
                        MainActivity.this, null, android.R.attr.contextPopupMenuStyle);
                dialog.setContentView(menuOverflow);
                dialog.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.setOutsideTouchable(true);
                dialog.setFocusable(true);

                dialog.showAsDropDown(mActionOverflow, 0, -actionBarSize());

                // this is used in several actions
                final String url = webView.getUrl();

                MaterialButton actionNextPage = menuOverflow.findViewById(R.id.action_next_page);
                actionNextPage.setEnabled(webView.canGoForward());
                actionNextPage.setOnClickListener(v1 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        goForward();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                MaterialButton actionBookmark = menuOverflow.findViewById(R.id.action_bookmark);

                BOOKS books = BOOKS.getInstance(getApplicationContext());


                if (url != null && !url.isEmpty()) {
                    Uri uri = Uri.parse(url);
                    if (books.hasBookmark(uri.toString())) {
                        actionBookmark.setIcon(AppCompatResources.getDrawable(
                                getApplicationContext(), R.drawable.star));
                    } else {
                        actionBookmark.setIcon(AppCompatResources.getDrawable(
                                getApplicationContext(), R.drawable.star_outline));
                    }
                } else {
                    actionBookmark.setEnabled(false);
                }

                actionBookmark.setOnClickListener(v1 -> {

                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        Uri uri = Uri.parse(url);

                        Bookmark bookmark = books.getBookmark(uri.toString());
                        if (bookmark != null) {

                            String msg = bookmark.getTitle();
                            books.removeBookmark(bookmark);

                            if (msg.isEmpty()) {
                                msg = uri.toString();
                            }
                            EVENTS.getInstance(getApplicationContext()).warning(
                                    getString(R.string.bookmark_removed, msg));
                        } else {
                            Bitmap bitmap = webView.getFavicon();
                            String title = webView.getTitle();

                            if (title == null) {
                                title = uri.getHost();
                            }

                            bookmark = books.createBookmark(uri.toString(), title);
                            if (bitmap != null) {
                                bookmark.setBitmapIcon(bitmap);
                            }

                            books.storeBookmark(bookmark);

                            String msg = title;
                            if (msg.isEmpty()) {
                                msg = uri.toString();
                            }

                            EVENTS.getInstance(getApplicationContext()).warning(
                                    getString(R.string.bookmark_added, msg));

                            updateUri(uri);
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                });

                Button actionFindPage = menuOverflow.findViewById(R.id.action_find_page);

                actionFindPage.setOnClickListener(v12 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        actionMode = startSupportActionMode(
                                createFindActionModeCallback());

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                Button actionDownload = menuOverflow.findViewById(R.id.action_download);

                actionDownload.setEnabled(downloadActive());

                actionDownload.setOnClickListener(v13 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        download();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                Button actionShare = menuOverflow.findViewById(R.id.action_share);

                actionShare.setOnClickListener(v14 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        Uri uri = Uri.parse(url);

                        ComponentName[] names = {new ComponentName(getApplicationContext(), MainActivity.class)};

                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_link));
                        intent.putExtra(Intent.EXTRA_TEXT, uri.toString());
                        intent.setType(MimeTypeService.PLAIN_MIME_TYPE);
                        intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);


                        Intent chooser = Intent.createChooser(intent, getText(R.string.share));
                        chooser.putExtra(Intent.EXTRA_EXCLUDE_COMPONENTS, names);
                        startActivity(chooser);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                Button actionReload = menuOverflow.findViewById(R.id.action_reload);

                actionReload.setOnClickListener(v15 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        reload();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionInformation = menuOverflow.findViewById(R.id.action_information);
                actionInformation.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < 500) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        ContentDialogFragment.newInstance(webView.getUrl())
                                .show(getSupportFragmentManager(), ContentDialogFragment.TAG);


                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionHistory = menuOverflow.findViewById(R.id.action_history);
                actionHistory.setOnClickListener(v16 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        HistoryDialogFragment dialogFragment = new HistoryDialogFragment();
                        dialogFragment.show(getSupportFragmentManager(), HistoryDialogFragment.TAG);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionDownloads = menuOverflow.findViewById(R.id.action_downloads);
                actionDownloads.setOnClickListener(v17 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        showDownloads();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionCleanup = menuOverflow.findViewById(R.id.action_cleanup);
                actionCleanup.setOnClickListener(v18 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        webView.clearHistory();
                        webView.clearCache(true);
                        webView.clearFormData();

                        // Clear data and cookies
                        BrowserResetWorker.reset(getApplicationContext());

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionSettings = menuOverflow.findViewById(R.id.action_settings);
                actionSettings.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        SettingsDialogFragment dialogFragment = new SettingsDialogFragment();
                        dialogFragment.show(getSupportFragmentManager(), SettingsDialogFragment.TAG);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionDocumentation = menuOverflow.findViewById(R.id.action_documentation);
                actionDocumentation.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        String uri = "https://gitlab.com/remmer.wilts/thor";

                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri),
                                getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        Button mActionBookmarks = findViewById(R.id.action_bookmarks);
        mActionBookmarks.setOnClickListener(v -> {
            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                BookmarksDialogFragment dialogFragment = new BookmarksDialogFragment();
                dialogFragment.show(getSupportFragmentManager(), BookmarksDialogFragment.TAG);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        swipeRefreshLayout.setOnRefreshListener(() -> {
            try {
                swipeRefreshLayout.setRefreshing(true);
                reload();
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            } finally {
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        browserText = findViewById(R.id.action_browser);

        browserText.setOnClickListener(view -> {

            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                actionMode = startSupportActionMode(
                        createSearchActionModeCallback());

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        EventViewModel eventViewModel =
                new ViewModelProvider(this).get(EventViewModel.class);

        eventViewModel.getUri().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        openUri(Uri.parse(content));
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.getFatal().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(webView, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());

                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.getError().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(webView, content, Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        eventViewModel.getWarning().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(webView, content,
                                Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        eventViewModel.getPermission().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mDrawerLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(R.string.settings, new PermissionAction());
                        snackbar.show();

                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.getInfo().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Toast.makeText(getApplicationContext(), content, Toast.LENGTH_SHORT).show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        CustomWebChromeClient mCustomWebChromeClient = new CustomWebChromeClient(this);
        webView.setWebChromeClient(mCustomWebChromeClient);


        webView.setDownloadListener((url, userAgent, contentDisposition, mimeType, contentLength) -> {

            try {

                String filename = URLUtil.guessFileName(url, contentDisposition, mimeType);
                Uri uri = Uri.parse(url);

                if (Objects.equals(uri.getScheme(), Content.IPFS) ||
                        Objects.equals(uri.getScheme(), Content.IPNS)) {
                    String res = uri.getQueryParameter("download");
                    if (Objects.equals(res, "0")) {
                        try {
                            EVENTS.getInstance(getApplicationContext())
                                    .warning(getString(R.string.browser_handle_file, filename));
                        } finally {
                            progressIndicator.setVisibility(View.GONE);
                        }
                    } else {
                        contentDownloader(uri);
                    }
                } else {
                    fileDownloader(uri, filename, mimeType, contentLength);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        webView.setWebViewClient(new WebViewClient() {

            private final Map<Uri, Boolean> loadedUrls = new HashMap<>();
            private final AtomicReference<String> host = new AtomicReference<>();


            @Override
            public void onReceivedHttpError(
                    WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                LogUtils.info(TAG, "onReceivedHttpError " + errorResponse.getReasonPhrase());
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                LogUtils.info(TAG, "onReceivedSslError " + error.toString());
            }


            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
                LogUtils.info(TAG, "onPageCommitVisible " + url);
                progressIndicator.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {

                try {

                    WebViewDatabase database = WebViewDatabase.getInstance(getApplicationContext());
                    String[] data = database.getHttpAuthUsernamePassword(host, realm);


                    String storedName = null;
                    String storedPass = null;

                    if (data != null) {
                        storedName = data[0];
                        storedPass = data[1];
                    }

                    LayoutInflater inflater = (LayoutInflater)
                            getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View form = inflater.inflate(R.layout.http_auth_request, null);


                    final EditText usernameInput = form.findViewById(R.id.user_name);
                    final EditText passwordInput = form.findViewById(R.id.password);

                    if (storedName != null) {
                        usernameInput.setText(storedName);
                    }

                    if (storedPass != null) {
                        passwordInput.setText(storedPass);
                    }

                    MaterialAlertDialogBuilder authDialog = new MaterialAlertDialogBuilder(
                            MainActivity.this)
                            .setTitle(R.string.authentication)
                            .setView(form)
                            .setCancelable(false)
                            .setPositiveButton(android.R.string.ok, (dialog, whichButton) -> {

                                String username = usernameInput.getText().toString();
                                String password = passwordInput.getText().toString();

                                database.setHttpAuthUsernamePassword(host, realm, username, password);

                                handler.proceed(username, password);
                                dialog.dismiss();
                            })

                            .setNegativeButton(android.R.string.cancel, (dialog, whichButton) -> {
                                dialog.dismiss();
                                view.stopLoading();
                                handler.cancel();
                            });


                    authDialog.show();
                    return;
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

                super.onReceivedHttpAuthRequest(view, handler, host, realm);
            }


            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                LogUtils.info(TAG, "onLoadResource : " + url);
            }

            @Override
            public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
                super.doUpdateVisitedHistory(view, url, isReload);
                LogUtils.info(TAG, "doUpdateVisitedHistory : " + url + " " + isReload);
            }

            @Override
            public void onPageStarted(WebView view, String uri, Bitmap favicon) {
                LogUtils.info(TAG, "onPageStarted : " + uri);

                progressIndicator.setVisibility(View.VISIBLE);
                releaseActionMode();
                updateUri(Uri.parse(uri));
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                LogUtils.info(TAG, "onPageFinished : " + url);

                Uri uri = Uri.parse(url);
                if (Objects.equals(uri.getScheme(), Content.IPNS) ||
                        Objects.equals(uri.getScheme(), Content.IPFS)) {

                    try {
                        if (DOCS.getInstance(getApplicationContext()).numUris() == 0) {
                            progressIndicator.setVisibility(View.GONE);
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                } else {
                    progressIndicator.setVisibility(View.GONE);
                }

            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                LogUtils.info(TAG, "onReceivedError " + view.getUrl() + " " + error.getDescription());
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

                try {
                    Uri uri = request.getUrl();
                    LogUtils.error(TAG, "shouldOverrideUrlLoading : " + uri);

                    if (Objects.equals(uri.getScheme(), Content.ABOUT)) {
                        return true;
                    } else if (Objects.equals(uri.getScheme(), Content.HTTP)) {

                        Uri redirectUri = DOCS.redirectHttp(uri);
                        if (!Objects.equals(redirectUri, uri)) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, redirectUri,
                                    getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                            return true;
                        }
                        return false;
                    } else if (Objects.equals(uri.getScheme(), Content.HTTPS)) {
                        if (Settings.isRedirectUrlEnabled(getApplicationContext())) {
                            Uri redirectUri = DOCS.redirectHttps(uri);
                            if (!Objects.equals(redirectUri, uri)) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, redirectUri,
                                        getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                                return true;
                            }
                        }
                        return false;
                    } else if (Objects.equals(uri.getScheme(), Content.MAGNET)) {

                        MagnetUri magnetUri = MagnetUriParser.lenientParser().parse(uri.toString());

                        String name = uri.toString();
                        if (magnetUri.getDisplayName().isPresent()) {
                            name = magnetUri.getDisplayName().get();
                        }
                        magnetDownloader(uri, name);

                        return true;
                    } else if (Objects.equals(uri.getScheme(), Content.IPNS) ||
                            Objects.equals(uri.getScheme(), Content.IPFS)) {

                        String res = uri.getQueryParameter("download");
                        if (Objects.equals(res, "1")) {
                            contentDownloader(uri);
                            return true;
                        }

                        progressIndicator.setVisibility(View.VISIBLE);
                        return false;
                    } else if (Objects.equals(uri.getScheme(), Content.MAGNET)) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            startActivity(intent);

                        } catch (Throwable ignore) {
                            EVENTS.getInstance(getApplicationContext()).warning(
                                    getString(R.string.no_activity_found_to_handle_uri));
                        }
                        return true;
                    } else {
                        // all other stuff
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);

                        } catch (Throwable ignore) {
                            LogUtils.error(TAG, "Not  handled uri " + uri);
                        }
                        return true;
                    }

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

                return false;

            }


            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {

                Uri uri = request.getUrl();
                LogUtils.error(TAG, "shouldInterceptRequest : " + uri.toString());
                host.set(uri.getHost());
                if (Objects.equals(uri.getScheme(), Content.HTTP) ||
                        Objects.equals(uri.getScheme(), Content.HTTPS)) {
                    boolean ad;
                    if (!loadedUrls.containsKey(uri)) {
                        ad = AdBlocker.isAd(uri);
                        loadedUrls.put(uri, ad);
                    } else {
                        Boolean value = loadedUrls.get(uri);
                        Objects.requireNonNull(value);
                        ad = value;
                    }

                    if (ad) {
                        try {
                            return createEmptyResource();
                        } catch (Throwable throwable) {
                            return null;
                        }
                    } else {
                        return null;
                    }

                } else if (Objects.equals(uri.getScheme(), Content.IPNS) ||
                        Objects.equals(uri.getScheme(), Content.IPFS)) {

                    try {
                        DOCS docs = DOCS.getInstance(getApplicationContext());
                        docs.attachUri(uri);

                        int authority = uri.getAuthority().hashCode();
                        Session session = docs.getSession(authority);

                        Cancellable cancellable = () -> !docs.hasSession(authority);

                        try {
                            Uri redirectUri = uri;
                            if (Settings.isRedirectIndexEnabled(getApplicationContext())) {
                                redirectUri = docs.redirectUri(session, uri, cancellable);
                                if (!Objects.equals(uri, redirectUri)) {
                                    return docs.createRedirectMessage(redirectUri);
                                }
                            }

                            return docs.getResponse(session, getApplicationContext(),
                                    redirectUri, cancellable);

                        } catch (Throwable throwable) {
                            if (cancellable.isCancelled()) {
                                return createEmptyResource();
                            }
                            return createErrorMessage(throwable);
                        } finally {
                            docs.detachUri(uri);
                        }
                    } catch (Throwable throwable) {
                        return createErrorMessage(throwable);
                    }
                }
                return null;
            }
        });


        try {
            DOCS.getInstance(getApplicationContext()).darkMode.set(isDarkTheme());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        Intent intent = getIntent();
        boolean urlLoading = handleIntents(intent);

        if (savedInstanceState != null) {
            webView.restoreState(savedInstanceState);
        } else {
            if (!urlLoading) {
                openUri(Uri.parse(Settings.HOMEPAGE));
            }
        }
    }

    public String generateErrorHtml(@NonNull Throwable throwable) {
        String message = throwable.getMessage();
        if (message == null || message.isEmpty()) {
            message = throwable.getClass().getSimpleName();
        }
        return "<html>" + "<head>" + MimeTypeService.META +
                "<title>" + "Error" + "</title>" +
                "</head>\n" + MimeTypeService.STYLE +
                "<body><div <div>" + message + "</div></body></html>";
    }

    public WebResourceResponse createEmptyResource() {
        return new WebResourceResponse(MimeTypeService.PLAIN_MIME_TYPE, Content.UTF8,
                new ByteArrayInputStream("".getBytes()));
    }

    public WebResourceResponse createErrorMessage(@NonNull Throwable throwable) {
        LogUtils.error(TAG, throwable);
        String message = generateErrorHtml(throwable);
        return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE, Content.UTF8,
                new ByteArrayInputStream(message.getBytes()));
    }

    private void download() {
        try {
            String url = webView.getUrl();
            if (url != null && !url.isEmpty()) {
                Uri uri = Uri.parse(url);
                if (Objects.equals(uri.getScheme(), Content.IPFS) ||
                        Objects.equals(uri.getScheme(), Content.IPNS)) {
                    contentDownloader(uri);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private boolean downloadActive() {
        try {
            String url = webView.getUrl();
            if (url != null && !url.isEmpty()) {
                Uri uri = Uri.parse(url);
                if (Objects.equals(uri.getScheme(), Content.IPFS) ||
                        Objects.equals(uri.getScheme(), Content.IPNS)) {
                    return true;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    private void showDownloads() {
        try {
            mFolderRequestForResult.launch(InitApplication.getDownloadsIntent());
        } catch (Throwable e) {
            EVENTS.getInstance(getApplicationContext()).warning(
                    getString(R.string.no_activity_found_to_handle_uri));
        }
    }

    private void releaseActionMode() {
        try {
            if (actionMode != null) {
                actionMode.finish();
                actionMode = null;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntents(intent);
    }

    @Override
    public void onBackPressed() {

        boolean result = onBackPressedCheck();
        if (result) {
            return;
        }
        super.onBackPressed();
    }

    private boolean handleIntents(Intent intent) {

        final String action = intent.getAction();

        if (Intent.ACTION_VIEW.equals(action)) {
            Uri uri = intent.getData();
            if (uri != null) {
                openUri(uri);
                return true;
            }
        }

        if (Intent.ACTION_SEND.equals(action)) {
            if (Objects.equals(intent.getType(), MimeTypeService.PLAIN_MIME_TYPE)) {
                String text = intent.getStringExtra(Intent.EXTRA_TEXT);
                return doSearch(text);
            }
        }

        return false;
    }

    private boolean doSearch(@Nullable String query) {
        try {
            releaseActionMode();
            if (query != null && !query.isEmpty()) {
                Uri uri = Uri.parse(query);
                String scheme = uri.getScheme();
                if (Objects.equals(scheme, Content.IPNS) ||
                        Objects.equals(scheme, Content.IPFS) ||
                        Objects.equals(scheme, Content.HTTP) ||
                        Objects.equals(scheme, Content.HTTPS)) {
                    openUri(uri);
                } else {
                    String search = "https://duckduckgo.com/?q=" + query + "&kp=-1";
                    try {
                        // in case the search is an ipfs string
                        search = Content.IPFS + "://" + Cid.decode(query);
                    } catch (Throwable ignore) {
                    }
                    openUri(Uri.parse(search));
                }
                return true;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    private void openUri(@NonNull Uri uri) {

        try {
            LogUtils.error(TAG, uri.toString());
            updateUri(uri);

            progressIndicator.setVisibility(View.VISIBLE);

            DOCS docs = DOCS.getInstance(getApplicationContext());
            if (Objects.equals(uri.getScheme(), Content.IPNS) ||
                    Objects.equals(uri.getScheme(), Content.IPFS)) {

                docs.cleanupResolver(uri);
                docs.attachUri(uri);

                webView.getSettings().setJavaScriptEnabled(false);
            } else {
                webView.getSettings().setJavaScriptEnabled(
                        Settings.isJavascriptEnabled(getApplicationContext())
                );
            }
            docs.releaseSessions(uri.getAuthority().hashCode());

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            webView.stopLoading();

            webView.loadUrl(uri.toString());

            appBarLayout.setExpanded(true, true);
        }


    }

    @Override
    public WebView getWebView() {
        return webView;
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        webView.saveState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        webView.restoreState(savedInstanceState);
    }

    private ActionMode.Callback createSearchActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_searchable, menu);

                mode.setTitle("");

                MenuItem scanMenuItem = menu.findItem(R.id.action_scan);
                if (!hasCamera) {
                    scanMenuItem.setVisible(false);
                }

                MenuItem searchMenuItem = menu.findItem(R.id.action_search);
                SearchView mSearchView = (SearchView) searchMenuItem.getActionView();

                TextView textView = mSearchView.findViewById(
                        androidx.appcompat.R.id.search_src_text);
                textView.setTextSize(16);
                textView.setMinHeight(dp48ToPixels());

                ImageView magImage = mSearchView.findViewById(
                        androidx.appcompat.R.id.search_mag_icon);
                magImage.setVisibility(View.GONE);
                magImage.setImageDrawable(null);

                mSearchView.setMinimumHeight(actionBarSize());
                mSearchView.setIconifiedByDefault(false);
                mSearchView.setIconified(false);
                mSearchView.setSubmitButtonEnabled(false);
                mSearchView.setQueryHint(getString(R.string.enter_url));
                mSearchView.setFocusable(true);
                mSearchView.requestFocus();


                ListPopupWindow mPopupWindow = new ListPopupWindow(MainActivity.this,
                        null, android.R.attr.contextPopupMenuStyle) {

                    @Override
                    public boolean isInputMethodNotNeeded() {
                        return true;
                    }
                };

                mPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                mPopupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
                mPopupWindow.setModal(false);
                mPopupWindow.setAnchorView(mSearchView);
                mPopupWindow.setVerticalOffset(actionBarSize());
                mPopupWindow.setAnimationStyle(android.R.style.Animation);


                mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        mPopupWindow.dismiss();
                        doSearch(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {

                        if (!newText.isEmpty()) {
                            BOOKS books = BOOKS.getInstance(getApplicationContext());
                            List<Bookmark> bookmarks = books.getBookmarksByQuery(newText);

                            if (!bookmarks.isEmpty()) {
                                mPopupWindow.setAdapter(new SearchesAdapter(
                                        MainActivity.this, new ArrayList<>(bookmarks)) {
                                    @Override
                                    public void onClick(@NonNull Bookmark bookmark) {
                                        try {
                                            Thread.sleep(150);
                                            openUri(Uri.parse(bookmark.getUri()));
                                        } catch (Throwable throwable) {
                                            LogUtils.error(TAG, throwable);
                                        } finally {
                                            mPopupWindow.dismiss();
                                            releaseActionMode();
                                        }
                                    }
                                });
                                mPopupWindow.setAnchorView(mSearchView);
                                mPopupWindow.show();
                                return true;
                            } else {
                                mPopupWindow.dismiss();
                            }
                        } else {
                            mPopupWindow.dismiss();
                        }

                        return false;
                    }
                });

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                if (item.getItemId() == R.id.action_scan) {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < 500) {
                            return false;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();


                        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissionLauncher.launch(Manifest.permission.CAMERA);
                            return false;
                        }

                        invokeScan();

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    } finally {
                        mode.finish();
                    }
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                actionMode = null;
            }
        };

    }

    private void tearDown(@NonNull PopupWindow dialog) {
        try {
            Thread.sleep(150);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            dialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        releaseActionMode();
    }


    public abstract static class AppBarStateChangedListener implements AppBarLayout.OnOffsetChangedListener {

        private State mCurrentState = State.IDLE;

        @Override
        public final void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
            if (verticalOffset == 0) {
                setCurrentStateAndNotify(State.EXPANDED);
            } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                setCurrentStateAndNotify(State.COLLAPSED);
            } else {
                setCurrentStateAndNotify(State.IDLE);
            }
        }

        private void setCurrentStateAndNotify(State state) {
            if (mCurrentState != state) {
                onStateChanged(state);
            }
            mCurrentState = state;
        }

        public abstract void onStateChanged(State state);

        public enum State {
            EXPANDED,
            COLLAPSED,
            IDLE
        }
    }
}