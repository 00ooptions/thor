package threads.magnet.magnet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;

import threads.magnet.bencoding.BEInteger;
import threads.magnet.bencoding.BEMap;
import threads.magnet.bencoding.BEParser;
import threads.magnet.net.buffer.ByteBufferView;
import threads.magnet.protocol.DecodingContext;
import threads.magnet.protocol.EncodingContext;
import threads.magnet.protocol.Message;
import threads.magnet.protocol.handler.MessageHandler;

public class UtMetadataMessageHandler implements MessageHandler<UtMetadata> {
    private final Collection<Class<? extends UtMetadata>> supportedTypes = Collections.singleton(UtMetadata.class);

    @Override
    public boolean encode(EncodingContext context, Message base, ByteBuffer buffer) {
        UtMetadata message = (UtMetadata) base;
        Objects.requireNonNull(message);
        boolean encoded = false;
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            writeMessage(message, bos);
            byte[] payload = bos.toByteArray();
            if (buffer.remaining() >= payload.length) {
                buffer.put(payload);
                encoded = true;
            }
        } catch (IOException e) {
            // can't happen
        }
        return encoded;
    }

    private void writeMessage(UtMetadata message, OutputStream out) throws IOException {
        BEMap m = new BEMap(null, new HashMap<>() {{
            put(UtMetadata.messageTypeField(), new BEInteger(
                    BigInteger.valueOf(message.getType().id())));
            put(UtMetadata.pieceIndexField(), new BEInteger(
                    BigInteger.valueOf(message.getPieceIndex())));
            if (message.getTotalSize() != null) {
                put(UtMetadata.totalSizeField(), new BEInteger(
                        BigInteger.valueOf(message.getTotalSize())));
            }
        }});
        m.writeTo(out);
        if (message.getData() != null) {
            out.write(message.getData());
        }
    }

    @Override
    public int decode(DecodingContext context, ByteBufferView buffer) {
        byte[] payload = new byte[buffer.remaining()];
        buffer.get(payload);
        try (BEParser parser = new BEParser(payload)) {
            BEMap m = parser.readMap();
            int length = m.getContent().length;
            UtMetadata.Type messageType = getMessageType(m);
            int pieceIndex = getPieceIndex(m);
            switch (messageType) {
                case REQUEST: {
                    context.setMessage(UtMetadata.request(pieceIndex));
                    return length;
                }
                case DATA: {
                    byte[] data = Arrays.copyOfRange(payload, length, payload.length);
                    context.setMessage(UtMetadata.data(pieceIndex, getTotalSize(m), data));
                    return payload.length;
                }
                case REJECT: {
                    context.setMessage(UtMetadata.reject(pieceIndex));
                    return length;
                }
                default: {
                    throw new IllegalStateException("Unknown message type: " + messageType.name());
                }
            }
        }
    }

    private UtMetadata.Type getMessageType(BEMap m) {
        BEInteger type = (BEInteger) m.getValue().get(UtMetadata.messageTypeField());
        int typeId = Objects.requireNonNull(type).getValue().intValue();
        return UtMetadata.Type.forId(typeId);
    }

    private int getPieceIndex(BEMap m) {
        return getIntAttribute(UtMetadata.pieceIndexField(), m);
    }

    private int getTotalSize(BEMap m) {
        return getIntAttribute(UtMetadata.totalSizeField(), m);
    }

    private int getIntAttribute(String name, BEMap m) {
        BEInteger value = ((BEInteger) m.getValue().get(name));
        if (value == null) {
            throw new IllegalStateException("Message attribute is missing: " + name);
        }
        return value.getValue().intValue();
    }

    @Override
    public Collection<Class<? extends UtMetadata>> getSupportedTypes() {
        return supportedTypes;
    }

    @Override
    public Class<? extends UtMetadata> readMessageType(ByteBufferView buffer) {
        return UtMetadata.class;
    }
}
