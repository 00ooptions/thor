package threads.magnet.service;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import threads.magnet.LogUtils;

public class NetworkUtil {
    private static final String TAG = NetworkUtil.class.getSimpleName();

    public static boolean hasIpv6() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = networkInterfaces.nextElement();
                for (InterfaceAddress address : networkInterface.getInterfaceAddresses()) {
                    InetAddress ia = address.getAddress();
                    if (ia instanceof Inet6Address) {
                        if (!ia.isLinkLocalAddress() &&
                                !ia.isLoopbackAddress() &&
                                !ia.isSiteLocalAddress()) {
                            return true;
                        }
                    }
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    public static InetAddress getInetAddressFromNetworkInterfaces() {
        boolean isIpv6 = hasIpv6();
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = networkInterfaces.nextElement();
                Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
                while (inetAddresses.hasMoreElements()) {
                    InetAddress inetAddress = inetAddresses.nextElement();
                    if (!inetAddress.isMulticastAddress() && !inetAddress.isLoopbackAddress()) {
                        if (isIpv6 && inetAddress instanceof Inet6Address) {
                            if (!inetAddress.isLinkLocalAddress() &&
                                    !inetAddress.isSiteLocalAddress()) {
                                return inetAddress;
                            }
                        } else {
                            if (inetAddress instanceof Inet4Address) {
                                return inetAddress;
                            }
                        }
                    }
                }
            }

        } catch (SocketException e) {
            throw new RuntimeException("Failed to retrieve network address", e);
        }
        // explicitly returning a loopback address here instead of null;
        // otherwise we'll depend on how JDK classes handle this,
        // e.g. java/net/Socket.java:635
        return InetAddress.getLoopbackAddress();
    }
}
