package threads.magnet.bencoding;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * BEncoded list. May contain objects of different types.
 *
 * @since 1.0
 */
public class BEList implements BEObject {

    private final List<BEObject> value;

    /**
     * @param value Parsed value
     * @since 1.0
     */
    public BEList(List<BEObject> value) {
        this.value = Collections.unmodifiableList(value);
    }

    @Override
    public BEType getType() {
        return BEType.LIST;
    }

    public List<BEObject> getValue() {
        return value;
    }

    @Override
    public void writeTo(OutputStream out) throws IOException {
        BEEncoder.encoder().encode(this, out);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof BEList)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        return value.equals(((BEList) obj).getValue());
    }

    @NonNull
    @Override
    public String toString() {
        return Arrays.toString(value.toArray());
    }
}
