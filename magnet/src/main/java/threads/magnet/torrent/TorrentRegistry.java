package threads.magnet.torrent;


import androidx.annotation.Nullable;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import threads.magnet.LogUtils;
import threads.magnet.data.DataDescriptorFactory;
import threads.magnet.data.Storage;
import threads.magnet.metainfo.Torrent;
import threads.magnet.metainfo.TorrentId;
import threads.magnet.service.RuntimeLifecycleBinder;

public final class TorrentRegistry {
    private static final String TAG = TorrentRegistry.class.getSimpleName();
    private final DataDescriptorFactory dataDescriptorFactory;
    private final RuntimeLifecycleBinder lifecycleBinder;

    private final Set<TorrentId> torrentIds;
    private final ConcurrentMap<TorrentId, Torrent> torrents;
    private final ConcurrentMap<TorrentId, TorrentDescriptor> descriptors;

    public TorrentRegistry(DataDescriptorFactory dataDescriptorFactory,
                           RuntimeLifecycleBinder lifecycleBinder) {

        this.dataDescriptorFactory = dataDescriptorFactory;
        this.lifecycleBinder = lifecycleBinder;

        this.torrentIds = ConcurrentHashMap.newKeySet();
        this.torrents = new ConcurrentHashMap<>();
        this.descriptors = new ConcurrentHashMap<>();
    }

    public Collection<TorrentId> getTorrentIds() {
        return Collections.unmodifiableCollection(torrentIds);
    }

    @Nullable
    public Torrent getTorrent(TorrentId torrentId) {
        Objects.requireNonNull(torrentId, "Missing threads.torrent ID");
        return torrents.get(torrentId);
    }


    @Nullable
    public TorrentDescriptor getDescriptor(TorrentId torrentId) {
        Objects.requireNonNull(torrentId, "Missing threads.torrent ID");
        return descriptors.get(torrentId);
    }

    public TorrentDescriptor register(Torrent torrent, Storage storage) {
        TorrentId torrentId = torrent.getTorrentId();

        TorrentDescriptor descriptor = descriptors.get(torrentId);
        if (descriptor != null) {
            if (descriptor.getDataDescriptor() != null) {
                throw new IllegalStateException(
                        "Torrent already registered and data descriptor created: " + torrent.getTorrentId());
            }
            descriptor.setDataDescriptor(dataDescriptorFactory.createDescriptor(torrent, storage));

        } else {
            descriptor = new TorrentDescriptor();
            descriptor.setDataDescriptor(dataDescriptorFactory.createDescriptor(torrent, storage));

            TorrentDescriptor existing = descriptors.putIfAbsent(torrentId, descriptor);
            if (existing != null) {
                descriptor = existing;
            } else {
                torrentIds.add(torrentId);
                addShutdownHook(torrentId, descriptor);
            }
        }

        torrents.putIfAbsent(torrentId, torrent);
        return descriptor;
    }

    public TorrentDescriptor register(TorrentId torrentId) {
        TorrentDescriptor torrentDescriptor = getDescriptor(torrentId);
        if (torrentDescriptor != null) {
            return torrentDescriptor;
        }
        TorrentDescriptor descriptor = new TorrentDescriptor();

        TorrentDescriptor existing = descriptors.putIfAbsent(torrentId, descriptor);
        if (existing != null) {
            descriptor = existing;
        } else {
            torrentIds.add(torrentId);
            addShutdownHook(torrentId, descriptor);
        }

        return descriptor;

    }

    public boolean isSupportedAndActive(TorrentId torrentId) {
        TorrentDescriptor descriptor = getDescriptor(torrentId);
        // it's OK if descriptor is not present -- torrent might be being fetched at the time
        return getTorrentIds().contains(torrentId)
                && (descriptor == null || descriptor.isActive());
    }

    private void addShutdownHook(TorrentId torrentId, TorrentDescriptor descriptor) {
        lifecycleBinder.onShutdown("Closing data descriptor for threads.torrent ID: " + torrentId, () -> {
            if (descriptor.getDataDescriptor() != null) {
                try {
                    descriptor.getDataDescriptor().close();
                } catch (Throwable e) {
                    LogUtils.error(TAG, e);
                }
            }
        });
    }
}
