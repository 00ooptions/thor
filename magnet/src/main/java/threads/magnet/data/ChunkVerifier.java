package threads.magnet.data;

import android.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.magnet.LogUtils;
import threads.magnet.Settings;
import threads.magnet.data.digest.JavaSecurityDigester;
import threads.magnet.event.EventBus;
import threads.magnet.metainfo.TorrentId;

public class ChunkVerifier {

    private static final String TAG = ChunkVerifier.class.getSimpleName();
    private final JavaSecurityDigester digester;

    private final EventBus eventBus;
    private final AtomicBoolean exception = new AtomicBoolean(false);

    public ChunkVerifier(EventBus eventBus, JavaSecurityDigester digester) {
        this.eventBus = eventBus;
        this.digester = digester;
    }


    public void verify(TorrentId torrentId, List<ChunkDescriptor> chunks, Bitfield bitfield) {
        if (chunks.size() != bitfield.getPiecesTotal()) {
            throw new IllegalArgumentException("Bitfield has different size than the list of chunks. Bitfield size: " +
                    bitfield.getPiecesTotal() + ", number of chunks: " + chunks.size());
        }

        ChunkDescriptor[] arr = chunks.toArray(new ChunkDescriptor[0]);
        collectParallel(torrentId, arr, bitfield);

        // try to purge all data that was loaded by the verifiers
        System.gc();

    }


    public boolean verify(ChunkDescriptor chunk) {
        byte[] expected = chunk.getChecksum();
        byte[] actual = digester.digestForced(chunk.getData());
        return Arrays.equals(expected, actual);
    }


    public boolean verifyIfPresent(ChunkDescriptor chunk) {
        byte[] expected = chunk.getChecksum();
        byte[] actual = digester.digest(chunk.getData());
        return Arrays.equals(expected, actual);
    }

    private void collectParallel(TorrentId torrentId, ChunkDescriptor[] chunks, Bitfield bitfield) {
        int n = Settings.numOfHashingThreads;
        ExecutorService executor = Executors.newFixedThreadPool(n);

        List<Pair<Integer, Integer>> tasks = new ArrayList<>();

        int batchSize = chunks.length / n;
        int i, limit = 0;
        while ((i = limit) < chunks.length) {
            if (tasks.size() == n - 1) {
                // assign the remaining bits to the last worker
                limit = chunks.length;
            } else {
                limit = i + batchSize;
            }
            tasks.add(new Pair<>(i, limit));
        }


        CountDownLatch latch = new CountDownLatch(tasks.size());
        for (Pair<Integer, Integer> task : tasks) {
            executor.execute(() -> {
                try {
                    executeVerify(torrentId, chunks, task.first, task.second, bitfield);
                } finally {
                    latch.countDown();
                }
            });
        }
        executor.shutdown();

        try {
            latch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException("Unexpectedly interrupted");
        }

        if (exception.get()) {
            throw new RuntimeException("Failed to verify threads.torrent data ");
        }

    }

    private void executeVerify(TorrentId torrentId, ChunkDescriptor[] chunks,
                               int from, int to, Bitfield bitfield) {
        try {
            int i = from;
            while (i < to) {
                // optimization to speedup the initial verification of threads.torrent's data
                int[] emptyUnits = new int[]{0};
                chunks[i].getData().visitUnits((u, off, lim) -> {
                    // limit of 0 means an empty file,
                    // and we don't want to account for those
                    if (u.size() == 0 && lim != 0) {
                        emptyUnits[0]++;
                    }
                });

                // if any of this chunk's storage units is empty,
                // then the chunk is neither complete nor verified
                if (emptyUnits[0] == 0) {
                    boolean verified = verifyIfPresent(chunks[i]);
                    if (verified) {
                        bitfield.markVerified(i);
                        eventBus.firePieceVerified(torrentId, i);
                    }
                }
                i++;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            exception.set(true);
        }
    }

}
