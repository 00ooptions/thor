package threads.lite;


import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;
import android.util.Pair;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.spec.NamedParameterSpec;
import java.util.Base64;
import java.util.List;

import threads.lite.blockstore.BlockStoreCache;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.cid.ID;
import threads.lite.cid.IPLD;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multibase;
import threads.lite.cid.Multihash;
import threads.lite.cid.PeerId;
import threads.lite.cid.Prefix;
import threads.lite.cid.Tag;
import threads.lite.core.Keys;
import threads.lite.crypto.Key;
import threads.lite.dag.DagService;
import threads.lite.dht.QueryPeer;
import threads.lite.mplex.Mplex;
import threads.lite.mplex.MuxFlag;
import threads.lite.mplex.MuxId;
import threads.lite.noise.Noise;
import threads.lite.utils.DataHandler;


@RunWith(AndroidJUnit4.class)
public class IpfsUtilsTest {
    private static final String TAG = IpfsUtilsTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void testBlockStoreCache() throws Exception {
        BlockStoreCache a = BlockStoreCache.createInstance(context);
        BlockStoreCache b = BlockStoreCache.createInstance(context);

        assertNotNull(a.getName());
        assertNotNull(b.getName());
        assertNotEquals(a.getName(), b.getName());

        DagService.DirectoryNode directory = DagService.createDirectory();
        Block block = Block.createBlock(directory.getNode());
        Cid cid = block.getCid();
        a.storeBlock(block);

        assertTrue(a.hasBlock(cid));
        assertFalse(b.hasBlock(cid));
        a.close();
        assertFalse(a.hasBlock(cid));
        b.close();
    }


    @Test
    public void testDnsAddr() throws Exception {
        String address = "/dnsaddr/bootstrap.libp2p.io/p2p/QmbLHAnMoJPWSCR5Zhtx6BHJX9KiKNN6tpvbUcqanj75Nb";
        Multiaddr multiaddr = Multiaddr.create(address);
        assertNotNull(multiaddr);
        assertFalse(multiaddr.isCircuitAddress());
        assertTrue(multiaddr.strictSupportedAddress());
        assertTrue(multiaddr.isDnsaddr());
        assertNotNull(multiaddr.getPeerId());

    }

    @Test
    public void testDns4() throws Exception {

        String address = "/dns4/quicweb3-storage-am6.web3.dwebops.net/udp/4001/quic/p2p/12D3KooWPySxxWQjBgX9Jp6uAHQfVmdq8HG1gVvS1fRawHNSrmqW";
        Multiaddr multiaddr = Multiaddr.create(address);
        assertNotNull(multiaddr);
        assertFalse(multiaddr.isCircuitAddress());
        assertTrue(multiaddr.strictSupportedAddress());
        assertTrue(multiaddr.isDns4());
        assertNotNull(multiaddr.getPeerId());
        assertEquals(address, multiaddr.toString());


        address = "/dns4/luflosi.de/udp/4002/quic/p2p/12D3KooWBqQrnTqx9Wp89p2bD1hrwmXYJQ5x1fDfigRCfZJGKQfr/p2p-circuit/p2p/12D3KooWKQTnJ3vkV3aQFKiZQKJTfqrzj75Gfpp6CLMdBvqpu6Ac";
        multiaddr = Multiaddr.create(address);
        assertNotNull(multiaddr);
        assertTrue(multiaddr.isCircuitAddress());
        assertTrue(multiaddr.strictSupportedAddress());
        assertTrue(multiaddr.isDns4());
        assertNotNull(multiaddr.getPeerId());
        assertEquals(address, multiaddr.toString());


    }

    @Test
    public void testAddress() throws Exception {
        PeerId peerId = Key.random();
        String address = "/ip4/139.178.68.146/udp/4001/quic/p2p/" + peerId;
        Multiaddr multiaddr = Multiaddr.create(address);
        assertNotNull(multiaddr);

        Tag[] cmp = Multiaddr.fromArray(Multiaddr.toArray(multiaddr.getTags()));
        assertNotNull(cmp);
        assertArrayEquals(cmp, multiaddr.getTags());
    }

    @Test
    public void muxHeaderTest() {
        MuxId muxId = new MuxId(0, true);
        int header = Mplex.encodeHeader(muxId, MuxFlag.OPEN);
        assertEquals(header, 0);

        Pair<MuxId, MuxFlag> pair = Mplex.decodeHeader(header);
        assertNotNull(pair);
        MuxId cmp = pair.first;
        assertEquals(muxId.getStreamId(), cmp.getStreamId());
        assertTrue(muxId.isInitiator());
        assertFalse(cmp.isInitiator());
        assertEquals(pair.second, MuxFlag.OPEN);
    }


    @Test
    public void bigEndianTest() {
        int test = 30;
        ByteBuffer bigEndian = Noise.getBigEndianShort(test);
        bigEndian.flip();
        int length = DataHandler.getBigEndianUnsignedShort(bigEndian);
        assertEquals(length, test);
    }

    @Test
    public void bigEndianTestBig() {
        int test = 666;
        ByteBuffer bigEndian = Noise.getBigEndianShort(test);
        bigEndian.flip();
        int length = DataHandler.getBigEndianUnsignedShort(bigEndian);
        assertEquals(length, test);
    }

    @Test
    public void cidV1() throws Exception {

        Cid cid = Cid.nsToCid("moin welt");
        assertNotNull(cid);
        Prefix prefix = cid.getPrefix();
        assertNotNull(prefix);
        assertEquals(prefix.getCodec(), IPLD.RAW);
        assertEquals(prefix.getType(), Multihash.Type.sha2_256);
        assertEquals(prefix.getVersion(), 1);
        assertNotNull(cid.getMultihash());

        Cid cmp = Cid.fromArray(cid.encoded());
        assertNotNull(cmp);
        assertEquals(cmp, cid);
        assertEquals(cmp.getPrefix(), cid.getPrefix());
        assertEquals(cmp.getMultihash(), cid.getMultihash());

        // automatic conversion to version 1 and base32
        Cid v0 = Cid.decode("QmbWqxBEKC3P8tqsKc98xmWNzrzDtRLMiMPL8wBuTGsMnR");
        assertEquals(v0.toString(), "bafybeigdyrzt5sfp7udm7hu76uh7y26nf3efuylqabf3oclgtqy55fbzdi");

    }

    @Test
    public void peerId_random() {
        PeerId peerId = Key.random();
        byte[] bytes = PeerId.toArray(peerId);
        assertEquals(PeerId.fromArray(bytes), peerId);
    }

    @Test
    public void decode_name() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        // test of https://docs.ipfs.io/how-to/address-ipfs-on-web/#http-gateways
        PeerId test = PeerId.decode("QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN");
        Multihash multihash = Multihash.decode(test.encoded());
        String base36 = Multibase.encode(Multibase.Base.Base36,
                Cid.createCidV1(IPLD.Libp2pKey, multihash).encoded());
        assertEquals("k2k4r8jl0yz8qjgqbmc2cdu5hkqek5rj6flgnlkyywynci20j0iuyfuj", base36);

        Cid cid = Cid.decode("QmbWqxBEKC3P8tqsKc98xmWNzrzDtRLMiMPL8wBuTGsMnR");

        assertEquals(Cid.createCidV1(cid.getCodec(), cid.getMultihash()).toString(),
                "bafybeigdyrzt5sfp7udm7hu76uh7y26nf3efuylqabf3oclgtqy55fbzdi");

        assertEquals(ipfs.self(), ipfs.decodePeerId(ipfs.self().toBase36()));
    }

    @Test
    public void network() throws Exception {

        int port = 4001;

        IPFS ipfs = TestEnv.getTestInstance(context);

        List<Multiaddr> siteLocalAddresses = Multiaddr.getSiteLocalAddresses(ipfs.self(), port);
        assertNotNull(siteLocalAddresses);
        LogUtils.warning(TAG, siteLocalAddresses.toString());

        Multiaddr ma = Multiaddr.getLoopbackAddress(ipfs.self(), port);
        assertNotNull(ma);
        LogUtils.warning(TAG, ma.toString());


    }

    @Test
    public void cat_utils() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        PeerId peerId = ipfs.self();


        ID a = ID.convertPeerID(peerId);
        ID b = ID.convertPeerID(peerId);


        BigInteger dist = QueryPeer.distance(a, b);
        assertEquals(dist.longValue(), 0L);


        int cmp = a.compareTo(b);
        assertEquals(0, cmp);


        PeerId randrom = Key.random();

        ID r1 = ID.convertPeerID(randrom);
        ID r2 = ID.convertPeerID(randrom);

        BigInteger distCmp = QueryPeer.distance(a, r1);
        assertNotEquals(distCmp.longValue(), 0L);

        int rcmp = r1.compareTo(r2);
        assertEquals(0, rcmp);


        Cid cid = Cid.nsToCid("time");
        assertNotNull(cid);
        assertEquals(cid.getPrefix().getVersion(), 1);

    }

    // not yet supported
    @Test(expected = NoSuchAlgorithmException.class)
    public void test_curve25519() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("XDH");
        NamedParameterSpec paramSpec = new NamedParameterSpec("X25519");
        kpg.initialize(paramSpec); // equivalent to kpg.initialize(255)
        // alternatively: kpg = KeyPairGenerator.getInstance("X25519")
        KeyPair kp = kpg.generateKeyPair();
        assertNotNull(kp.getPrivate());
        assertNotNull(kp.getPublic());
    }


    @Test
    public void test_tink_ed25519() throws Exception {
        Keys keys = Key.generateKeys();

        PeerId peerId = Key.createPeerId(keys.getPublic());
        LogUtils.warning(TAG, peerId.toString());

        byte[] msg = "moin moin".getBytes();
        byte[] signature = Key.sign(keys.getPrivate(), msg);

        Base64.Encoder encoder = Base64.getEncoder();
        String privateKeyAsString = encoder.encodeToString(keys.getPrivate());
        assertNotNull(privateKeyAsString);
        String publicKeyAsString = encoder.encodeToString(keys.getPublic());
        assertNotNull(publicKeyAsString);

        Base64.Decoder decoder = Base64.getDecoder();

        byte[] privateKey = decoder.decode(privateKeyAsString);
        assertNotNull(privateKey);
        byte[] publicKey = decoder.decode(publicKeyAsString);


        PeerId peerIdCmp = Key.createPeerId(publicKey);
        LogUtils.warning(TAG, peerIdCmp.toString());

        assertEquals(peerId, peerIdCmp);

        Key.verify(publicKey, msg, signature);

    }

}
