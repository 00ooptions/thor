package threads.lite;


import static junit.framework.TestCase.assertEquals;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.blockstore.BlockStoreCache;
import threads.lite.cid.Cid;
import threads.lite.cid.Network;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutProgress;


@RunWith(AndroidJUnit4.class)
public class IpfsStressTest {
    private static final String TAG = IpfsStressTest.class.getSimpleName();


    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @NonNull
    public File createCacheFile() throws IOException {
        return File.createTempFile("temp", ".cid", context.getCacheDir());
    }

    @Test
    public void stress_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (BlockStoreCache cache = BlockStoreCache.createInstance(context)) {

            try (Session session = ipfs.createSession(cache, true)) {

                File file = createCacheFile();

                AtomicInteger percent = new AtomicInteger(0);

                ipfs.fetchToFile(session, file, Cid.decode("QmcniBv7UQ4gGPQQW2BwbD4ZZHzN3o3tPuNLZCbBchd1zh"),
                        new TimeoutProgress(TimeUnit.MINUTES.toSeconds(5)) {
                            @Override
                            public void setProgress(int progress) {
                                LogUtils.info(TAG, "Progress " + progress);
                                percent.set(progress);
                            }

                            @Override
                            public boolean doProgress() {
                                return true;
                            }
                        });

                assertEquals(percent.get(), 100);
            }
        }
    }

}
