package threads.lite;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import threads.lite.mplex.Mplex;
import threads.lite.mplex.MuxFlag;
import threads.lite.mplex.MuxId;
import threads.lite.mplex.MuxedStream;
import threads.lite.noise.CipherState;
import threads.lite.noise.CipherStatePair;
import threads.lite.noise.Noise;
import threads.lite.utils.DataHandler;

@RunWith(AndroidJUnit4.class)
public class IpfsNoiseTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    public byte[] readMuxDecodedData(byte[] data) {
        ByteBuffer wrap = ByteBuffer.wrap(data);
        int header = DataHandler.readUnsignedVariant(wrap);
        int length = DataHandler.readUnsignedVariant(wrap);
        Mplex.decodeHeader(header);
        byte[] result = new byte[length];
        wrap.get(result);
        return result;
    }

    @Test
    public void test_noise() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);
        Dummy dummy = Dummy.getInstance(context);

        Noise.NoiseState initiator = Noise.getInitiator(dummy.self(),
                ipfs.getKeys());
        assertNotNull(initiator);


        Noise.NoiseState responder = Noise.getResponder(ipfs.self(),
                dummy.getHost().getKeys());
        assertNotNull(responder);

        byte[] msg = initiator.getInitalMessage();

        Noise.Response resResponder = responder.handshake(msg);
        assertNotNull(resResponder);

        assertNull(resResponder.getCipherStatePair());
        assertNotNull(resResponder.getMessage());

        Noise.Response resInitiator = initiator.handshake(resResponder.getMessage());
        assertNotNull(resInitiator);

        CipherStatePair initiatorStatePair = resInitiator.getCipherStatePair();

        assertNotNull(initiatorStatePair); // done here initiator
        assertNotNull(resInitiator.getMessage());

        resResponder = responder.handshake(resInitiator.getMessage());
        assertNotNull(resResponder);

        CipherStatePair responderStatePair = resResponder.getCipherStatePair();
        assertNotNull(responderStatePair); // done here responder
        assertNull(resResponder.getMessage());


        CipherState initiatorSender = initiatorStatePair.getSender();
        assertNotNull(initiatorSender);
        CipherState initiatorReceiver = initiatorStatePair.getReceiver();
        assertNotNull(initiatorReceiver);


        CipherState responderReceiver = responderStatePair.getReceiver();
        assertNotNull(responderReceiver);
        CipherState responderSender = responderStatePair.getSender();
        assertNotNull(responderSender);


        // test 1 simple
        byte[] simpleData = "Moin Moin".getBytes();
        byte[] encrypted = Noise.encrypt(initiatorSender, simpleData);
        byte[] decrypted = Noise.decrypt(responderReceiver, encrypted);
        assertArrayEquals(simpleData, decrypted);


        // test 1 simple reverse
        encrypted = Noise.encrypt(responderSender, simpleData);
        decrypted = Noise.decrypt(initiatorReceiver, encrypted);
        assertArrayEquals(simpleData, decrypted);


        // test 2 mux stream
        byte[] muxData = "moin moin".getBytes();
        MuxId muxId = Mplex.defaultMuxId();
        MuxFlag muxFlag = MuxFlag.DATA;


        AtomicReference<byte[]> writtenBytesInitiator = new AtomicReference<>();
        MuxedStream muxedStreamInitiator = createDummy(initiatorStatePair, writtenBytesInitiator::set);
        muxedStreamInitiator.setMuxFlag(muxFlag);
        muxedStreamInitiator.setMuxId(muxId);
        muxedStreamInitiator.writeOutput(DataHandler.encode(muxData));

        assertNotNull(writtenBytesInitiator.get());
        byte[] bytes = writtenBytesInitiator.get();


        AtomicReference<byte[]> writtenBytesResponder = new AtomicReference<>();
        MuxedStream muxedStreamResponder = createDummy(responderStatePair, writtenBytesResponder::set);
        byte[] muxEncodedData = Noise.decodeNoiseMessage(bytes);
        List<ByteBuffer> list = muxedStreamResponder.readFrames(ByteBuffer.wrap(muxEncodedData));
        assertNotNull(list);
        assertEquals(list.size(), 1);
        byte[] muxDecodedData = list.get(0).array();
        assertArrayEquals(muxData, muxDecodedData);


        // test 2 mux stream reverse
        muxedStreamResponder.writeOutput(DataHandler.encode(muxData));
        assertNotNull(writtenBytesResponder.get());
        bytes = writtenBytesResponder.get();
        muxEncodedData = Noise.decodeNoiseMessage(bytes);
        list = muxedStreamInitiator.readFrames(ByteBuffer.wrap(muxEncodedData));
        assertNotNull(list);
        assertEquals(list.size(), 1);
        muxDecodedData = list.get(0).array();
        assertArrayEquals(muxData, muxDecodedData);


        // test 3
        assertEquals(Mplex.defaultMuxId().getStreamId(), 0);

    }

    private MuxedStream createDummy(CipherStatePair statePair, Consumer<byte[]> dataConsumer) {
        return new MuxedStream(new DummyStreamTest(dataConsumer),
                statePair.getSender(), statePair.getReceiver());
    }

    private static class DummyStreamTest implements QuicStream {

        private final Consumer<byte[]> dataConsumer;

        public DummyStreamTest(Consumer<byte[]> dataConsumer) {
            this.dataConsumer = dataConsumer;
        }

        @Override
        public QuicConnection getConnection() {
            return null;
        }

        @Override
        public void setAttribute(String key, Object value) {

        }

        @Nullable
        @Override
        public Object getAttribute(String key) {
            return null;
        }

        @Override
        public CompletableFuture<QuicStream> closeOutput() {
            return null;
        }

        @Override
        public CompletableFuture<QuicStream> writeOutput(byte[] data) {
            dataConsumer.accept(data);
            CompletableFuture<QuicStream> future = new CompletableFuture<>();
            future.complete(this);
            return future;
        }

        @Override
        public int getStreamId() {
            return 0;
        }

        @Override
        public boolean isUnidirectional() {
            return false;
        }

        @Override
        public boolean isClientInitiatedBidirectional() {
            return false;
        }

        @Override
        public boolean isServerInitiatedBidirectional() {
            return false;
        }

        @Override
        public void closeInput() {

        }
    }

}
