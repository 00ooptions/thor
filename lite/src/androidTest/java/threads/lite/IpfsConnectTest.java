package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import net.luminis.quic.QuicClientConnectionImpl;
import net.luminis.quic.Version;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

import threads.lite.cid.Multiaddr;
import threads.lite.core.Session;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.host.LiteCertificate;
import threads.lite.quic.ConnectionBuilder;
import threads.lite.utils.PackageReader;


@RunWith(AndroidJUnit4.class)
public class IpfsConnectTest {
    private static final String TAG = IpfsConnectTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    // @Test
    public void test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        LiteCertificate liteCertificate = LiteCertificate.createCertificate(ipfs.getKeys());

        InetSocketAddress remoteAddress = new InetSocketAddress("147.28.156.11", 4001);

        ConnectionBuilder builder = ConnectionBuilder.newBuilder()
                .version(Version.QUIC_version_1)
                .trustManager(new DummyTrust())
                .clientCertificate(liteCertificate.x509Certificate())
                .clientCertificateKey(liteCertificate.getKey())
                .remoteAddress(remoteAddress)
                .alpn(IPFS.ALPN)
                .transportParams(ipfs.getConnectionParameters());

        QuicClientConnectionImpl conn = builder.build(null, null,
                quicStream -> new PackageReader(new StreamHandler() {
                    @Override
                    public void throwable(Stream stream, Throwable throwable) {
                        LogUtils.warning(TAG, throwable.getMessage());
                    }

                    @Override
                    public void protocol(Stream stream, String protocol) {
                        LogUtils.warning(TAG, protocol);
                    }

                    @Override
                    public void data(Stream stream, ByteBuffer data) {

                    }

                    @Override
                    public void streamTerminated() {

                    }
                }));
        conn.connect(5);


    }


    // @Test(expected = TimeoutException.class)
    public void swarm_connect() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            String address = "/ip4/139.178.68.146/udp/4001/quic/p2p/" +
                    "12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR";

            Multiaddr multiaddr = ipfs.decodeMultiaddr(address);
            assertNotNull(multiaddr.getInetSocketAddress());
            assertEquals(multiaddr.getInetSocketAddress().getPort(), multiaddr.getPort());
            assertEquals(multiaddr.getInetSocketAddress().getHostName(), multiaddr.getHost());

            assertEquals(multiaddr.getPeerId(),
                    ipfs.decodePeerId("12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR"));


            // multiaddress is just a fiction
            ipfs.dial(session, multiaddr, ipfs.getConnectionParameters());

        }
    }

    @Test
    public void test_ipv6() throws Exception {

        String a1 = "/ip6/2804:d41:432f:3f00:ccbd:8e0d:a023:376b/udp/4001/quic/p2p/12D3KooWQ6SJ5A3uX5WjxCNbEbdAu8ufKJ3TmcjReTLSGaFk4HDU";
        String a2 = "/ip6/2804:d41:432f:3f00:94b9:ba41:dfc7:af66/udp/4001/quic/p2p/12D3KooWQ6SJ5A3uX5WjxCNbEbdAu8ufKJ3TmcjReTLSGaFk4HDU";
        String a3 = "/ip6/2804:d41:432f:3f00:a5b1:7947:debe:c88b/udp/4001/quic/p2p/12D3KooWQ6SJ5A3uX5WjxCNbEbdAu8ufKJ3TmcjReTLSGaFk4HDU";
        String a4 = "/ip6/2804:d41:432f:3f00:250e:c623:3b25:ddc8/udp/4001/quic/p2p/12D3KooWQ6SJ5A3uX5WjxCNbEbdAu8ufKJ3TmcjReTLSGaFk4HDU";

        Multiaddr multiaddr1 = Multiaddr.create(a1);
        LogUtils.debug(TAG, "Strict support " + multiaddr1.strictSupportedAddress());
        Multiaddr multiaddr2 = Multiaddr.create(a2);
        LogUtils.debug(TAG, "Strict support " + multiaddr2.strictSupportedAddress());
        Multiaddr multiaddr3 = Multiaddr.create(a3);
        LogUtils.debug(TAG, "Strict support " + multiaddr3.strictSupportedAddress());
        Multiaddr multiaddr4 = Multiaddr.create(a4);
        LogUtils.debug(TAG, "Strict support " + multiaddr4.strictSupportedAddress());
    }

}
