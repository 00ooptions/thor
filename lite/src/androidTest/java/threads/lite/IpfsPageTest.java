package threads.lite;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

import threads.lite.cid.Cid;
import threads.lite.core.IpnsEntity;
import threads.lite.core.PageStore;
import threads.lite.core.Session;


@RunWith(AndroidJUnit4.class)
public class IpfsPageTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_page() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession(false)) {
            String content = "Hallo dfsadf";
            Cid text = ipfs.storeText(session, content);
            assertNotNull(text);
            PageStore pageStore = ipfs.getPageStore();

            IpnsEntity page = pageStore.getPage(ipfs.self());
            assertNull(page);


            int sequence = 100;
            Date eol = IpnsEntity.getDefaultEol();

            page = new IpnsEntity(ipfs.self(), eol.getTime(),
                    IpnsEntity.encodeIpnsData(text), sequence);

            assertNotNull(page);

            pageStore.storePage(page);

            page = pageStore.getPage(ipfs.self());
            assertNotNull(page);

            Cid cmp = IpnsEntity.decodeIpnsData(page.getValue());
            assertEquals(text, cmp);

            Date eolCmp = page.getEolDate();
            assertEquals(eolCmp, eol);

            assertTrue(eolCmp.after(new Date(System.currentTimeMillis()))); // now time

            cmp = pageStore.getPageContent(ipfs.self());
            assertEquals(text, cmp);


            Thread.sleep(1000);
            pageStore.updatePageContent(ipfs.self(), text, IpnsEntity.getDefaultEol());

            page = pageStore.getPage(ipfs.self());
            assertNotNull(page);

            eolCmp = page.getEolDate();
            assertNotEquals(eolCmp, eol); // time has changed

        }
    }

}