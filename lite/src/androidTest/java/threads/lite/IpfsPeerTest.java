package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.core.Connection;
import threads.lite.core.IpnsRecord;
import threads.lite.core.Parameters;
import threads.lite.core.Session;

@RunWith(AndroidJUnit4.class)
public class IpfsPeerTest {
    private static final String TAG = IpfsPeerTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_connectPeer() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        if (ipfs.ipv().get() == IPV.IPv6) {
            return; // nothing to do here
        }


        try (Session session = ipfs.createSession()) {
            Multiaddr multiaddr = ipfs.decodeMultiaddr("/ip4/139.178.68.145/udp/4001/quic/p2p/" +
                    "12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR");
            assertTrue(multiaddr.isIP4());
            Parameters parameters = ipfs.getConnectionParameters();
            Connection conn = ipfs.dial(session, multiaddr, parameters);
            assertNotNull(conn);
            TestCase.assertTrue(conn.isConnected());
            byte[] test = "moin".getBytes(StandardCharsets.UTF_8);
            IpnsRecord data = ipfs.createSelfSignedIpnsRecord(0, test);
            try {
                ipfs.push(conn, data).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                fail();
            } catch (Throwable throwable) {
                LogUtils.info(TAG, throwable.getMessage());
            }
            conn.close();

        }
    }


}