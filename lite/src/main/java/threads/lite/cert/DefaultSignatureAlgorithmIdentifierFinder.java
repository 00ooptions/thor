package threads.lite.cert;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import threads.lite.asn1.ASN1ObjectIdentifier;
import threads.lite.asn1.DERNull;
import threads.lite.asn1.util.Strings;
import threads.lite.asn1.x509.AlgorithmIdentifier;
import threads.lite.asn1.x9.X9ObjectIdentifiers;

public class DefaultSignatureAlgorithmIdentifierFinder {

    private static final Map<String, ASN1ObjectIdentifier> algorithms = new HashMap<>();
    private static final Set<ASN1ObjectIdentifier> noParams = new HashSet<>();


    static {
        algorithms.put("SHA1WITHECDSA", X9ObjectIdentifiers.ecdsa_with_SHA1);
        algorithms.put("ECDSAWITHSHA1", X9ObjectIdentifiers.ecdsa_with_SHA1);
        algorithms.put("SHA224WITHECDSA", X9ObjectIdentifiers.ecdsa_with_SHA224);
        algorithms.put("SHA256WITHECDSA", X9ObjectIdentifiers.ecdsa_with_SHA256);
        algorithms.put("SHA384WITHECDSA", X9ObjectIdentifiers.ecdsa_with_SHA384);
        algorithms.put("SHA512WITHECDSA", X9ObjectIdentifiers.ecdsa_with_SHA512);


        //
        // According to RFC 3279, the ASN.1 encoding SHALL (id-dsa-with-sha1) or MUST (ecdsa-with-SHA*) omit the parameters field.
        // The parameters field SHALL be NULL for RSA based signature algorithms.
        //
        noParams.add(X9ObjectIdentifiers.ecdsa_with_SHA1);
        noParams.add(X9ObjectIdentifiers.ecdsa_with_SHA224);
        noParams.add(X9ObjectIdentifiers.ecdsa_with_SHA256);
        noParams.add(X9ObjectIdentifiers.ecdsa_with_SHA384);
        noParams.add(X9ObjectIdentifiers.ecdsa_with_SHA512);

    }

    private static AlgorithmIdentifier generate(String signatureAlgorithm) {
        AlgorithmIdentifier sigAlgId;

        String algorithmName = Strings.toUpperCase(signatureAlgorithm);
        ASN1ObjectIdentifier sigOID = algorithms.get(algorithmName);
        if (sigOID == null) {
            throw new IllegalArgumentException("Unknown signature type requested: " + algorithmName);
        }

        if (noParams.contains(sigOID)) {
            sigAlgId = new AlgorithmIdentifier(sigOID);
        } else {
            sigAlgId = new AlgorithmIdentifier(sigOID, DERNull.INSTANCE);
        }

        return sigAlgId;
    }

    public AlgorithmIdentifier find(String sigAlgName) {
        return generate(sigAlgName);
    }
}