package threads.lite.ipns;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;
import java.util.SortedMap;
import java.util.TreeMap;

import dht.pb.Dht;
import ipns.pb.Ipns;
import ipns.pb.Ipns.IpnsEntry;
import threads.lite.IPFS;
import threads.lite.cbor.CborObject;
import threads.lite.cbor.Cborable;
import threads.lite.cid.PeerId;
import threads.lite.core.IpnsEntity;
import threads.lite.crypto.Key;
import threads.lite.crypto.PubKey;


public class IpnsService {

    private static final String validity = "Validity";
    private static final String validityType = "ValidityType";
    private static final String value = "Value";
    private static final String sequence = "Sequence";
    private static final String ttl = "TTL";


    @SuppressLint("SimpleDateFormat")
    @NonNull
    public static Date getDate(@NonNull String format) throws ParseException {
        return Objects.requireNonNull(new SimpleDateFormat(IPFS.TIME_FORMAT_IPFS).parse(format));
    }


    @NonNull
    public static IpnsEntry create(byte[] key, byte[] value, long sequence, @NonNull Date eol,
                                   @NonNull Duration duration) throws Exception {

        @SuppressLint("SimpleDateFormat") String format = new SimpleDateFormat(
                IPFS.TIME_FORMAT_IPFS).format(eol);

        IpnsEntry entry = IpnsEntry.newBuilder()
                .setValidityType(IpnsEntry.ValidityType.EOL)
                .setSequence(sequence)
                .setTtl(duration.toNanos())
                .setValue(ByteString.copyFrom(value))
                .setValidity(ByteString.copyFrom(format.getBytes())).buildPartial();

        byte[] data = createCborDataForIpnsEntry(entry);
        IpnsEntry.Builder builder = entry.toBuilder();
        builder.setData(ByteString.copyFrom(data));
        entry = builder.buildPartial();

        byte[] sig1 = Key.sign(key, ipnsEntryDataForSigV1(entry));
        byte[] sig2 = Key.sign(key, ipnsEntryDataForSigV2(entry));

        builder = entry.toBuilder();
        builder.setSignatureV1(ByteString.copyFrom(sig1));
        builder.setSignatureV2(ByteString.copyFrom(sig2));

        return builder.build();
    }


    private static byte[] createCborDataForIpnsEntry(IpnsEntry entry) {

        SortedMap<CborObject, CborObject> map = new TreeMap<>(
                Comparator.comparingInt(Object::hashCode));
        map.put(new CborObject.CborString(value),
                new CborObject.CborByteArray(entry.getValue().toByteArray()));
        map.put(new CborObject.CborString(validity),
                new CborObject.CborByteArray(entry.getValidity().toByteArray()));
        map.put(new CborObject.CborString(validityType),
                new CborObject.CborLong(entry.getValidityType().getNumber()));
        map.put(new CborObject.CborString(sequence),
                new CborObject.CborLong(entry.getSequence()));
        map.put(new CborObject.CborString(ttl),
                new CborObject.CborLong(entry.getTtl()));
        CborObject.CborMap cborMap = new CborObject.CborMap(map);

        return cborMap.toByteArray();
    }

    static void validateCborDataMatchesPbData(IpnsEntry entry) throws Exception {

        byte[] data = entry.getData().toByteArray();
        if (data.length == 0) {
            throw new Exception("record data is missing");
        }


        CborObject deserialized = CborObject.fromByteArray(data);

        Objects.requireNonNull(deserialized);
        if (deserialized instanceof CborObject.CborMap) {

            CborObject.CborMap map = (CborObject.CborMap) deserialized;


            Cborable nd = map.values.get(new CborObject.CborString(value));
            Objects.requireNonNull(nd);
            byte[] ndBytes = ((CborObject.CborByteArray) nd.toCbor()).value;

            if (!Arrays.equals(entry.getValue().toByteArray(), ndBytes)) {
                throw new Exception("field value did not match between protobuf and CBOR");
            }


            nd = map.values.get(new CborObject.CborString(validity));
            Objects.requireNonNull(nd);
            ndBytes = ((CborObject.CborByteArray) nd.toCbor()).value;

            if (!Arrays.equals(entry.getValidity().toByteArray(), ndBytes)) {
                throw new Exception("field validity did not match between protobuf and CBOR");
            }

            nd = map.values.get(new CborObject.CborString(validityType));
            Objects.requireNonNull(nd);
            int type = (int) ((CborObject.CborLong) nd.toCbor()).value;

            if (entry.getValidityType().getNumber() != type) {
                throw new Exception("field validityType did not match between protobuf and CBOR");
            }


            nd = map.values.get(new CborObject.CborString(sequence));
            Objects.requireNonNull(nd);
            long sequence = ((CborObject.CborLong) nd.toCbor()).value;

            if (entry.getSequence() != sequence) {
                throw new Exception("field sequence did not match between protobuf and CBOR");
            }

            nd = map.values.get(new CborObject.CborString(ttl));
            Objects.requireNonNull(nd);
            long ttl = ((CborObject.CborLong) nd.toCbor()).value;

            if (entry.getTtl() != ttl) {
                throw new Exception("field ttl did not match between protobuf and CBOR");
            }
        } else {
            throw new Exception("CborObject.CborMap expected");
        }

    }


    public static byte[] ipnsEntryDataForSigV1(Ipns.IpnsEntry entry) throws Exception {
        ByteString value = entry.getValue();
        ByteString validity = entry.getValidity();
        String type = entry.getValidityType().toString();

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            outputStream.write(value.toByteArray());
            outputStream.write(validity.toByteArray());
            outputStream.write(type.getBytes());
            return outputStream.toByteArray();
        }
    }

    @NonNull
    public static IpnsEntity createIpnsEntity(@NonNull byte[] data) throws Exception {
        Dht.Message.Record record = Dht.Message.Record.parseFrom(data);
        return validate(record.getKey().toByteArray(), record.getValue().toByteArray());
    }

    private static byte[] ipnsEntryDataForSigV2(Ipns.IpnsEntry entry) {

        byte[] dataForSig = "ipns-signature:".getBytes();

        byte[] data = entry.getData().toByteArray();

        ByteBuffer out = ByteBuffer.allocate(dataForSig.length + data.length);
        out.put(dataForSig);
        out.put(data);
        return out.array();
    }

    @NonNull
    public static IpnsEntity validate(byte[] key, byte[] value) throws Exception {

        PeerId peerId = Key.decodeIpnsKey(key);

        Ipns.IpnsEntry entry = IpnsEntry.parseFrom(value);
        Objects.requireNonNull(entry);


        PubKey pubKey = extractPublicKey(peerId, entry);
        Objects.requireNonNull(pubKey);


        validate(pubKey, entry);

        return new IpnsEntity(peerId, getEOL(entry).getTime(),
                entry.getValue().toByteArray(), entry.getSequence());
    }


    public static int compare(@NonNull IpnsEntity a, @NonNull IpnsEntity b) {

        long as = a.getSequence();
        long bs = b.getSequence();

        if (as > bs) {
            return 1;
        } else if (as < bs) {
            return -1;
        }

        Date at = a.getEolDate();
        Date bt = b.getEolDate();

        if (at.after(bt)) {
            return 1;
        } else if (bt.after(at)) {
            return -1;
        }
        return 0;
    }

    @NonNull
    private static Date getEOL(Ipns.IpnsEntry entry) throws Exception {
        if (entry.getValidityType() != Ipns.IpnsEntry.ValidityType.EOL) {
            throw new Exception("validity type");
        }
        String date = new String(entry.getValidity().toByteArray());
        return getDate(date);
    }

    // ExtractPublicKey extracts a public key matching `pid` from the IPNS record,
    // if possible.
    //
    // This function returns (nil, nil) when no public key can be extracted and
    // nothing is malformed.
    @NonNull
    private static PubKey extractPublicKey(PeerId pid, Ipns.IpnsEntry entry)
            throws Exception {


        if (entry.hasPubKey()) {
            byte[] pubKey = entry.getPubKey().toByteArray();

            PubKey pk = Key.unmarshalPublicKey(pubKey);
            PeerId expPid = Key.fromPubKey(pk);

            if (!Objects.equals(pid, expPid)) {
                throw new Exception("invalid peer");
            }
            return pk;
        }

        return Key.extractPublicKey(pid);
    }

    // Validates validates the given IPNS entry against the given public key
    private static void validate(PubKey pubKey, Ipns.IpnsEntry entry) throws Exception {

        if (entry.hasSignatureV2()) {
            pubKey.verify(ipnsEntryDataForSigV2(entry), entry.getSignatureV2().toByteArray());
        } else {
            throw new Exception("no valid signature [SigV1 ignored]");
        }

        validateCborDataMatchesPbData(entry);

        if (new Date().after(getEOL(entry))) {
            throw new Exception("outdated ipns entry");
        }
    }

}
