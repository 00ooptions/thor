package threads.lite.mplex;

import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import threads.lite.LogUtils;
import threads.lite.core.Connection;
import threads.lite.core.Stream;
import threads.lite.host.LiteConnection;
import threads.lite.noise.CipherState;
import threads.lite.noise.Noise;
import threads.lite.utils.DataHandler;

public class MuxedStream implements Stream {
    private static final String TAG = MuxedStream.class.getSimpleName();

    @NonNull
    private final QuicStream quicStream;
    @NonNull
    private final CipherState sender;
    @NonNull
    private final CipherState receiver;
    @NonNull
    private MuxFlag muxFlag;
    @NonNull
    private MuxId muxId;

    public MuxedStream(@NonNull QuicStream quicStream,
                       @NonNull CipherState sender,
                       @NonNull CipherState receiver) {
        this.quicStream = quicStream;
        this.muxId = Mplex.defaultMuxId();
        this.sender = sender;
        this.receiver = receiver;
        this.muxFlag = MuxFlag.OPEN;
    }

    @NonNull
    public static CompletableFuture<MuxedStream> createStream(MuxedTransport transport) {
        CompletableFuture<MuxedStream> future = new CompletableFuture<>();
        MuxedStream muxedStream = new MuxedStream(transport.getQuicStream(),
                transport.getSender(), transport.getReceiver());
        MuxId muxId = Mplex.generateMuxId(transport.getQuicStream());
        muxedStream.setMuxId(muxId);
        muxedStream.setMuxFlag(MuxFlag.OPEN);
        muxedStream.writeOutput(String.valueOf(muxId.getStreamId()).getBytes())
                .whenComplete((stream, throwable) -> {
                    if (throwable != null) {
                        future.completeExceptionally(throwable);
                    } else {
                        muxedStream.setMuxFlag(MuxFlag.DATA);
                        future.complete(muxedStream);
                    }
                });
        return future;
    }

    @NonNull
    public MuxFlag getMuxFlag() {
        return muxFlag;
    }

    public void setMuxFlag(@NonNull MuxFlag muxFlag) {
        this.muxFlag = muxFlag;
    }

    public void setMuxId(@NonNull MuxId muxId) {
        this.muxId = muxId;
    }

    @NonNull
    @Override
    public Connection getConnection() {
        return new LiteConnection(quicStream.getConnection());
    }

    @NonNull
    @Override
    public CompletableFuture<Stream> closeOutput() {
        setMuxFlag(MuxFlag.CLOSE);
        return writeOutput(new byte[0]);
    }

    @NonNull
    @Override
    public CompletableFuture<Stream> writeOutput(byte[] data) {
        CompletableFuture<Stream> stream = new CompletableFuture<>();
        try {
            byte[] muxEncoded = Mplex.encode(muxId, muxFlag, data);
            byte[] muxEncrypted = Noise.encrypt(sender, muxEncoded);
            byte[] encoded = Noise.encodeNoiseMessage(muxEncrypted);

            LogUtils.debug(TAG, "WRITE MUX " + muxId + " " +
                    MplexFlag.toMplexFlag(muxFlag, muxId.isInitiator()).name());

            quicStream.writeOutput(encoded).whenComplete((quicStream, throwable) -> {
                if (throwable != null) {
                    stream.completeExceptionally(throwable);
                } else {
                    stream.complete(this);
                }
            });
        } catch (Throwable throwable) {
            stream.completeExceptionally(throwable);
        }
        return stream;
    }

    @Override
    public int getStreamId() {
        return muxId.getStreamId();
    }

    @Override
    public void close() {
        setMuxFlag(MuxFlag.RESET);
        writeOutput(new byte[0]);
    }

    @Override
    public void setAttribute(String key, Object value) {
        quicStream.setAttribute(key, value);
    }

    @Nullable
    @Override
    public Object getAttribute(String key) {
        return quicStream.getAttribute(key);
    }

    @Override
    public boolean isInitiator() {
        return muxId.isInitiator();
    }

    @NonNull
    public List<ByteBuffer> readFrames(ByteBuffer data) throws Exception {
        byte[] decrypted = Noise.decrypt(receiver, data.array());
        // read out mux frame (decode mux frame)
        ByteBuffer mux = ByteBuffer.wrap(decrypted);
        int header = DataHandler.readUnsignedVariant(mux);
        int length = DataHandler.readUnsignedVariant(mux);
        byte[] muxData = new byte[length];
        mux.get(muxData);
        Pair<MuxId, MuxFlag> pair = Mplex.decodeHeader(header);
        setMuxId(pair.first);
        setMuxFlag(pair.second);

        switch (getMuxFlag()) {
            case DATA: {
                LogUtils.debug(TAG, "READ MUX DATA " + muxId + " " +
                        MplexFlag.toMplexFlag(muxFlag, muxId.isInitiator()).name());
                return DataHandler.decode(ByteBuffer.wrap(muxData));
            }
            case RESET: {
                // stream is closed (maybe a check that no writing, nor reading occurs)
                LogUtils.debug(TAG, "READ MUX RESET " + muxId + " " +
                        MplexFlag.toMplexFlag(muxFlag, muxId.isInitiator()).name() +
                        " " + new String(muxData));
                return Collections.emptyList();
            }
            case CLOSE: {
                // nothing to do here, stream is closed for writing (maybe a check to ensure that)
                LogUtils.debug(TAG, "READ MUX CLOSE " + muxId + " " +
                        MplexFlag.toMplexFlag(muxFlag, muxId.isInitiator()).name() +
                        " " + new String(muxData));
                return Collections.emptyList();
            }
            case OPEN: {
                LogUtils.debug(TAG, "READ MUX OPEN " + muxId + " " +
                        MplexFlag.toMplexFlag(muxFlag, muxId.isInitiator()).name() +
                        " " + new String(muxData));
                return Collections.emptyList();
            }
            default:
                throw new IllegalStateException("not handled mux flag");
        }
    }

    @NonNull
    @Override
    public String toString() {
        return "MuxedStream{" +
                "muxId=" + muxId +
                ", muxFlag=" + muxFlag.name() +
                '}';
    }

    @SuppressWarnings("unused")
    @NonNull
    public MuxedTransport getTransport() {
        return new MuxedTransport(quicStream, sender, receiver);
    }
}
