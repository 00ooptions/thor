package threads.lite.ident;

import java.nio.ByteBuffer;

import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.core.Host;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Stream;
import threads.lite.utils.DataHandler;

public class IdentityPushHandler implements ProtocolHandler {

    private final Host host;

    public IdentityPushHandler(Host host) {
        this.host = host;
    }

    @Override
    public String getProtocol() {
        return IPFS.IDENTITY_PUSH_PROTOCOL;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.IDENTITY_PUSH_PROTOCOL))
                .thenApply(Stream::closeOutput);

    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {
        // just read out the identity (later store them into the connection
        // when a use-case is there
        IdentifyOuterClass.Identify identify = IdentifyOuterClass.Identify.parseFrom(data.array());
        IdentityService.getPeerInfo(host.self(), identify);

    }
}
