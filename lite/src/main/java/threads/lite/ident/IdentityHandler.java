package threads.lite.ident;

import java.nio.ByteBuffer;
import java.util.Set;

import crypto.pb.Crypto;
import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.cid.Multiaddr;
import threads.lite.core.Host;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Stream;
import threads.lite.utils.DataHandler;

public class IdentityHandler implements ProtocolHandler {

    private final Host host;

    public IdentityHandler(Host host) {
        this.host = host;
    }

    @Override
    public String getProtocol() {
        return IPFS.IDENTITY_PROTOCOL;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.IDENTITY_PROTOCOL));

        Crypto.PublicKey publicKey = host.getPublicKey();
        Set<Multiaddr> multiaddrs = host.publishMultiaddrs();
        Set<String> set = host.getProtocolNames();

        IdentifyOuterClass.Identify response =
                IdentityService.createIdentity(publicKey, set, multiaddrs,
                        stream.getConnection().remoteMultiaddr());
        stream.writeOutput(DataHandler.encode(response))
                .thenApply(Stream::closeOutput);

    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {
        throw new Exception("should not be invoked");
    }
}
