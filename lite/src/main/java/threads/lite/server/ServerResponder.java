package threads.lite.server;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.core.Transport;
import threads.lite.host.LiteTransport;
import threads.lite.utils.DataHandler;

public final class ServerResponder implements StreamHandler {

    private static final String TAG = ServerResponder.class.getSimpleName();
    @NonNull
    private final Map<String, ProtocolHandler> protocols = new ConcurrentHashMap<>();

    public void addProtocolHandler(@NonNull ProtocolHandler protocolHandler) throws Exception {

        // some small tests
        String protocol = protocolHandler.getProtocol();
        Objects.requireNonNull(protocol);

        if (protocol.isEmpty()) {
            throw new Exception("invalid protocol name");
        }

        if (!protocol.startsWith("/")) {
            throw new Exception("invalid protocol name");
        }

        if (protocols.containsKey(protocol)) {
            throw new Exception("protocol name already exists");
        }

        protocols.put(protocol, protocolHandler);
    }

    @Nullable
    public ProtocolHandler getProtocolHandler(@NonNull String protocol) {
        return protocols.get(protocol);
    }

    @Override
    public void throwable(Stream stream, Throwable throwable) {
        LogUtils.error(TAG, throwable.getMessage());
        stream.close();
    }

    @NonNull
    @Override
    public Transport getTransport(QuicStream quicStream) {
        return new LiteTransport(quicStream);
    }

    @Override
    public void streamTerminated() {
        LogUtils.info(TAG, "stream terminated nothing to do");
    }

    @Override
    public void protocol(Stream stream, String protocol) throws Exception {
        stream.setAttribute(PROTOCOL, protocol);
        ProtocolHandler protocolHandler = getProtocolHandler(protocol);
        if (protocolHandler != null) {
            protocolHandler.protocol(stream);
        } else {
            LogUtils.error(TAG, "Ignore " + protocol);
            stream.writeOutput(DataHandler.encodeProtocols(IPFS.NA));
        }
    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {
        String protocol = (String) stream.getAttribute(PROTOCOL);
        if (protocol != null) {
            ProtocolHandler protocolHandler = getProtocolHandler(protocol);
            if (protocolHandler != null) {
                protocolHandler.data(stream, data);
            } else {
                throwable(stream, new Exception("StreamHandler invalid protocol"));
            }
        } else {
            throwable(stream, new Exception("StreamHandler invalid protocol"));
        }
    }

    @NonNull
    public Set<String> getProtocolNames() {
        return protocols.keySet();
    }

    @NonNull
    public Map<String, ProtocolHandler> getProtocols() {
        return protocols;
    }
}
