package threads.lite.dag;

import androidx.annotation.NonNull;

import java.util.Stack;

import merkledag.pb.Merkledag;

public class DagVisitor {
    private final Stack<DagStage> stack = new Stack<>();
    private boolean rootVisited;

    public DagVisitor(@NonNull Merkledag.PBNode root) {
        rootVisited = false;
        pushActiveNode(root);
    }

    public void reset(@NonNull Stack<DagStage> dagStages) {
        stack.clear();
        stack.addAll(dagStages);
        rootVisited = true;
    }

    public void pushActiveNode(@NonNull Merkledag.PBNode node) {
        stack.push(new DagStage(node));
    }

    public void popStage() {
        stack.pop();
    }

    public DagStage peekStage() {
        return stack.peek();
    }

    public boolean isRootVisited(boolean visited) {
        boolean temp = rootVisited;
        rootVisited = visited;
        return temp;
    }

    public boolean isPresent() {
        return !stack.isEmpty();
    }

    @NonNull
    @Override
    public String toString() {
        String result = "";
        for (DagStage dagStage : stack) {
            result = result.concat(dagStage.toString());
        }
        return result;
    }
}
