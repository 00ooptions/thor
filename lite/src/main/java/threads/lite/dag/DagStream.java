package threads.lite.dag;


import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import merkledag.pb.Merkledag;
import threads.lite.LogUtils;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.cid.Dir;
import threads.lite.core.Cancellable;
import threads.lite.core.Link;
import threads.lite.core.Session;
import threads.lite.utils.ReaderInputStream;
import threads.lite.utils.Resolver;
import unixfs.pb.Unixfs;


public class DagStream {


    private static DagAdder getFileAdder(@NonNull Session session) {
        return DagAdder.createAdder(session);
    }

    public static boolean isDir(@NonNull Cancellable cancellable,
                                @NonNull Session session,
                                @NonNull Cid cid) throws Exception {


        DagService dagService = DagService.createDagService(session);

        Merkledag.PBNode node = Resolver.resolveNode(cancellable, dagService, cid);
        Objects.requireNonNull(node);
        return DagService.isDirectory(node);
    }

    @NonNull
    public static Dir createEmptyDirectory(@NonNull Session session) throws Exception {
        DagAdder dagAdder = getFileAdder(session);
        return dagAdder.createEmptyDirectory();
    }

    @NonNull
    public static Dir addLinkToDirectory(@NonNull Session session,
                                         @NonNull Cid directory,
                                         @NonNull Link link) throws Exception {

        DagAdder dagAdder = getFileAdder(session);
        Block block = session.getBlockStore().getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = block.getData();
        Objects.requireNonNull(dirNode);
        return dagAdder.addChild(dirNode, link);

    }

    @NonNull
    public static Dir updateLinkToDirectory(@NonNull Session session, @NonNull Cid directory,
                                            @NonNull Link link) throws Exception {

        DagAdder dagAdder = getFileAdder(session);
        Block block = session.getBlockStore().getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = block.getData();
        Objects.requireNonNull(dirNode);
        return dagAdder.updateChild(dirNode, link);

    }

    @NonNull
    public static Dir createDirectory(@NonNull Session session, @NonNull List<Link> links)
            throws Exception {
        DagAdder dagAdder = getFileAdder(session);
        return dagAdder.createDirectory(links);
    }

    @NonNull
    public static Dir removeFromDirectory(@NonNull Session session, @NonNull Cid directory,
                                          @NonNull String name) throws Exception {

        DagAdder dagAdder = getFileAdder(session);
        Block block = session.getBlockStore().getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = block.getData();
        Objects.requireNonNull(dirNode);
        return dagAdder.removeChild(dirNode, name);
    }

    @NonNull
    public static List<Cid> getBlocks(@NonNull Session session, @NonNull Cid cid) {
        List<Cid> result = new ArrayList<>();

        Block block = session.getBlockStore().getBlock(cid);
        if (block != null) { // block is locally available
            Merkledag.PBNode node = block.getData();
            List<Merkledag.PBLink> links = node.getLinksList();

            for (Merkledag.PBLink link : links) {
                Cid child = Cid.decode(link.getHash().toByteArray());
                result.add(child);
                result.addAll(getBlocks(session, child));
            }
        }
        return result;
    }

    public static void ls(@NonNull Cancellable cancellable, @NonNull Consumer<Link> consumer,
                          @NonNull Session session, @NonNull Cid cid, boolean resolveChildren)
            throws Exception {

        DagService dagService = DagService.createDagService(session);


        Merkledag.PBNode node = Resolver.resolveNode(cancellable, dagService, cid);
        Objects.requireNonNull(node);
        List<Merkledag.PBLink> links = node.getLinksList();
        for (Merkledag.PBLink link : links) {
            processLink(cancellable, consumer, dagService, link, resolveChildren);
        }
    }

    public static boolean hasLink(@NonNull Cancellable cancellable,
                                  @NonNull Session session, @NonNull Cid cid,
                                  @NonNull String name) throws Exception {

        DagService dagService = DagService.createDagService(session);

        Merkledag.PBNode node = Resolver.resolveNode(cancellable, dagService, cid);
        Objects.requireNonNull(node);
        Merkledag.PBLink link = DagReader.getLinkByName(node, name);
        return link != null;
    }

    @NonNull
    public static Cid readInputStream(@NonNull Session session,
                                      @NonNull ReaderInputStream readerInputStream) throws Exception {

        DagAdder dagAdder = getFileAdder(session);
        return dagAdder.createFromStream(readerInputStream);
    }

    private static void processLink(@NonNull Cancellable cancellable,
                                    @NonNull Consumer<Link> consumer,
                                    @NonNull DagService dagService,
                                    @NonNull Merkledag.PBLink link,
                                    boolean resolveChildren) throws Exception {

        String name = link.getName();
        long size = link.getTsize();
        Cid cid = Cid.decode(link.getHash().toByteArray());

        if (!resolveChildren) {
            consumer.accept(Link.create(cid, name, size, Link.Unknown));
        } else {

            Merkledag.PBNode linkNode = dagService.getNode(cancellable, cid);

            Unixfs.Data data = DagReader.getData(linkNode);

            int type;
            switch (data.getType()) {
                case File:
                    type = Link.File;
                    break;
                case Raw:
                    type = Link.Raw;
                    break;
                case Directory:
                    type = Link.Dir;
                    break;
                case Metadata:
                    LogUtils.error(DagStream.class.getSimpleName(),
                            "Handle MimeType" +
                                    Unixfs.Metadata.parseFrom(
                                            data.getData().toByteArray()).getMimeType());
                default:
                    type = Link.Unknown;
                    LogUtils.error(DagStream.class.getSimpleName(),
                            "TODO Handle " + data.getType().name());
            }
            size = data.getFilesize();
            consumer.accept(Link.create(cid, name, size, type));
        }
    }
}
