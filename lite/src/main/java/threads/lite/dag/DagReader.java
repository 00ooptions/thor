package threads.lite.dag;


import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.util.Objects;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

import merkledag.pb.Merkledag;
import threads.lite.core.Cancellable;
import unixfs.pb.Unixfs;

public class DagReader {

    public final AtomicInteger atomicLeft = new AtomicInteger(0);
    private final long size;
    private final DagVisitor dagVisitor;
    private final DagWalker dagWalker;


    public DagReader(@NonNull DagWalker dagWalker, long size) {
        this.dagWalker = dagWalker;
        this.size = size;
        this.dagVisitor = new DagVisitor(dagWalker.getRoot());

    }

    public static DagReader create(@NonNull Merkledag.PBNode node,
                                   @NonNull DagService dagService) throws Exception {
        long size = 0;

        Unixfs.Data unixData = DagReader.getData(node);

        switch (unixData.getType()) {
            case Raw:
            case File:
                size = unixData.getFilesize();
                break;
        }


        DagWalker dagWalker = DagWalker.createWalker(node, dagService);
        return new DagReader(dagWalker, size);

    }

    @Nullable
    public static Merkledag.PBLink getLinkByName(@NonNull Merkledag.PBNode node,
                                                 @NonNull String name) {

        for (Merkledag.PBLink link : node.getLinksList()) {
            if (Objects.equals(link.getName(), name)) {
                return link;
            }
        }
        return null;

    }

    @NonNull
    public static Unixfs.Data getData(@NonNull Merkledag.PBNode node) throws Exception {
        return Unixfs.Data.parseFrom(node.getData().toByteArray());
    }

    public long getSize() {
        return size;
    }

    public void seek(@NonNull Cancellable cancellable, long offset) throws Exception {
        Pair<Stack<DagStage>, Long> result = dagWalker.seek(cancellable, offset);
        this.atomicLeft.set(result.second.intValue());
        this.dagVisitor.reset(result.first);
    }

    @NonNull
    public ByteString loadNextData(@NonNull Cancellable cancellable) throws Exception {

        int left = atomicLeft.getAndSet(0);
        if (left > 0) {
            DagStage dagStage = dagVisitor.peekStage();
            Merkledag.PBNode node = dagStage.getNode();

            if (node.getLinksCount() == 0) {
                return readUnixNodeData(dagStage.getData(), left);
            }
        }

        while (true) {
            DagStage dagStage = dagWalker.next(cancellable, dagVisitor);
            if (dagStage == null) {
                // done nothing left
                return ByteString.EMPTY;
            }

            Merkledag.PBNode node = dagStage.getNode();
            if (node.getLinksCount() > 0) {
                continue;
            }

            Unixfs.Data unixData = dagStage.getData();
            return readUnixNodeData(unixData, 0);
        }
    }

    private ByteString readUnixNodeData(@NonNull Unixfs.Data unixData, int position) {

        switch (unixData.getType()) {
            case Directory:
            case File:
            case Raw:
                return unixData.getData().substring(position);
            default:
                throw new IllegalStateException("found %s node in unexpected place " +
                        unixData.getType().name());
        }
    }
}
