package threads.lite.relay;


import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import circuit.pb.Circuit;
import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.Limit;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Server;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.holepunch.HolePunch;
import threads.lite.host.LiteStream;
import threads.lite.ident.IdentityService;
import threads.lite.mplex.MuxedTransport;
import threads.lite.noise.CipherStatePair;
import threads.lite.noise.Handshake;
import threads.lite.noise.Noise;
import threads.lite.utils.DataHandler;

public final class RelayResponder implements StreamHandler {
    private static final String TAG = RelayResponder.class.getSimpleName();
    private static final String NOISE = "NOISE";
    private static final String PEER = "PEER";
    private final Server server;


    public RelayResponder(Server server) {
        this.server = server;

    }

    public Noise.NoiseState getResponder(Stream stream) throws Exception {
        Noise.NoiseState state = (Noise.NoiseState) stream.getAttribute(NOISE);
        PeerId peerId = Objects.requireNonNull((PeerId) stream.getAttribute(PEER));

        if (state == null) {
            state = Noise.getResponder(peerId, server.getKeys());
            stream.setAttribute(NOISE, state);
        }
        return state;
    }

    @Override
    public void throwable(Stream stream, Throwable throwable) {
        LogUtils.error(TAG, throwable.getMessage());
        stream.close();
    }

    @Override
    public void protocol(Stream stream, String protocol) throws Exception {

        LogUtils.info(TAG, "Protocol " + protocol +
                " streamId " + stream.getStreamId() + " initiator " + stream.isInitiator());

        stream.setAttribute(PROTOCOL, protocol);
        switch (protocol) {
            case IPFS.MULTISTREAM_PROTOCOL:
                if (!stream.isInitiator()) {
                    stream.writeOutput(DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL));
                }
                break;
            case IPFS.HOLE_PUNCH_PROTOCOL: {
                if (!stream.isInitiator()) {
                    stream.writeOutput(DataHandler.encodeProtocols(IPFS.HOLE_PUNCH_PROTOCOL));
                } else {
                    HolePunch.initializeConnect(stream, server.getObserved());
                }
                break;
            }
            case IPFS.RELAY_PROTOCOL_STOP:
                if (!stream.isInitiator()) {
                    stream.writeOutput(DataHandler.encodeProtocols(IPFS.RELAY_PROTOCOL_STOP));
                } else {
                    throw new Exception("wrong initiator");
                }
                break;
            case IPFS.IDENTITY_PROTOCOL:
                if (!stream.isInitiator()) {

                    Set<String> protocols = new HashSet<>();
                    protocols.add(IPFS.RELAY_PROTOCOL_STOP);
                    protocols.add(IPFS.MULTISTREAM_PROTOCOL);
                    protocols.add(IPFS.MPLEX_PROTOCOL);
                    protocols.add(IPFS.IDENTITY_PROTOCOL);
                    protocols.add(IPFS.NOISE_PROTOCOL);
                    protocols.add(IPFS.HOLE_PUNCH_PROTOCOL);


                    if (stream.getConnection().getAttribute(Connection.KIND) == Limit.Kind.STATIC) {
                        protocols.addAll(server.getProtocolNames());
                    }

                    IdentifyOuterClass.Identify response = IdentityService.createIdentity(
                            server.getPublicKey(), protocols,
                            server.publishMultiaddrs(),
                            stream.getConnection().remoteMultiaddr());

                    stream.writeOutput(DataHandler.encodeProtocols(IPFS.IDENTITY_PROTOCOL));
                    stream.writeOutput(DataHandler.encode(response))
                            .thenApply(Stream::closeOutput);
                } else {
                    stream.closeOutput();
                }
                break;
            case IPFS.NOISE_PROTOCOL:
                if (!stream.isInitiator()) {
                    stream.writeOutput(DataHandler.encodeProtocols(IPFS.NOISE_PROTOCOL));
                    LiteStream liteStream = Objects.requireNonNull((LiteStream) stream);
                    stream.setAttribute(TRANSPORT, new Handshake(liteStream.getQuicStream()));
                    LogUtils.info(TAG, "Transport set to Handshake");
                } else {
                    throw new Exception("wrong initiator");
                }
                break;
            default:
                ProtocolHandler protocolHandler = null;

                if (stream.getConnection().getAttribute(Connection.KIND) == Limit.Kind.STATIC) {
                    protocolHandler = server.getProtocolHandler(protocol);
                }
                if (protocolHandler != null) {
                    protocolHandler.protocol(stream);
                } else {
                    LogUtils.error(TAG, "Ignore " + protocol);
                    if (!stream.isInitiator()) {
                        stream.writeOutput(DataHandler.encodeProtocols(IPFS.NA));
                    } else {
                        LogUtils.error(TAG, "something wrong not " + protocol);
                        throw new Exception("wrong initiator");
                    }
                }
        }
    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {

        String protocol = (String) stream.getAttribute(PROTOCOL);

        LogUtils.info(TAG, "data streamId " + stream.getStreamId() +
                " protocol " + protocol + " initiator " + stream.isInitiator());

        Objects.requireNonNull(protocol);

        switch (protocol) {
            case IPFS.NOISE_PROTOCOL: {

                if (!stream.isInitiator()) {
                    Noise.Response response = getResponder(stream).handshake(data.array());

                    byte[] msg = response.getMessage();
                    if (msg != null) {
                        stream.writeOutput(Noise.encodeNoiseMessage(msg));
                    }


                    CipherStatePair cipherStatePair = response.getCipherStatePair();
                    if (cipherStatePair != null) {
                        LiteStream liteStream = Objects.requireNonNull((LiteStream) stream);

                        // upgrade connection
                        MuxedTransport muxedTransport = new MuxedTransport(liteStream.getQuicStream(),
                                cipherStatePair.getSender(),
                                cipherStatePair.getReceiver());
                        stream.setAttribute(TRANSPORT, muxedTransport);

                        LogUtils.error(TAG, "Transport set to MuxedTransport, do hole punch initiate");

                        // initiate hole punch
                        HolePunch.initiate(muxedTransport);
                    }
                } else {
                    throw new Exception("wrong initiator");
                }
                break;
            }

            case IPFS.HOLE_PUNCH_PROTOCOL: {
                if (!stream.isInitiator()) {
                    throw new Exception("wrong initiator");
                } else {
                    PeerId peerId = Objects.requireNonNull((PeerId) stream.getAttribute(PEER));
                    HolePunch.sendSync(server, stream, peerId, data);
                }
                break;
            }
            case IPFS.RELAY_PROTOCOL_STOP: {
                if (!stream.isInitiator()) {
                    Circuit.StopMessage stopMessage = Circuit.StopMessage.parseFrom(data.array());
                    Objects.requireNonNull(stopMessage);

                    if (stopMessage.hasPeer()) {
                        PeerId peerId = PeerId.create(stopMessage.getPeer().getId().toByteArray());
                        Circuit.StopMessage.Builder builder =
                                Circuit.StopMessage.newBuilder()
                                        .setType(Circuit.StopMessage.Type.STATUS);
                        builder.setStatus(Circuit.Status.OK);
                        stream.setAttribute(PEER, peerId);
                        stream.writeOutput(DataHandler.encode(builder.build()));
                    } else {
                        Circuit.StopMessage.Builder builder =
                                Circuit.StopMessage.newBuilder()
                                        .setType(Circuit.StopMessage.Type.STATUS);
                        builder.setStatus(Circuit.Status.MALFORMED_MESSAGE);
                        stream.writeOutput(DataHandler.encode(builder.build()));
                    }
                }
                break;
            }
            default:
                ProtocolHandler protocolHandler = null;
                // Note : also the relay can only use server functions when it is static
                if (stream.getConnection().getAttribute(Connection.KIND) == Limit.Kind.STATIC) {
                    protocolHandler = server.getProtocolHandler(protocol);
                }
                if (protocolHandler != null) {
                    protocolHandler.protocol(stream);
                } else {
                    throw new Exception("Protocol " + protocol + " not supported data " + data);
                }
        }

    }

    @Override
    public void streamTerminated() {
        LogUtils.error(TAG, "todo stream is terminated ");
    }

}
