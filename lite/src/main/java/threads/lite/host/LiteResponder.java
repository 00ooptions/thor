package threads.lite.host;


import androidx.annotation.NonNull;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.util.Map;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Session;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.core.Transport;
import threads.lite.utils.DataHandler;


public final class LiteResponder implements StreamHandler {
    private static final String TAG = LiteResponder.class.getSimpleName();

    @NonNull
    private final Map<String, ProtocolHandler> protocols;

    public LiteResponder(@NonNull Session session) {
        this.protocols = session.getProtocols();
    }

    @Override
    public void throwable(Stream stream, Throwable throwable) {
        LogUtils.error(TAG, throwable);
    }

    @NonNull
    @Override
    public Transport getTransport(QuicStream quicStream) {
        return new LiteTransport(quicStream);
    }

    @Override
    public void streamTerminated() {
        LogUtils.info(TAG, "stream terminated nothing to do");
    }

    @Override
    public void protocol(Stream stream, String protocol) throws Exception {
        stream.setAttribute(PROTOCOL, protocol);
        ProtocolHandler protocolHandler = protocols.get(protocol);
        if (protocolHandler != null) {
            protocolHandler.protocol(stream);
        } else {
            LogUtils.info(TAG, "Ignore " + protocol);
            stream.writeOutput(DataHandler.encodeProtocols(IPFS.NA));
        }
    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {
        String protocol = (String) stream.getAttribute(PROTOCOL);
        if (protocol != null) {
            ProtocolHandler protocolHandler = protocols.get(protocol);
            if (protocolHandler != null) {
                protocolHandler.data(stream, data);
            } else {
                throwable(stream, new Exception("StreamHandler invalid protocol"));
            }
        } else {
            throwable(stream, new Exception("StreamHandler invalid protocol"));
        }
    }

}


