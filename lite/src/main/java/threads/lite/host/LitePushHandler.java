package threads.lite.host;

import androidx.annotation.NonNull;

import java.nio.ByteBuffer;

import threads.lite.IPFS;
import threads.lite.core.IpnsEntity;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Stream;
import threads.lite.ipns.IpnsService;
import threads.lite.utils.DataHandler;

public class LitePushHandler implements ProtocolHandler {

    private final LiteHost host;

    public LitePushHandler(@NonNull LiteHost host) {
        this.host = host;
    }

    @Override
    public String getProtocol() {
        return IPFS.LITE_PUSH_PROTOCOL;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.LITE_PUSH_PROTOCOL))
                .thenApply(Stream::closeOutput);
    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {
        IpnsEntity entry = IpnsService.createIpnsEntity(data.array());
        host.push(stream.getConnection(), entry);
    }
}
