package threads.lite.host;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import dht.pb.Dht;
import threads.lite.IPFS;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.IpnsRecord;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.ipns.IpnsService;
import threads.lite.utils.DataHandler;

public class LiteService {

    @NonNull
    public static CompletableFuture<IpnsEntity> pull(@NonNull Connection connection) {
        CompletableFuture<IpnsEntity> done = new CompletableFuture<>();
        connection.createStream(new StreamHandler() {
            @Override
            public void throwable(Stream stream, Throwable throwable) {
                done.completeExceptionally(throwable);
            }

            @Override
            public void protocol(Stream stream, String protocol) throws Exception {
                if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL, IPFS.LITE_PULL_PROTOCOL).contains(protocol)) {
                    throw new Exception("Token " + protocol + " not supported");
                }
                if (Objects.equals(protocol, IPFS.LITE_PULL_PROTOCOL)) {
                    stream.closeOutput();
                }
            }

            @Override
            public void data(Stream stream, ByteBuffer data) throws Exception {
                IpnsEntity entry = IpnsService.createIpnsEntity(data.array());
                done.complete(entry);
            }

            @Override
            public void streamTerminated() {
                if (!done.isDone()) {
                    done.completeExceptionally(new Throwable("stream terminated"));
                }
            }

        }).whenComplete((stream, throwable) -> {
            if (throwable != null) {
                done.completeExceptionally(throwable);
            } else {
                stream.writeOutput(DataHandler.encodeProtocols(
                        IPFS.MULTISTREAM_PROTOCOL, IPFS.LITE_PULL_PROTOCOL));
            }
        });
        return done;
    }

    @NonNull
    public static CompletableFuture<Void> push(@NonNull Connection connection,
                                               @NonNull IpnsRecord ipnsRecord) {

        CompletableFuture<Void> done = new CompletableFuture<>();

        @SuppressLint("SimpleDateFormat") String format = new SimpleDateFormat(
                IPFS.TIME_FORMAT_IPFS).format(new Date());

        Dht.Message.Record rec = Dht.Message.Record.newBuilder()
                .setKey(ByteString.copyFrom(ipnsRecord.getIpnsKey()))
                .setValue(ByteString.copyFrom(ipnsRecord.getSealedRecord()))
                .setTimeReceived(format).build();


        connection.createStream(new StreamHandler() {
            @Override
            public void throwable(Stream stream, Throwable throwable) {
                done.completeExceptionally(throwable);
            }

            @Override
            public void protocol(Stream stream, String protocol) throws Exception {
                if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL, IPFS.LITE_PUSH_PROTOCOL).contains(protocol)) {
                    throw new Exception("Token " + protocol + " not supported");
                }
                if (Objects.equals(protocol, IPFS.LITE_PUSH_PROTOCOL)) {
                    stream.writeOutput(DataHandler.encode(rec))
                            .thenApply(Stream::closeOutput)
                            .thenRun(() -> done.complete(null));
                }
            }

            @Override
            public void data(Stream stream, ByteBuffer data) throws Exception {
                throw new Exception("data method invoked [not expected]");
            }

            @Override
            public void streamTerminated() {
                if (!done.isDone()) {
                    done.completeExceptionally(new Throwable("stream terminated"));
                }
            }
        }).whenComplete((stream, throwable) -> {
            if (throwable != null) {
                done.completeExceptionally(throwable);
            } else {
                stream.writeOutput(DataHandler.encodeProtocols(
                        IPFS.MULTISTREAM_PROTOCOL, IPFS.LITE_PUSH_PROTOCOL));
            }
        });
        return done;
    }
}
