package threads.lite.host;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.server.ApplicationProtocolConnectionFactory;
import net.luminis.quic.server.ServerConnector;

import java.net.DatagramSocket;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import crypto.pb.Crypto;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.BlockStore;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.IpnsRecord;
import threads.lite.core.Keys;
import threads.lite.core.PageStore;
import threads.lite.core.PeerInfo;
import threads.lite.core.PeerStore;
import threads.lite.core.Resolver;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.core.SwarmStore;
import threads.lite.crypto.Key;
import threads.lite.ident.IdentityService;
import threads.lite.ipns.IpnsService;
import threads.lite.minidns.DnsClient;
import threads.lite.minidns.DnsResolver;


public final class LiteHost {
    private static final String TAG = LiteHost.class.getSimpleName();

    @NonNull
    private final AtomicReference<IPV> version = new AtomicReference<>(IPV.IPv4v6);
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final SwarmStore swarmStore;
    @NonNull
    private final PeerStore peerStore;
    @NonNull
    private final PageStore pageStore;
    @NonNull
    private final Keys keys;
    @NonNull
    private final PeerId self;
    @NonNull
    private final LiteCertificate liteCertificate;
    @NonNull
    private final DnsClient dnsClient;
    @NonNull
    private final Resolver resolver;
    @NonNull
    private final Crypto.PublicKey publicKey;
    @Nullable
    private Consumer<LitePush> incomingPush;
    @Nullable
    private Supplier<IpnsRecord> recordSupplier;

    public LiteHost(@NonNull Keys keys, @NonNull BlockStore blockStore,
                    @NonNull PeerStore peerStore, @NonNull SwarmStore swarmStore,
                    @NonNull PageStore pageStore)
            throws Exception {

        this.liteCertificate = LiteCertificate.createCertificate(keys);
        this.keys = keys;
        this.blockStore = blockStore;
        this.peerStore = peerStore;
        this.swarmStore = swarmStore;
        this.pageStore = pageStore;

        this.self = Key.createPeerId(keys.getPublic());
        evaluateProtocol();


        this.dnsClient = DnsResolver.getInstance(() -> {
            switch (version.get()) {
                case IPv4:
                    return new ArrayList<>(DnsResolver.STATIC_IPV4_DNS_SERVERS);
                case IPv6:
                    return new ArrayList<>(DnsResolver.STATIC_IPV6_DNS_SERVERS);
                default:
                    ArrayList<InetAddress> list = new ArrayList<>();
                    list.addAll(DnsResolver.STATIC_IPV4_DNS_SERVERS);
                    list.addAll(DnsResolver.STATIC_IPV6_DNS_SERVERS);
                    return list;
            }
        });

        this.publicKey = Key.createCryptoKey(keys.getPublic());
        this.resolver = new Resolver(dnsClient, ipv());
    }

    public static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        if (isLocalPortFree(port)) {
            return port;
        } else {
            return nextFreePort();
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (Throwable ignore) {
            return false;
        }
    }

    @NonNull
    public PageStore getPageStore() {
        return pageStore;
    }

    @NonNull
    public DnsClient getDnsClient() {
        return dnsClient;
    }

    public void setRecordSupplier(@Nullable Supplier<IpnsRecord> recordSupplier) {
        this.recordSupplier = recordSupplier;
    }

    @NonNull
    private DatagramSocket getSocket(int port) {
        try {
            return new DatagramSocket(port);
        } catch (Throwable ignore) {
            return getSocket(nextFreePort());
        }
    }

    @NonNull
    public Keys getKeys() {
        return keys;
    }

    @NonNull
    public Session createSession(@NonNull BlockStore blockStore,
                                 boolean findProvidersActive) {
        return new LiteSession(blockStore, this, findProvidersActive);
    }

    @NonNull
    public PeerId self() {
        return self;
    }


    @NonNull
    public SwarmStore getSwarmStore() {
        return swarmStore;
    }

    @NonNull
    public BlockStore getBlockStore() {
        return blockStore;
    }

    @NonNull
    public Set<Peer> getRoutingPeers() {
        Set<Peer> peers = new HashSet<>();


        Set<PeerId> peerIds = new HashSet<>();
        for (String name : new HashSet<>(IPFS.DHT_BOOTSTRAP_NODES)) {
            try {
                PeerId peerId = PeerId.decode(name);
                Objects.requireNonNull(peerId);
                peerIds.add(peerId);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

        for (Multiaddr addr : getBootstrap()) {
            if (peerIds.contains(addr.getPeerId())) {
                try {
                    Peer peer = Peer.create(addr);
                    peer.setReplaceable(false);
                    peers.add(peer);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        }
        return peers;
    }

    public void push(@NonNull Connection connection, @NonNull IpnsEntity ipnsEntity) {
        try {
            if (incomingPush != null) {
                incomingPush.accept(new LitePush(connection, ipnsEntity));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    public void setIncomingPush(@Nullable Consumer<LitePush> incomingPush) {
        this.incomingPush = incomingPush;
    }


    public void updateNetwork() {
        evaluateProtocol();
    }

    private void evaluateProtocol() {
        List<InetAddress> addresses = new ArrayList<>();
        try {
            addresses.addAll(Network.networkAddresses());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        if (!addresses.isEmpty()) {
            version.set(getProtocol(addresses));
        } else {
            version.set(IPV.IPv4);
        }
    }


    @NonNull
    private IPV getProtocol(@NonNull List<InetAddress> addresses) {
        boolean ipv4 = false;
        boolean ipv6 = false;
        for (InetAddress inet : addresses) {
            if (inet instanceof Inet6Address) {
                ipv6 = true;
            } else {
                ipv4 = true;
            }
        }

        if (ipv4 && ipv6) {
            return IPV.IPv6; // prefer ipv6
        } else if (ipv4) {
            return IPV.IPv4;
        } else if (ipv6) {
            return IPV.IPv6;
        } else {
            return IPV.IPv4v6;
        }
    }

    @NonNull
    public CompletableFuture<PeerInfo> getPeerInfo(@NonNull Connection conn) {
        return IdentityService.getPeerInfo(self(), conn);
    }


    public Set<Multiaddr> getBootstrap() {

        Set<Multiaddr> multiaddrs = DnsResolver.resolveDnsaddrHost(getDnsClient(), ipv().get(),
                IPFS.LIB2P_DNS);
        if (ipv().get() == IPV.IPv4 || ipv().get() == IPV.IPv4v6) {
            try {
                multiaddrs.add(Multiaddr.create(IPFS.KAD_BOOTSTRAP));
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
        return multiaddrs;
    }


    @NonNull
    public Supplier<IPV> ipv() {
        return version::get;
    }


    // creates a self signed record (used for ipns)
    public byte[] createSelfSignedRecord(byte[] value, long sequence, Date eol) throws Exception {
        Duration duration = Duration.ofHours(IPFS.IPNS_DURATION);
        ipns.pb.Ipns.IpnsEntry record =
                IpnsService.create(keys.getPrivate(), value, sequence, eol, duration);
        return record.toByteArray();
    }


    @NonNull
    public PeerStore getPeerStore() {
        return peerStore;
    }

    @NonNull
    public IpnsRecord getIpnsRecord() throws Exception {
        if (recordSupplier != null) {
            return recordSupplier.get();
        }
        // return empty content
        return new IpnsRecord(Key.createIpnsKey(self()),
                createSelfSignedRecord(new byte[0], 0, IpnsEntity.getDefaultEol()));
    }


    @NonNull
    public Server createServer(@NonNull DatagramSocket socket,
                               @NonNull Consumer<Connection> connectConsumer,
                               @NonNull Consumer<Connection> closedConsumer,
                               @NonNull Function<PeerId, Boolean> isGated) {


        LiteServer server = new LiteServer(this, socket);

        ServerConnector serverConnector = server.getServerConnector();

        serverConnector.setClosedConsumer(connection -> closedConsumer.accept(new LiteConnection(connection)));

        serverConnector.registerApplicationProtocol(IPFS.ALPN, new ApplicationProtocolConnectionFactory() {
            @Override
            public void createConnection(String protocol, QuicConnection quicConnection) {

                LogUtils.error(TAG, "Server connection established " +
                        quicConnection.getRemoteAddress().toString());

                LiteConnection liteConnection = new LiteConnection(quicConnection);

                try {
                    X509Certificate cert = quicConnection.getRemoteCertificate();
                    Objects.requireNonNull(cert);
                    PeerId remotePeerId = LiteCertificate.extractPeerId(cert);
                    Objects.requireNonNull(remotePeerId);

                    if (isGated.apply(remotePeerId)) {
                        throw new Exception("Peer is gated " + remotePeerId);
                    }

                    // now the remote PeerId is available
                    quicConnection.setAttribute(Connection.REMOTE_PEER, remotePeerId);

                } catch (Throwable throwable) {
                    quicConnection.close();
                    LogUtils.error(TAG, throwable);
                    return;
                }

                try {
                    connectConsumer.accept(liteConnection);
                } catch (Throwable ignore) {
                }

            }
        });

        serverConnector.start();
        return server;
    }


    public Server startSever(int port,
                             @NonNull Consumer<Connection> connectConsumer,
                             @NonNull Consumer<Connection> closedConsumer,
                             @NonNull Function<PeerId, Boolean> isGated) {

        DatagramSocket socket = getSocket(port);

        return createServer(socket, connectConsumer, closedConsumer, isGated);

    }


    @NonNull
    public LiteCertificate getLiteCertificate() {
        return liteCertificate;
    }

    @NonNull
    public Resolver getResolver() {
        return resolver;
    }

    @NonNull
    public Crypto.PublicKey getPublicKey() {
        return publicKey;
    }

}


