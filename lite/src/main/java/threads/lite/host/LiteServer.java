package threads.lite.host;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.Version;
import net.luminis.quic.server.ServerConnector;

import java.net.ConnectException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import crypto.pb.Crypto;
import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.autonat.Autonat;
import threads.lite.autonat.AutonatService;
import threads.lite.bitswap.BitSwapEngine;
import threads.lite.bitswap.BitSwapEngineHandler;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.Network;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.AutonatResult;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.Keys;
import threads.lite.core.NatType;
import threads.lite.core.Parameters;
import threads.lite.core.PeerInfo;
import threads.lite.core.PeerStore;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Reservation;
import threads.lite.core.Resolver;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.core.StreamHandler;
import threads.lite.dht.DhtKademlia;
import threads.lite.ident.IdentityHandler;
import threads.lite.ident.IdentityPushHandler;
import threads.lite.ident.IdentityService;
import threads.lite.quic.ConnectionBuilder;
import threads.lite.relay.RelayService;
import threads.lite.server.ServerResponder;
import threads.lite.utils.MultistreamHandler;
import threads.lite.utils.PackageReader;

public class LiteServer implements Server {
    private final static String TAG = LiteServer.class.getSimpleName();

    @NonNull
    private final ReentrantLock autonat = new ReentrantLock();
    @NonNull
    private final ReentrantLock reserve = new ReentrantLock();
    @NonNull
    private final AtomicReference<NatType> natType = new AtomicReference<>();
    @NonNull
    private final AtomicReference<Multiaddr> dialableAddress = new AtomicReference<>();
    @NonNull
    private final AtomicReference<Multiaddr> observedAddress = new AtomicReference<>();
    @NonNull
    private final Set<Reservation> reservations = ConcurrentHashMap.newKeySet();
    @NonNull
    private final DatagramSocket socket;
    @NonNull
    private final ServerConnector serverConnector;
    @NonNull
    private final ServerResponder serverResponder;
    @NonNull
    private final LiteHost host;
    @NonNull
    private final ConcurrentHashMap<PeerId, Connection> swarm = new ConcurrentHashMap<>();

    public LiteServer(@NonNull LiteHost host, @NonNull DatagramSocket socket) {
        this.host = host;
        this.socket = socket;
        this.serverResponder = new ServerResponder();
        this.serverConnector = createServerConnector(socket, serverResponder);

        BitSwapEngine bitSwapEngine = new BitSwapEngine(host.getBlockStore());
        // add the default
        try {
            addProtocolHandler(new MultistreamHandler());
            addProtocolHandler(new LitePushHandler(host));
            addProtocolHandler(new LitePullHandler(host));
            addProtocolHandler(new IdentityHandler(this));
            addProtocolHandler(new IdentityPushHandler(this));
            addProtocolHandler(new BitSwapEngineHandler(bitSwapEngine));
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @NonNull
    private ServerConnector createServerConnector(DatagramSocket socket, StreamHandler streamHandler) {
        List<Version> supportedVersions = new ArrayList<>();
        supportedVersions.add(Version.QUIC_version_1);
        return new ServerConnector(socket, new LiteTrust(),
                host.getLiteCertificate().certificate(),
                host.getLiteCertificate().privateKey(), supportedVersions,
                quicStream -> new PackageReader(streamHandler),
                false, false);

    }

    @Override
    public void closeConnection(Connection connection) {
        try {
            connection.close();
            swarm.remove(connection.remotePeerId());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Nullable
    @Override
    public Connection getConnection(PeerId peerId) {
        Connection connection = swarm.get(peerId);
        if (connection != null) {
            if (connection.isConnected()) {
                return connection;
            } else {
                swarm.remove(peerId);
            }
        }
        return null;
    }

    @Override
    public void addSwarmConnection(Connection connection) {
        swarm.put(connection.remotePeerId(), connection);
    }

    @NonNull
    public Set<Connection> getSwarm() {
        Set<Connection> result = new HashSet<>();
        for (Connection connection : swarm.values()) {
            if (connection.isConnected()) {
                result.add(connection);
            } else {
                swarm.remove(connection.remotePeerId());
            }
        }
        return result;
    }

    @Override
    public Crypto.PublicKey getPublicKey() {
        return host.getPublicKey();
    }

    @Override
    public Multiaddrs publishMultiaddrs() {
        return publishMultiaddrs(IPFS.MAX_PUBLISH_RESERVATIONS);
    }

    @NonNull
    public DatagramSocket getSocket() {
        return socket;
    }


    @Override
    public void shutdown() {
        LogUtils.error(TAG, "shutdown");
        swarm.values().forEach(Connection::close);
        serverConnector.shutdown();
        swarm.clear();
    }

    @Override
    public int getPort() {
        return socket.getLocalPort();
    }

    @Override
    public int numServerConnections() {
        return serverConnector.numConnections();
    }

    private void punching(Multiaddr multiaddr) {

        // Upon expiry of the timer, B starts to send UDP packets filled with random bytes to A's
        // address. Packets should be sent repeatedly in random intervals between 10 and 200 ms.
        try {
            InetAddress address = multiaddr.getInetAddress();
            int port = multiaddr.getPort();

            byte[] datagramData = new byte[64];
            Random rd = new Random();
            rd.nextBytes(datagramData);

            DatagramPacket datagram = new DatagramPacket(
                    datagramData, datagramData.length, address, port);

            socket.send(datagram);
            LogUtils.debug(TAG, "[B] Hole Punch Send Random " + address + " " + port);

            Thread.sleep(getRandomPunch()); // sleep for random value between 10 and 200

            if (!Thread.currentThread().isInterrupted()) {
                punching(multiaddr);
            }

            // interrupt exception should occur, but it simply ends the loop
        } catch (Throwable ignore) {
        }
    }

    private int getRandomPunch() {
        return new Random().nextInt(190) + 10;
    }

    public void holePunching(@NonNull Multiaddrs multiaddrs) {
        try {
            ExecutorService service = Executors.newFixedThreadPool(multiaddrs.size());
            for (Multiaddr multiaddr : multiaddrs) {
                service.execute(() -> punching(multiaddr));
            }
            boolean finished = service.awaitTermination(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            if (!finished) {
                service.shutdownNow();
            }
        } catch (Throwable ignore) {
        }
    }

    @NonNull
    public ServerConnector getServerConnector() {
        return serverConnector;
    }

    @NonNull
    @Override
    public LiteCertificate getLiteCertificate() {
        return host.getLiteCertificate();
    }

    @Override
    public PeerId self() {
        return host.self();
    }

    @NonNull
    @Override
    public Resolver getResolver() {
        return host.getResolver();
    }

    @NonNull
    @Override
    public Set<Peer> getRoutingPeers() {
        return host.getRoutingPeers();
    }

    @NonNull
    @Override
    public PeerStore getPeerStore() {
        return host.getPeerStore();
    }

    @NonNull
    public Connection connect(Multiaddr multiaddr, Parameters parameters)
            throws ConnectException, InterruptedException, TimeoutException {
        Connection connection = getConnection(multiaddr.getPeerId());
        if (connection != null) {
            return connection;
        }
        connection = ConnectionBuilder.connect(serverResponder, multiaddr, parameters,
                getLiteCertificate());
        addSwarmConnection(connection);
        return connection;
    }

    @Override
    @Nullable
    public ProtocolHandler getProtocolHandler(String protocol) {
        return serverResponder.getProtocolHandler(protocol);
    }

    @Override
    public Map<String, ProtocolHandler> getProtocols() {
        return serverResponder.getProtocols();
    }

    @Override
    public void addProtocolHandler(ProtocolHandler protocolHandler) throws Exception {
        serverResponder.addProtocolHandler(protocolHandler);
    }

    @Override
    @NonNull
    public Set<String> getProtocolNames() {
        return serverResponder.getProtocolNames();
    }

    @Override
    public Keys getKeys() {
        return host.getKeys();
    }


    @NonNull
    public Multiaddrs publishMultiaddrs(int maxReservations) {
        Multiaddrs set = new Multiaddrs();
        Multiaddr dialable = dialableAddress.get();
        if (dialable != null) {
            set.add(dialable);
        }

        // shuffle the list, so that new values will be defined
        set.addAll(shuffle().stream().limit(maxReservations).collect(Collectors.toList()));
        return set;
    }


    @NonNull
    public Set<Reservation> reservations() {
        Set<Reservation> result = new HashSet<>();
        for (Reservation reservation : reservations) {
            try {
                if (reservation.getConnection().isConnected()) {
                    result.add(reservation);
                } else {
                    reservations.remove(reservation);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
        return result;
    }

    @NonNull
    private Set<Multiaddr> shuffle() {
        List<Reservation> reservationList = new ArrayList<>(reservations());
        Collections.shuffle(reservationList); // shuffle the list, so that new values will be defined
        return reservationList.stream()
                .map(Reservation::circuitMultiaddr)
                .collect(Collectors.toSet());
    }


    @NonNull
    private Set<Reservation> reservations(@NonNull Collection<Multiaddr> multiaddrs,
                                          @NonNull Cancellable cancellable) {
        reserve.lock();
        try {
            Set<PeerId> relaysStillValid = ConcurrentHashMap.newKeySet();
            Set<Reservation> list = reservations();
            // check if reservations are still valid and not expired
            for (Reservation reservation : list) {

                // check if still a connection
                // only for safety here
                if (!reservation.getConnection().isConnected()) {
                    list.remove(reservation);
                    continue;
                }

                if (reservation.isStaticRelay()) {
                    // still valid
                    relaysStillValid.add(reservation.getRelayId());
                } else {
                    if (reservation.expireInMinutes() > 2) {
                        relaysStillValid.add(reservation.getRelayId());
                    } else {
                        list.remove(reservation);
                    }
                }
            }

            if (!multiaddrs.isEmpty()) {
                ExecutorService service = Executors.newFixedThreadPool(
                        Runtime.getRuntime().availableProcessors());
                for (Multiaddr address : multiaddrs) {
                    service.execute(() -> {
                        try {
                            if (cancellable.isCancelled()) {
                                return;
                            }
                            PeerId peerId = address.getPeerId();
                            Objects.requireNonNull(peerId);

                            if (!relaysStillValid.contains(peerId)) {
                                reservation(address).get(cancellable.timeout(), TimeUnit.SECONDS);
                            } // else case: nothing to do here, reservation is sill valid

                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable.getMessage());
                        }
                    });

                }
                service.shutdown();
                if (cancellable.timeout() > 0) {
                    try {
                        boolean termination = service.awaitTermination(
                                cancellable.timeout(), TimeUnit.SECONDS);
                        if (!termination) {
                            service.shutdownNow();
                        }
                    } catch (Throwable ignore) {
                    }
                }
            }
        } finally {
            reserve.unlock();
        }
        return reservations();
    }

    @Override
    @NonNull
    public CompletableFuture<Reservation> reservation(@NonNull Multiaddr multiaddr) {
        CompletableFuture<Reservation> done = new CompletableFuture<>();
        RelayService.reservation(this, multiaddr)
                .whenComplete((reservation, throwable) -> {
                            if (throwable != null) {
                                done.completeExceptionally(throwable);
                            } else {
                                reservations.add(reservation);
                                done.complete(reservation);
                            }
                        }
                );
        return done;
    }

    public boolean hasReservations() {
        return reservations.size() > 0;
    }


    public void reservations(Cancellable cancellable) {
        try {

            List<Multiaddr> swarm = host.getSwarmStore().getMultiaddrs();

            Set<Reservation> reservations = reservations(swarm, cancellable);

            // cleanup swarm begin
            Set<Multiaddr> relays = ConcurrentHashMap.newKeySet();
            for (Reservation reservation : reservations) {
                LogUtils.error(TAG, reservation.toString());
                relays.add(reservation.getRelayAddress());
            }
            for (Multiaddr address : swarm) {
                if (!relays.contains(address)) {
                    host.getSwarmStore().removeMultiaddr(address);
                }
            }
            // cleanup swarm end

            if (cancellable.isCancelled()) {
                return;
            }


            try (DhtKademlia dhtKademlia = new DhtKademlia(this)) {

                // fill up reservations [not yet enough]
                dhtKademlia.findClosestPeers(cancellable,
                        multiaddr -> {

                            if (relays.contains(multiaddr)) {
                                return;
                            }

                            if (cancellable.isCancelled()) {
                                return;
                            }

                            reservation(multiaddr)
                                    .whenComplete((reservation, throwable) -> {
                                        if (throwable != null) {
                                            LogUtils.error(TAG, throwable.getMessage());
                                        } else {
                                            host.getSwarmStore().storeMultiaddr(
                                                    reservation.getRelayAddress());
                                        }
                                    });
                        }, self());

            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    public void swarm(Cancellable cancellable) {
        try {

            Set<Multiaddr> handled = new HashSet<>();


            getSwarm().forEach(connection -> handled.add(connection.remoteMultiaddr()));


            if (cancellable.isCancelled()) {
                return;
            }

            List<Multiaddr> swarm = host.getSwarmStore().getMultiaddrs();
            swarm.forEach(multiaddr -> {
                if (handled.contains(multiaddr)) {
                    return;
                }

                if (cancellable.isCancelled()) {
                    return;
                }

                try {
                    Connection connection = connect(multiaddr,
                            Parameters.getDefault(IPFS.GRACE_PERIOD_RESERVATION));
                    connection.keepAlive(IPFS.GRACE_PERIOD);
                    host.getSwarmStore().storeMultiaddr(multiaddr);
                } catch (Throwable throwable) {
                    host.getSwarmStore().removeMultiaddr(multiaddr);
                    LogUtils.error(TAG, throwable);
                }
            });


            try (DhtKademlia dhtKademlia = new DhtKademlia(this)) {

                // fill up reservations [not yet enough]
                dhtKademlia.findClosestPeers(cancellable,
                        multiaddr -> {

                            if (handled.contains(multiaddr)) {
                                return;
                            }

                            if (cancellable.isCancelled()) {
                                return;
                            }

                            try {
                                Connection connection = connect(multiaddr,
                                        Parameters.getDefault(IPFS.GRACE_PERIOD_RESERVATION));
                                connection.keepAlive(IPFS.GRACE_PERIOD);
                                host.getSwarmStore().storeMultiaddr(multiaddr);
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }, self());
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    @NonNull
    public AutonatResult autonat() {
        autonat.lock();
        try (Session session = host.createSession(host.getBlockStore(), false)) {
            Autonat autonat = new Autonat();
            Set<Multiaddr> addresses = networkListenAddresses();
            AutonatService.autonat(session, addresses, autonat, host.getBootstrap());

            natType.set(autonat.getNatType());
            Multiaddr winner = autonat.winner();
            if (winner != null) {
                dialableAddress.set(winner);
            }

            InetAddress inetAddress = autonat.getInetAddress();
            if (inetAddress != null) {
                observedAddress.set(Multiaddr.create(self(), inetAddress, getPort()));
            }
            return new AutonatResult(autonat.getNatType(), winner);
        } finally {
            autonat.unlock();
        }
    }


    @NonNull
    public Set<Multiaddr> networkListenAddresses() {
        Set<Multiaddr> set = new HashSet<>();

        int port = getPort();
        try {
            for (InetAddress inetAddress : Network.networkAddresses()) {
                try {
                    set.add(Multiaddr.create(self(), new InetSocketAddress(inetAddress, port)));
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        return set;
    }


    @NonNull
    public PeerInfo getIdentity() throws Exception {
        IdentifyOuterClass.Identify identity = IdentityService.createIdentity(getPublicKey(),
                getProtocolNames(), publishMultiaddrs(), null);
        return IdentityService.getPeerInfo(self(), identity);

    }


    @NonNull
    public Multiaddrs dialableAddresses() {
        Multiaddrs set = publishMultiaddrs(Integer.MAX_VALUE);
        try {
            set.addAll(networkListenAddresses());
            int port = getPort();
            set.addAll(Multiaddr.getSiteLocalAddresses(self(), port));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable.getClass().getSimpleName());
        }
        return set;
    }


    public void provide(@NonNull Cid cid, @NonNull Consumer<Multiaddr> consumer,
                        @NonNull Cancellable cancellable) {

        try (DhtKademlia dhtKademlia = new DhtKademlia(this)) {
            dhtKademlia.provide(cancellable, this, consumer, cid);
        }
    }

    @NonNull
    @Override
    public NatType getNatType() {
        return natType.get();
    }

    @Nullable
    @Override
    public Multiaddr getObserved() {
        return observedAddress.get();
    }
}
