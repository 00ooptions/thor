package threads.lite.asn1;

/**
 * A basic parser for a SEQUENCE object
 */
public interface ASN1SequenceParser
        extends ASN1Encodable, InMemoryRepresentable {

}
