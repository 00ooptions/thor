package threads.lite.asn1.x9;

import threads.lite.asn1.ASN1ObjectIdentifier;

/**
 * Object identifiers for the various X9 standards.
 * <pre>
 * ansi-X9-62 OBJECT IDENTIFIER ::= { iso(1) member-body(2)
 *                                    us(840) ansi-x962(10045) }
 * </pre>
 */
public interface X9ObjectIdentifiers {
    /**
     * Base OID: 1.2.840.10045
     */
    ASN1ObjectIdentifier ansi_X9_62 = new ASN1ObjectIdentifier("1.2.840.10045");

    /**
     * OID: 1.2.840.10045.4
     */
    ASN1ObjectIdentifier id_ecSigType = ansi_X9_62.branch("4");

    /**
     * OID: 1.2.840.10045.4.1
     */
    ASN1ObjectIdentifier ecdsa_with_SHA1 = id_ecSigType.branch("1");
    /**
     * OID: 1.2.840.10045.4.3
     */
    ASN1ObjectIdentifier ecdsa_with_SHA2 = id_ecSigType.branch("3");
    /**
     * OID: 1.2.840.10045.4.3.1
     */
    ASN1ObjectIdentifier ecdsa_with_SHA224 = ecdsa_with_SHA2.branch("1");
    /**
     * OID: 1.2.840.10045.4.3.2
     */
    ASN1ObjectIdentifier ecdsa_with_SHA256 = ecdsa_with_SHA2.branch("2");
    /**
     * OID: 1.2.840.10045.4.3.3
     */
    ASN1ObjectIdentifier ecdsa_with_SHA384 = ecdsa_with_SHA2.branch("3");
    /**
     * OID: 1.2.840.10045.4.3.4
     */
    ASN1ObjectIdentifier ecdsa_with_SHA512 = ecdsa_with_SHA2.branch("4");
}
