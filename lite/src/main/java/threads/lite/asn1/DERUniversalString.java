package threads.lite.asn1;

/**
 * DER UniversalString object - encodes UNICODE (ISO 10646) characters using 32-bit format. In Java we
 * have no way of representing this directly so we rely on byte arrays to carry these.
 */
public class DERUniversalString
        extends ASN1UniversalString {
    DERUniversalString(byte[] contents) {
        super(contents);
    }
}
