package threads.lite.asn1;

import androidx.annotation.NonNull;

import java.io.IOException;

import threads.lite.asn1.util.Arrays;
import threads.lite.asn1.util.Strings;

/**
 * ASN.1 GENERAL-STRING data type.
 * <p>
 * This is an 8-bit encoded ISO 646 (ASCII) character set
 * with optional escapes to other character sets.
 * </p>
 */
public abstract class ASN1GeneralString extends ASN1Primitive implements ASN1String {

    final byte[] contents;

    ASN1GeneralString(byte[] contents) {
        this.contents = contents;
    }

    static ASN1GeneralString createPrimitive(byte[] contents) {
        return new DERGeneralString(contents);
    }

    /**
     * Return a Java String representation of our contained String.
     *
     * @return a Java String representing our contents.
     */
    public final String getString() {
        return Strings.fromByteArray(contents);
    }

    @NonNull
    public String toString() {
        return getString();
    }

    final boolean encodeConstructed() {
        return false;
    }

    final int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    final void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.GENERAL_STRING, contents);
    }

    final boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1GeneralString)) {
            return false;
        }

        ASN1GeneralString that = (ASN1GeneralString) other;

        return Arrays.areEqual(this.contents, that.contents);
    }

    public final int hashCode() {
        return Arrays.hashCode(contents);
    }
}
