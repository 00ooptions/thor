package threads.lite.cid;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.google.protobuf.ByteString;

import multiaddr.pb.MultiaddrOuterClass;

@Entity
public final class Peer {

    @PrimaryKey
    @ColumnInfo(name = "key")
    private final short key;

    @NonNull
    @ColumnInfo(name = "multiaddr")
    @TypeConverters(Peer.class)
    private final Multiaddr multiaddr;

    @NonNull
    @ColumnInfo(name = "id")
    @TypeConverters(ID.class)
    private final ID id;

    @Ignore
    private int rtt = Integer.MAX_VALUE;  // temporary value
    @Ignore
    private boolean replaceable = true; // temporary value

    @Ignore
    private boolean bootstrap = false; // temporary value


    // Note: fore creating a peer instance use the static create method
    public Peer(short key, @NonNull Multiaddr multiaddr, @NonNull ID id) {
        this.key = key;
        this.multiaddr = multiaddr;
        this.id = id;
    }

    @NonNull
    @TypeConverter
    public static Multiaddr fromArray(byte[] data) {
        if (data == null) {
            throw new IllegalStateException("data can not be null");
        }
        try {
            MultiaddrOuterClass.Multiaddr stored =
                    MultiaddrOuterClass.Multiaddr.parseFrom(data);
            PeerId peerId = PeerId.create(stored.getId().toByteArray());
            return Multiaddr.create(peerId, stored.getAddrs().asReadOnlyByteBuffer());
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @NonNull
    @TypeConverter
    public static byte[] toArray(Multiaddr multiaddr) {
        if (multiaddr == null) {
            throw new IllegalStateException("multiaddr can not be null");
        }
        return MultiaddrOuterClass.Multiaddr.newBuilder()
                .setId(ByteString.copyFrom(multiaddr.getPeerId().encoded()))
                .setAddrs(ByteString.copyFrom(multiaddr.encoded()))
                .build().toByteArray();

    }

    @NonNull
    public static Peer create(@NonNull Multiaddr multiaddr) throws Exception {
        ID id = ID.convertPeerID(multiaddr.getPeerId());
        return new Peer(createKey(id), multiaddr, id);
    }

    public static short createKey(@NonNull ID id) {
        short shortKey = (short) id.hashCode();
        return shortKey > 0 ? shortKey : (short) -shortKey;
    }

    public int getRtt() {
        return rtt;
    }

    public void setRtt(int rtt) {
        this.rtt = rtt;
    }

    public short getKey() {
        return key;
    }

    @NonNull
    public ID getId() {
        return id;
    }

    @NonNull
    public PeerId getPeerId() {
        return multiaddr.getPeerId();
    }

    @NonNull
    public Multiaddr getMultiaddr() {
        return multiaddr;
    }

    @NonNull
    @Override
    public String toString() {
        return "Peer{" +
                "multiaddr=" + multiaddr +
                ", id=" + id +
                ", replaceable=" + replaceable +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Peer peer = (Peer) o;
        return multiaddr.equals(peer.multiaddr);
    }

    @Override
    public int hashCode() {
        return multiaddr.hashCode();
    }

    public boolean isReplaceable() {
        return replaceable;
    }

    public void setReplaceable(boolean replaceable) {
        this.replaceable = replaceable;
    }

    public boolean isBootstrap() {
        return bootstrap;
    }

    public void setBootstrap(boolean bootstrap) {
        this.bootstrap = bootstrap;
    }
}
