package threads.lite.cid;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.google.protobuf.ByteString;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import threads.lite.LogUtils;

@Entity
public final class Multiaddr {
    @ColumnInfo(name = "tags")
    @TypeConverters(Multiaddr.class)
    private final Tag[] tags;
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "peerId")
    @TypeConverters(PeerId.class)
    private final PeerId peerId;

    public Multiaddr(@NonNull PeerId peerId, Tag[] tags) {
        this.peerId = peerId;
        this.tags = tags;
    }

    @NonNull
    @TypeConverter
    public static Tag[] fromArray(byte[] data) {
        if (data == null) {
            throw new IllegalStateException("data can not be null");
        }
        try {
            return Protocol.encoded(data);
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @NonNull
    @TypeConverter
    public static byte[] toArray(Tag[] tags) {
        if (tags == null) {
            throw new IllegalStateException("multiaddr can not be null");
        }
        return Protocol.encoded(tags);

    }

    @NonNull
    public static Multiaddr getLoopbackAddress(PeerId peerId, int port) {
        return Multiaddr.create(peerId, new InetSocketAddress(
                InetAddress.getLoopbackAddress(), port));
    }

    @NonNull
    public static List<Multiaddr> getSiteLocalAddresses(PeerId peerId, int port) throws Exception {
        List<InetSocketAddress> inetSocketAddresses = Network.getSiteLocalAddress(port);
        List<Multiaddr> result = new ArrayList<>();
        for (InetSocketAddress inetSocketAddress : inetSocketAddresses) {
            result.add(create(peerId, inetSocketAddress));
        }
        return result;
    }

    @NonNull
    public static Multiaddr create(String address) throws Exception {
        String[] parts = address.split("/");
        if (parts.length == 0) {
            throw new Exception("Address has not a separator");
        }
        String empty = parts[0];
        if (!empty.isEmpty()) {
            throw new Exception("Address should start with separator '/'");
        }
        int size = parts.length;
        int last = size - 1;
        int udp = last - 1;

        PeerId peerId = PeerId.decode(parts[last]); // last element always a peerId
        Objects.requireNonNull(peerId);

        Tag[] tags = new Tag[udp - 1];

        // skip udp and peerId
        for (int i = 1; i < udp; i++) {
            String part = parts[i];
            Protocol p = Protocol.get(part);
            tags[i - 1] = p;
            if (p.size() == 0) continue;

            i++;
            String component = parts[i];
            if (component == null || component.isEmpty())
                throw new Exception("Protocol requires address, but non provided!");

            tags[i - 1] = p.addressToTag(component);
        }
        return new Multiaddr(peerId, tags);
    }

    @NonNull
    public static Multiaddr createCircuit(PeerId peerId, Multiaddr relayAddr) {
        Tag[] parts = relayAddr.getTags();
        int partsLength = parts.length;
        Tag[] newParts = Arrays.copyOf(parts, partsLength + 3);
        newParts[partsLength] = Protocol.get(Type.P2P);
        newParts[partsLength + 1] = relayAddr.getPeerId();
        newParts[partsLength + 2] = Protocol.get(Type.P2PCIRCUIT);
        return new Multiaddr(peerId, newParts);
    }

    @NonNull
    public static Multiaddr create(PeerId peerId, ByteBuffer raw) throws Exception {
        Tag[] parts = Protocol.parseMultiaddr(peerId, raw);
        return new Multiaddr(peerId, parts);
    }

    // circuit addresses are sorted out here
    @NonNull
    public static Multiaddrs create(PeerId peerId, List<ByteString> byteStrings, IPV ipv) {
        Multiaddrs result = new Multiaddrs();
        Multiaddrs multiaddrs = Multiaddr.create(peerId, byteStrings);
        for (Multiaddr addr : multiaddrs) {
            if (!addr.isCircuitAddress()) {
                if (addr.protocolSupported(ipv)) {
                    result.add(addr);
                }
            }
        }
        return result;
    }

    @NonNull
    public static Multiaddrs create(PeerId peerId, List<ByteString> byteStrings) {
        Multiaddrs multiaddrs = new Multiaddrs();
        for (ByteString entry : byteStrings) {
            try {
                multiaddrs.add(Multiaddr.create(peerId, entry.asReadOnlyByteBuffer()));
            } catch (Throwable ignore) {
                // can happen of not supported multi addresses like tcp, etc.
            }
        }
        return multiaddrs;
    }

    @NonNull
    public static Multiaddr create(PeerId peerId, InetSocketAddress inetSocketAddress) {
        return create(peerId, inetSocketAddress.getAddress(), inetSocketAddress.getPort());
    }

    @NonNull
    public static Multiaddr create(PeerId peerId, java.net.InetAddress inetAddress, int port) {
        boolean ipv6 = inetAddress instanceof Inet6Address;
        int size = 5;

        Tag[] parts = new Tag[size];
        if (ipv6) {
            parts[0] = Protocol.get(Type.IP6);
        } else {
            parts[0] = Protocol.get(Type.IP4);
        }
        parts[1] = new Tag.InetAddress(inetAddress.getAddress());
        parts[2] = Protocol.get(Type.UDP);
        parts[3] = new Tag.Port(port);
        parts[4] = Protocol.get(Type.QUICV1);

        return new Multiaddr(peerId, parts);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean isLocalAddress(InetAddress inetAddress) {
        return inetAddress.isAnyLocalAddress() || inetAddress.isLinkLocalAddress()
                || (inetAddress.isLoopbackAddress())
                || (inetAddress.isSiteLocalAddress());
    }

    @NonNull
    public static Multiaddr upgrade(Multiaddr multiaddr, InetAddress address) {
        Tag ip = Protocol.get(Type.IP4);
        if (address instanceof Inet6Address) {
            ip = Protocol.get(Type.IP6);
        }
        multiaddr.tags[0] = ip;
        multiaddr.tags[1] = new Tag.InetAddress(address.getAddress());
        return multiaddr;
    }

    public Tag[] getTags() {
        return tags;
    }

    @NonNull
    public InetSocketAddress getInetSocketAddress() {
        return new InetSocketAddress(getHost(), getPort());
    }

    public boolean isAnyDns() {
        return isDnsaddr() || isDns() || isDns4() || isDns6();
    }

    public boolean strictSupportedAddress() {
        try {
            if (!isAnyDns()) {
                InetAddress inetAddress = getInetAddress();
                if (Network.isWellKnownIPv4Translatable(inetAddress)) {
                    return false;
                }
                return !Multiaddr.isLocalAddress(inetAddress);
            } else {
                return true;
            }
        } catch (Throwable ignore) {
            LogUtils.error(getClass().getSimpleName(), toString());
            // nothing to do here
        }
        return false;
    }


    public boolean hasQuic() {
        return has(Set.of(Protocol.get(Type.QUIC), Protocol.get(Type.QUICV1)));
    }

    public boolean protocolSupported(IPV ipv) {

        if (ipv == IPV.IPv4) {
            if (isIP6() || isDns6()) {
                return false;
            }
        }
        if (ipv == IPV.IPv6) {
            if (isIP4() || isDns4()) {
                return false;
            }
        }

        if (isDnsaddr()) {
            return true;
        }
        if (isDns()) {
            return hasQuic();
        }
        if (isDns4()) {
            return hasQuic();
        }
        if (isDns6()) {
            return hasQuic();
        }
        if (hasQuic()) {
            return strictSupportedAddress();
        }
        return false;
    }

    public byte[] encoded() {
        return Protocol.encoded(tags);
    }


    @NonNull
    public String getHost() {
        return tags[1].toString();
    }

    @NonNull
    public InetAddress getInetAddress() throws UnknownHostException {
        Tag part = tags[1]; // this is always the host address
        if (part instanceof Tag.Address) {
            throw new IllegalStateException("host must be resolved first");
        } else if (part instanceof Tag.InetAddress) {
            Tag.InetAddress inetAddress = (Tag.InetAddress) part;
            Objects.requireNonNull(inetAddress);
            return InetAddress.getByAddress(inetAddress.getAddress());
        } else {
            throw new IllegalStateException("unknown host");
        }
    }

    public int getPort() {
        try {
            Tag part = tags[3];
            Objects.requireNonNull(part);
            Tag.Port port = (Tag.Port) part;
            Objects.requireNonNull(port);
            return port.getPort();
        } catch (Throwable throwable) {
            LogUtils.error(Multiaddr.class.getSimpleName(), this.toString());
            throw throwable;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Multiaddr multiaddr = (Multiaddr) o;
        return Objects.equals(peerId, multiaddr.peerId) && Arrays.equals(tags, multiaddr.tags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Arrays.hashCode(tags), peerId);
    }

    @NonNull
    @Override
    public String toString() {
        return Protocol.toAddress(tags) + "/" + Protocol.get(Type.P2P) + "/" + peerId;
    }

    private boolean has(Set<Tag> types) {
        for (Tag part : tags) {
            if (types.contains(part)) {
                return true;
            }
        }
        return false;
    }


    public boolean isIP4() {
        return Objects.equals(tags[0], Protocol.get(Type.IP4));
    }

    public boolean isIP6() {
        return Objects.equals(tags[0], Protocol.get(Type.IP6));
    }

    public boolean isDns() {
        return Objects.equals(tags[0], Protocol.get(Type.DNS));
    }

    public boolean isDns6() {
        return Objects.equals(tags[0], Protocol.get(Type.DNS6));
    }

    public boolean isDns4() {
        return Objects.equals(tags[0], Protocol.get(Type.DNS4));
    }

    public boolean isDnsaddr() {
        return Objects.equals(tags[0], Protocol.get(Type.DNSADDR));
    }

    public boolean isCircuitAddress() {
        return has(Set.of(Protocol.get(Type.P2PCIRCUIT)));
    }

    @NonNull
    public PeerId getPeerId() {
        return peerId;
    }

    @NonNull
    public PeerId getRelayId() {
        if (!isCircuitAddress()) {
            throw new IllegalStateException("no circuit address");
        }
        for (Tag tag : tags) {
            if (tag instanceof PeerId) {
                return (PeerId) tag;
            }
        }
        throw new IllegalStateException("no valid circuit address");
    }

    public boolean isAnyLocalAddress() {
        try {
            InetAddress inetAddress = getInetAddress();
            return Multiaddr.isLocalAddress(inetAddress);
        } catch (Throwable ignore) {
            // nothing to do here
            LogUtils.error(getClass().getSimpleName(), toString());
        }
        return false;
    }

    @NonNull
    public Multiaddr getRelayAddress() {
        if (!isCircuitAddress()) {
            throw new IllegalStateException("not a circuit address");
        }
        PeerId relayId = getRelayId();
        Objects.requireNonNull(relayId);
        InetSocketAddress inetSocketAddress = getInetSocketAddress();
        Objects.requireNonNull(inetSocketAddress);
        return create(relayId, inetSocketAddress);
    }


}
