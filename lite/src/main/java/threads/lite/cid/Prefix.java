package threads.lite.cid;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;

import threads.lite.utils.DataHandler;


public final class Prefix {

    private final int version;
    private final IPLD codec;
    private final Multihash.Type type;


    public Prefix(IPLD codec, Multihash.Type type, int version) {
        this.version = version;
        this.codec = codec;
        this.type = type;
    }

    public static Prefix getPrefixFromBytes(byte[] bytes) throws IOException {
        ByteBuffer wrap = ByteBuffer.wrap(bytes);

        int version = DataHandler.readUnsignedVariant(wrap);
        if (version != 1 && version != 0) {
            throw new IOException("invalid version");
        }

        int codec = DataHandler.readUnsignedVariant(wrap);

        int mhtype = DataHandler.readUnsignedVariant(wrap);

        int mhlen = DataHandler.readUnsignedVariant(wrap);

        Multihash.Type type = Multihash.Type.lookup(mhtype);
        if (type.length != mhlen) {
            throw new IOException("invalid length");
        }

        return new Prefix(IPLD.get(codec), type, version);


    }

    @NonNull
    public static Cid sum(Prefix prefix, byte[] data) throws Exception {

        if (!prefix.isSha2556()) {
            throw new IOException("Type Multihash " +
                    prefix.type.name() + " is not supported");
        }

        switch (prefix.version) {
            case 0:
            case 1:
                return Cid.createCidV1(prefix.codec, data);
            default:
                throw new Exception("Invalid cid version");
        }


    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Prefix prefix = (Prefix) o;
        return version == prefix.version && codec == prefix.codec && type == prefix.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(version, codec, type);
    }

    @NonNull
    public Multihash.Type getType() {
        return type;
    }

    public boolean isSha2556() {
        return type == Multihash.Type.sha2_256;
    }

    @NonNull
    @Override
    public String toString() {
        return "Prefix{" +
                "version=" + version +
                ", codec=" + codec.name() +
                ", type=" + type.name() +
                '}';
    }

    public byte[] bytes() {
        int versionLength = DataHandler.unsignedVariantSize(version);
        int codecLength = DataHandler.unsignedVariantSize(codec.getCodec());
        int typeLength = DataHandler.unsignedVariantSize(type.index);
        int length = DataHandler.unsignedVariantSize(type.length);

        ByteBuffer buffer = ByteBuffer.allocate(versionLength + codecLength + typeLength + length);
        DataHandler.writeUnsignedVariant(buffer, version);
        DataHandler.writeUnsignedVariant(buffer, codec.getCodec());
        DataHandler.writeUnsignedVariant(buffer, type.index);
        DataHandler.writeUnsignedVariant(buffer, type.length);
        return buffer.array();
    }

    public int getVersion() {
        return version;
    }

    public IPLD getCodec() {
        return codec;
    }
}
