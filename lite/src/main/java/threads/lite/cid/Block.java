package threads.lite.cid;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import merkledag.pb.Merkledag;

@Entity
public class Block {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "cid")
    @TypeConverters(Cid.class)
    private final Cid cid;

    @NonNull
    @ColumnInfo(name = "data", typeAffinity = ColumnInfo.BLOB)
    @TypeConverters(Block.class)
    private final Merkledag.PBNode data;

    public Block(@NonNull Cid cid, @NonNull Merkledag.PBNode data) {
        this.cid = cid;
        this.data = data;
    }

    @NonNull
    public static Block createBlock(@NonNull Cid cid, @NonNull Merkledag.PBNode data) {
        return new Block(cid, data);
    }

    @NonNull
    public static Block createBlock(@NonNull Merkledag.PBNode data) throws Exception {
        Cid cid = Cid.createCidV1(IPLD.DagProtobuf, data.toByteArray());
        return Block.createBlock(cid, data);
    }

    @NonNull
    @TypeConverter
    public static Merkledag.PBNode fromArray(byte[] data) {
        try {
            return Merkledag.PBNode.parseFrom(data);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    @NonNull
    @TypeConverter
    public static byte[] toArray(Merkledag.PBNode data) {
        return data.toByteArray();
    }

    @NonNull
    public Merkledag.PBNode getData() {
        return data;
    }

    @NonNull
    public Cid getCid() {
        return cid;
    }

}
