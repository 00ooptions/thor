package threads.lite.cid;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.TypeConverter;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Objects;

import threads.lite.utils.DataHandler;

public final class Cid {

    private final Multihash multihash;
    private final int codec;


    private Cid(Multihash multihash, IPLD ipld) {
        this.multihash = multihash;
        this.codec = ipld.getCodec();
    }

    @Nullable
    @TypeConverter
    public static Cid fromArray(byte[] data) {
        if (data == null) {
            return null;
        }
        return decode(data);
    }

    @Nullable
    @TypeConverter
    public static byte[] toArray(Cid cid) {
        if (cid == null) {
            return null;
        }
        return cid.encoded();
    }

    @NonNull
    public static Cid nsToCid(@NonNull String ns) throws Exception {
        return createCidV1(IPLD.RAW, ns.getBytes(StandardCharsets.UTF_8));
    }

    @NonNull
    public static Cid decode(@NonNull String name) throws Exception {
        if (name.length() < 2) {
            throw new IllegalStateException("invalid cid");
        }

        if (name.length() == 46 && name.startsWith("Qm")) {
            Multihash multihash = Multihash.decode(Base58.decode(name));
            Objects.requireNonNull(multihash);
            return new Cid(multihash, IPLD.DagProtobuf);
        }

        byte[] raw = Multibase.decode(name);
        ByteBuffer wrap = ByteBuffer.wrap(raw);

        int version = DataHandler.readUnsignedVariant(wrap);
        if (version != 1) {
            throw new IllegalStateException("invalid version");
        }
        int codecType = DataHandler.readUnsignedVariant(wrap);
        IPLD ipld = IPLD.get(codecType);

        Multihash mh = Multihash.decode(wrap);
        Objects.requireNonNull(mh);
        return new Cid(mh, ipld);
    }

    @NonNull
    public static Cid createCidV1(IPLD ipld, byte[] data) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(data);
        Multihash multihash = Multihash.create(Multihash.Type.sha2_256, hash);
        return createCidV1(ipld, multihash);
    }

    @NonNull
    public static Cid createCidV1(IPLD ipld, Multihash multihash) {
        return new Cid(multihash, ipld);
    }

    @NonNull
    public static Cid decode(byte[] data) {
        if (data[0] == 1) { // can only be version cid version 1
            ByteBuffer wrap = ByteBuffer.wrap(data);
            DataHandler.readUnsignedVariant(wrap); // skip version
            int codecType = DataHandler.readUnsignedVariant(wrap);
            IPLD ipld = IPLD.get(codecType);
            Multihash mh = Multihash.decode(wrap);
            Objects.requireNonNull(mh);
            return new Cid(mh, ipld);
        } else {
            return Cid.createCidV1(IPLD.DagProtobuf, Multihash.decode(data));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cid cid = (Cid) o;
        return codec == cid.codec && multihash.equals(cid.multihash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(multihash, codec);
    }

    @NonNull
    @Override
    public String toString() {
        return Multibase.encode(Multibase.Base.Base32, encoded());
    }

    @NonNull
    public IPLD getCodec() {
        return IPLD.get(codec);
    }

    public byte[] encoded() {
        byte[] encoded = multihash.encoded();
        int versionLength = DataHandler.unsignedVariantSize(1);
        int codecLength = DataHandler.unsignedVariantSize(codec);

        ByteBuffer buffer = ByteBuffer.allocate(
                versionLength + codecLength + encoded.length);
        DataHandler.writeUnsignedVariant(buffer, 1);
        DataHandler.writeUnsignedVariant(buffer, codec);
        buffer.put(encoded);
        return buffer.array();

    }


    @NonNull
    public Prefix getPrefix() {
        Multihash.Type type = multihash.getType();
        return new Prefix(getCodec(), type, 1);

    }


    // this function returns the multihash as bytes
    // a Multihash consists of three parts (Type, Length, Hash of Data)
    // Support is right now only for Type "sha2_256"
    @NonNull
    public Multihash getMultihash() {
        return multihash;
    }

    public boolean isSupported() {
        return multihash.getType() == Multihash.Type.sha2_256;
    }
}