package threads.lite.blockstore;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.lite.cid.Block;
import threads.lite.cid.Cid;

@androidx.room.Database(entities = {Block.class}, version = 2, exportSchema = false)
@TypeConverters({Cid.class, Block.class})
public abstract class BlocksStoreDatabase extends RoomDatabase {

    public abstract BlockStoreDao blockDao();

}
