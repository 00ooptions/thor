package threads.lite.core;


import androidx.annotation.NonNull;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;

import threads.lite.host.LiteTransport;

public interface StreamHandler {

    String LIMIT = "LIMIT";
    String TRANSPORT = "TRANSPORT";
    String PROTOCOL = "PROTOCOL";

    void throwable(Stream stream, Throwable throwable);

    void protocol(Stream stream, String protocol) throws Exception;

    void data(Stream stream, ByteBuffer data) throws Exception;

    @NonNull
    default Transport getTransport(QuicStream quicStream) {
        Transport transport = (Transport) quicStream.getAttribute(TRANSPORT);
        if (transport != null) {
            return transport;
        }
        return new LiteTransport(quicStream);
    }

    void streamTerminated();
}
