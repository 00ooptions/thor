package threads.lite.core;


public interface Progress extends Cancellable {

    void setProgress(int progress);

    default boolean doProgress() {
        return true;
    }

}
