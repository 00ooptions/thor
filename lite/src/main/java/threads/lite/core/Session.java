package threads.lite.core;

import androidx.annotation.NonNull;

import java.io.Closeable;
import java.net.ConnectException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.host.LiteCertificate;


public interface Session extends Closeable, Routing, BitSwap, Host {

    String TAG = Session.class.getSimpleName();

    // returns the block store where all data is stored
    @NonNull
    BlockStore getBlockStore();

    // returns true, when in bitswap the provider search is enabled
    boolean isFindProvidersActive();

    // returns true when session is closed, otherwise false (note: when session is closed
    // operations on a session will fail [Interrupt Exception]
    boolean isClosed();

    @NonNull
    Connection dial(Multiaddr address, Parameters parameters)
            throws ConnectException, InterruptedException, TimeoutException;

    @NonNull
    default CompletableFuture<Connection> dial(
            Cancellable cancellable, PeerId peerId, Parameters parameters)
            throws ConnectException, InterruptedException, TimeoutException {

        CompletableFuture<Connection> done = new CompletableFuture<>();
        Connection connection = getConnection(peerId);
        if (connection != null) {
            done.complete(connection);
            return done;
        }

        AtomicBoolean found = new AtomicBoolean(false);
        findPeer(() -> cancellable.isCancelled() || found.get(),
                multiaddr -> {
                    try {
                        // step by step connecting (though not fast)
                        synchronized (multiaddr.getPeerId().toString().intern()) {
                            if (cancellable.isCancelled()) {
                                return;
                            }

                            Connection conn = dial(multiaddr, parameters);
                            found.set(true);
                            done.complete(conn);
                        }
                        // abort other stuff
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }, peerId);

        return done;
    }

    @NonNull
    LiteCertificate getLiteCertificate();
}

