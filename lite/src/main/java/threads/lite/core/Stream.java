package threads.lite.core;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.CompletableFuture;

public interface Stream {

    @NonNull
    Connection getConnection();

    @NonNull
    CompletableFuture<Stream> closeOutput();

    @NonNull
    CompletableFuture<Stream> writeOutput(byte[] data);

    int getStreamId();

    void close();

    void setAttribute(String key, Object value);

    @Nullable
    Object getAttribute(String key);

    boolean isInitiator();

}
