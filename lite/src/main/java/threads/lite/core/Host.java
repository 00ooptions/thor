package threads.lite.core;

import androidx.annotation.Nullable;

import java.util.Map;
import java.util.Set;

import crypto.pb.Crypto;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;

public interface Host extends Swarm {
    Crypto.PublicKey getPublicKey();

    Multiaddrs publishMultiaddrs();

    Resolver getResolver();

    Set<Peer> getRoutingPeers();

    PeerStore getPeerStore();

    PeerId self();

    Set<String> getProtocolNames();

    Map<String, ProtocolHandler> getProtocols();

    Keys getKeys();

    void addProtocolHandler(ProtocolHandler protocolHandler) throws Exception;

    @Nullable
    ProtocolHandler getProtocolHandler(String protocol);
}
