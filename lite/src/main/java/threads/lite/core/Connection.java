package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.net.InetSocketAddress;
import java.util.concurrent.CompletableFuture;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;

public interface Connection {
    String REMOTE_PEER = "REMOTE_PEER";
    String KIND = "KIND";

    void close();

    boolean isConnected();

    @NonNull
    InetSocketAddress getRemoteAddress();

    @NonNull
    InetSocketAddress getLocalAddress();

    @NonNull
    PeerId remotePeerId();

    @NonNull
    CompletableFuture<Stream> createStream(@NonNull StreamHandler streamHandler);

    @NonNull
    Multiaddr remoteMultiaddr();

    // throws exception when invoked double on a connection or not in connection client mode
    void keepAlive(int pingInterval) throws Exception;

    int getSmoothedRtt();

    void setAttribute(String key, Object value);

    @Nullable
    Object getAttribute(String key);

}
