package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.net.ConnectException;
import java.util.Set;
import java.util.concurrent.TimeoutException;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;

public interface Swarm {
    @NonNull
    Set<Connection> getSwarm();

    void closeConnection(Connection connection);

    @NonNull
    Connection connect(Multiaddr address, Parameters parameters)
            throws ConnectException, InterruptedException, TimeoutException;

    @Nullable
    Connection getConnection(PeerId peerId);

    void addSwarmConnection(Connection connection);
}
