package threads.lite.core;


import java.nio.ByteBuffer;
import java.util.Objects;

import threads.lite.utils.DataHandler;

public interface Transport {

    static FrameReader getFrameReader(Transport transport) {
        return data -> {
            switch (transport.getType()) {
                case MUXED:
                case SECURED:
                case HANDSHAKE:
                    return bigEndianUnsignedShortReader(data);
                case PLAIN:
                    return unsignedVarintReader(data);
                default:
                    throw new IllegalStateException("not supported transport reader");
            }
        };
    }

    private static ByteBuffer bigEndianUnsignedShortReader(ByteBuffer data) {
        Objects.requireNonNull(data);
        int length = DataHandler.getBigEndianUnsignedShort(data);
        return ByteBuffer.allocate(length);
    }

    private static ByteBuffer unsignedVarintReader(ByteBuffer data) {
        Objects.requireNonNull(data);
        int length = DataHandler.readUnsignedVariant(data);
        return ByteBuffer.allocate(length);
    }

    Type getType();

    Stream getStream();

    enum Type {PLAIN, HANDSHAKE, SECURED, MUXED}
}
