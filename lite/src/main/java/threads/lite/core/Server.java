package threads.lite.core;

import androidx.annotation.NonNull;

import java.net.DatagramSocket;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.host.LiteCertificate;

public interface Server extends Host {
    void shutdown();

    int getPort();

    int numServerConnections();

    @NonNull
    DatagramSocket getSocket();

    void holePunching(@NonNull Multiaddrs multiaddrs);

    @NonNull
    LiteCertificate getLiteCertificate();

    @NonNull
    PeerInfo getIdentity() throws Exception;

    @NonNull
    AutonatResult autonat();

    boolean hasReservations();

    Set<Reservation> reservations();

    void swarm(Cancellable cancellable);

    CompletableFuture<Reservation> reservation(Multiaddr multiaddr);

    void reservations(Cancellable cancellable);

    Set<Multiaddr> dialableAddresses();

    void provide(@NonNull Cid cid, @NonNull Consumer<Multiaddr> consumer,
                 @NonNull Cancellable cancellable);

    @NonNull
    NatType getNatType();

    Multiaddr getObserved();
}
